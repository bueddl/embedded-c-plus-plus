import yaml
from pathlib import Path
import os
import shutil
import re
import subprocess
from distutils.dir_util import copy_tree
import serial
from termcolor import colored
import sys
import argparse
import yamlpath.patches
from yamlpath.func import get_yaml_data, get_yaml_editor
from yamlpath.wrappers import ConsolePrinter
from yamlpath import Processor
from yamlpath import YAMLPath
import time
import datetime
import threading
import random
import telnetlib


class JobQueue:
    def __init__(self):
        self.__queue = list()

    def add_jobs(self, *jobs):
        self.__queue.extend(jobs)

    def __len__(self):
        return sum(map(len, self.__queue))

    def items(self):
        return self.__queue


class Job:
    def __init__(self):
        self._has_run = False

    def run(self, runtype):
        self._runtype = runtype
        self._has_run = True
        if not self._prepare():
            return False, None, None

        result = self._execute()
        self._cleanup()

        return result

    def has_run(self):
        return self._has_run

    def __repr__(self):
        return '{}[{}]'.format(
            type(self).__name__,
            self._id())

    def __len__(self):
        return 1

    def _id(self):
        pass

    def _prepare(self):
        pass

    def _execute(self):
        pass

    def _cleanup(self):
        pass

    def _run_tool(self, file, args, stdout_logfile, stderr_logfile, wait_for_completion=True, **kwargs):
        logdir = os.path.dirname(stdout_logfile)
        with open(Path(logdir).joinpath('cli.log'), 'a+') as f:
            print(file, ' '.join(map(str, args)), file=f)

        stdout = open(stdout_logfile, 'w+', encoding='utf-8')
        stderr = open(stderr_logfile, 'w+', encoding='utf-8')

        p = subprocess.Popen([file, *args],
                             stdout=stdout,
                             stderr=stderr,
                             **kwargs)

        if wait_for_completion:
            p.wait()
            return p.returncode
        else:
            return p

    def should_skip(self, args):
        status_dir = self.get_job_dir().joinpath(Path('status'))
        status_dir.mkdir(exist_ok=True, parents=True)
        status_file = status_dir.joinpath(type(self).__name__)
        return (status_file.exists() and args.skip_completed) or self._should_skip(args)

    def _should_skip(self, args):
        return False

    def _succeeded(self):
        status_dir = self.get_job_dir().joinpath(Path('status'))
        status_dir.mkdir(exist_ok=True, parents=True)
        status_file = status_dir.joinpath(type(self).__name__)
        with open(status_file, 'w') as f:
            pass

    def was_successful(self):
        status_dir = self.get_job_dir().joinpath(Path('status'))
        status_dir.mkdir(exist_ok=True, parents=True)
        status_file = status_dir.joinpath(type(self).__name__)
        return status_file.exists()


class BuildJob(Job):
    SOURCES_DIR_NAME = 'sources'
    BUILD_DIR_NAME = 'build'
    OUTPUT_DIR_NAME = 'out'

    def __init__(self, data: dict, config: dict):
        super(BuildJob, self).__init__()
        self.__target = data['Config']['Target']
        self.__toolchain = data['Config']['Toolchain']
        self.__optimization_goal = data['Config']['OptimizationGoal']
        self.__benchmark_category = data['Test']['Category']
        self.__benchmark = data['Test']['Test']
        self.__base_dir = Path(os.getcwd())
        self.__job_base_dir = None
        self.__sources_dir = None
        self.__build_dir = None
        self.__tmp_dir = None
        self.__template_dir = Path(config['Benchmarks']['Template'])
        self.__should_queue_benchmark_on_success = config['Benchmarks']['QueueBenchmarkAfterBuild']
        self.__system_config = config
        self.__specific_options = {'CMakeArgs': {}}
        if self.__benchmark_category['Id'] in config['Benchmarks']['Config']:
            if self.__benchmark['Id'] in config['Benchmarks']['Config'][self.__benchmark_category['Id']]:
                self.__specific_options.update(config['Benchmarks']['Config'][self.__benchmark_category['Id']][self.__benchmark['Id']])

        self.__job_base_dir = self.__base_dir.joinpath(self.__benchmark_category['Id'],
                               self.__benchmark['Id'],
                               self.__target['Id'] +
                               '-' + self.__toolchain['Id'] +
                               '-' + self.__optimization_goal)

        self.__out_dir = self.__job_base_dir.joinpath(BuildJob.OUTPUT_DIR_NAME)
        self.__build_log_dir = self.__out_dir.joinpath('build', 'log')
        self.__artifact_dir = self.__out_dir.joinpath('build', 'artifact')
        self.__sources_dir = self.__job_base_dir.joinpath(BuildJob.SOURCES_DIR_NAME)


    def _id(self):
        return '{}.{}|{}|{}|{}'.format(
            self.__benchmark_category['Id'],
            self.__benchmark['Id'],
            self.__target['Id'],
            self.__toolchain['Id'],
            self.__optimization_goal)

    def _prepare(self):
        # Create job directory
        self.__job_base_dir.mkdir(parents=True, exist_ok=True)

        # Copy template
        shutil.rmtree(self.__sources_dir, ignore_errors=True)
        shutil.copytree(self.__template_dir, self.__sources_dir,
                        ignore=shutil.ignore_patterns('.*', 'out'))

        # Copy benchmark
        copy_tree(self.__benchmark['Path'], str(self.__sources_dir.joinpath('benchmark').absolute()))

        # Create output folder structure
        self.__build_log_dir.mkdir(parents=True, exist_ok=True)
        self.__artifact_dir.mkdir(parents=True, exist_ok=True)

        # Create build directory and change change into
        self.__tmp_dir = self.__job_base_dir.joinpath('tmp')
        self.__tmp_dir.mkdir(parents=True, exist_ok=True)
        self.__build_dir = self.__tmp_dir.joinpath(BuildJob.BUILD_DIR_NAME)
        shutil.rmtree(self.__build_dir, ignore_errors=True)
        self.__build_dir.mkdir(exist_ok=True)
        # don't chdir anymore!

        return True

    def _execute(self):
        if not self.__cmake():
            return False, None, None

        if not self.__build():
            return False, None, None

        # Copy artifacts
        shutil.copy(self.__build_dir.joinpath('template.elf'), self.__artifact_dir)
        shutil.copy(self.__build_dir.joinpath('template.map'), self.__artifact_dir)

        # Generate benchmark job
        self._succeeded()
        return True, self.get_follow_up_jobs(), None

    def get_follow_up_jobs(self):
        if self.was_successful() and self.__should_queue_benchmark_on_success:
            return (BenchmarkJob(BuildInfo(self), self.__system_config),)
        else:
            return list()

    def __cmake(self):
        def resolve_vars(value):
            def var_value(var):
                if var in self.__target:
                    return self.__target[var]
                if var == 'OptimizationGoal':
                    return self.__optimization_goal
                return str()

            return re.sub(r'\$([a-zA-Z0-9_]+)', lambda m: var_value(m.group(1)), str(value))

        def cmake_define_var(arg, value):
            return '-D{}={}'.format(
                arg,
                resolve_vars(value))

        args = [*[cmake_define_var(arg, value) for (arg, value) in self.__toolchain['CMakeArgs'].items()],
                cmake_define_var('TEST_CATEGORY', self.__benchmark_category['Id']),
                cmake_define_var('TEST_BENCHMARK', self.__benchmark['Id']),
                cmake_define_var('CMAKE_VERBOSE_MAKEFILE', 'True'),
                *[cmake_define_var(arg, value) for (arg, value) in self.__specific_options['CMakeArgs'].items()],
                '-S', self.__sources_dir,
                '-B', self.__build_dir]

        stdout_logfile = self.__build_log_dir.joinpath('cmake.stdout.log')
        stderr_logfile = self.__build_log_dir.joinpath('cmake.stderr.log')

        return self._run_tool('/usr/bin/cmake', args, stdout_logfile, stderr_logfile) == 0

    def __build(self):
        stdout_logfile = self.__build_log_dir.joinpath('build.stdout.log')
        stderr_logfile = self.__build_log_dir.joinpath('build.stderr.log')

        return self._run_tool('/usr/bin/cmake', [
                '--build', self.__build_dir,
                '--', '-j8', 'VERBOSE=1'], 
                stdout_logfile,
                stderr_logfile) == 0

    def get_artifact(self):
        return self.__artifact_dir.joinpath('template.elf')

    def get_target_info(self):
        return self.__target

    def get_job_dir(self):
        return self.__job_base_dir

    def get_source_dir(self):
        return self.__sources_dir

    def get_benchmark_category(self):
        return self.__benchmark_category

    def get_benchmark(self):
        return self.__benchmark

    def get_toolchain(self):
        return self.__toolchain

    def get_optimization_goal(self):
        return self.__optimization_goal

    def _should_skip(self, args):
        return args.only_benchmarks


class BuildInfo:
    def __init__(self, producer: BuildJob):
        self.__producer = producer
        self.__artifact = producer.get_artifact()

    def get_artifact(self):
        return self.__artifact

    def get_build_job(self):
        return self.__producer


class BenchmarkJob(Job):
    BEGIN_TEST_TIMEOUT = 30
    SINGLE_TEST_TIMEOUT = 30

    def __init__(self, build: BuildInfo, config):
        super(BenchmarkJob, self).__init__()
        self.__build = build
        self.__base_dir = Path(os.getcwd())
        self.__job_base_dir = self.__build.get_build_job().get_job_dir()
        self.__benchmark_log_dir = None
        self.__system_config = config
        self.__required_successes = config['Benchmarks']['RequiredSuccesses']
        self.__repetition_count = config['Benchmarks']['Repetitions']

    def _id(self):
        return '{}.{}|{}|{}|{}'.format(
            self.__build.get_build_job().get_benchmark_category()['Id'],
            self.__build.get_build_job().get_benchmark()['Id'],
            self.__build.get_build_job().get_target_info()['Id'],
            self.__build.get_build_job().get_toolchain()['Id'],
            self.__build.get_build_job().get_optimization_goal())

    def _prepare(self):
        self.__benchmark_log_dir = self.__job_base_dir.joinpath('out', 'benchmark', 'log')
        self.__benchmark_log_dir.mkdir(parents=True, exist_ok=True)

        # Flash target
        if not self.__flash_target():
            return False

        return True

    def __benchmark(self, no):
        # Prepare tty
        tty_filename = self.__build.get_build_job().get_target_info()['TTY']
        with serial.Serial(tty_filename,
                           baudrate=self.__build.get_build_job().get_target_info()['TTYSpeed'],
                           bytesize=serial.EIGHTBITS,
                           parity=serial.PARITY_NONE,
                           stopbits=serial.STOPBITS_ONE,
                           timeout=BenchmarkJob.SINGLE_TEST_TIMEOUT) as tty, \
                open(self.__benchmark_log_dir.joinpath('run_{}.stdout.log'.format(no)), 'wb+') as stdout_logfile, \
                open(self.__benchmark_log_dir.joinpath('run_{}.stderr.log'.format(no)), 'w+') as stderr_logfile:

            self.__reset_target()

            if not self.__wait_until_begin_of_benchmark(tty, stdout_logfile, stderr_logfile):
                stderr_logfile.write('[Error] Timeout receiving begin of benchmark marker.')
                return False

            got_end_marker = False
            while not got_end_marker:
                content = tty.readall()
                if not content:
                    stderr_logfile.write('[Error] Premature end of content stream (receive timeout).')
                    return False
                stdout_logfile.write(content)
                if b'END OF BENCHMARKS' in content:
                    got_end_marker = True
            self._succeeded()
            return True

    def get_follow_up_jobs(self):
        return list()

    def _execute(self):
        count_success = 0
        for no in range(self.__repetition_count):
            if self.__benchmark(no):
                count_success += 1

        if count_success < self.__required_successes:
            self.__dump_backtrace()

        return count_success >= self.__required_successes, list(), \
               '{}/{}'.format(count_success, self.__repetition_count)

    def __wait_until_begin_of_benchmark(self, tty, stdout_logfile, stderr_logfile):
        timeout_counter = 0
        while True:
            line = tty.readline()
            if len(line) == 0:
                timeout_counter += 1
                if timeout_counter >= BenchmarkJob.BEGIN_TEST_TIMEOUT:
                    return False
                continue

            if b'BENCHMARK INFO' in line:
                stdout_logfile.write(line)
                return True
            stderr_logfile.write('[Warning] Got {} bytes of unexpected data before begin of benchmark marker:'
                                 .format(len(line)))
            try:
                stderr_logfile.write(str(line, 'utf-8'))
            except:
                stderr_logfile.write('hexdump: '.format(' '.join(['{:02x}'.format(x) for x in line])))

    def __flash_target(self):
        stdout_logfile = self.__benchmark_log_dir.joinpath('flash.stdout.log')
        stderr_logfile = self.__benchmark_log_dir.joinpath('flash.stderr.log')

        flash_config = self.__build.get_build_job().get_source_dir().joinpath(
            self.__build.get_build_job().get_target_info()['FlashConfig'])
        artifact = self.__build.get_artifact().relative_to(os.getcwd())

        return self._run_tool('/mingw64/bin/openocd', [
            '-c', 'tcl_port disabled',
            '-s', '/usr/share/openocd/scripts',
            '-f', str(flash_config),
            '-c', 'program "{}"'.format(artifact),
            '-c', 'init',
            '-c', 'reset init',
            '-c', 'reset halt',
            '-c', 'exit'],
                              stdout_logfile, stderr_logfile,
                              env={'SERIAL': self.__build.get_build_job().get_target_info()['STLinkSerial']}) == 0

    def __reset_target(self):
        stdout_logfile = self.__benchmark_log_dir.joinpath('reset.stdout.log')
        stderr_logfile = self.__benchmark_log_dir.joinpath('reset.stderr.log')

        flash_config = self.__build.get_build_job().get_source_dir().joinpath(
            self.__build.get_build_job().get_target_info()['FlashConfig'])

        return self._run_tool('/mingw64/bin/openocd', [
            '-c', 'tcl_port disabled',
            '-s', '/usr/share/openocd/scripts',
            '-f', str(flash_config),
            '-c', 'init',
            '-c', 'reset init',
            '-c', 'reset run',
            '-c', 'exit'],
                              stdout_logfile, stderr_logfile,
                              env={'SERIAL': self.__build.get_build_job().get_target_info()['STLinkSerial']}) == 0

    def __debug_target(self):
        stdout_logfile = self.__benchmark_log_dir.joinpath('debug.stdout.log')
        stderr_logfile = self.__benchmark_log_dir.joinpath('debug.stderr.log')

        flash_config = self.__build.get_build_job().get_source_dir().joinpath(
            self.__build.get_build_job().get_target_info()['FlashConfig'])

        gdb_port = random.randint(10000, 50000);
        telnet_port = random.randint(10000, 50000);

        p = self._run_tool('/mingw64/bin/openocd', [
            '-c', 'tcl_port disabled',
            '-s', '/usr/share/openocd/scripts',
            '-f', str(flash_config),
            '-c', 'gdb_port {}'.format(gdb_port),
            '-c', 'telnet_port {}'.format(telnet_port),
            '-c', 'init',
            '-c', 'reset init'],
                              stdout_logfile, stderr_logfile,
                              wait_for_completion=False,
                              env={'SERIAL': self.__build.get_build_job().get_target_info()['STLinkSerial']})
        return (gdb_port, telnet_port, p)

    def __dump_backtrace(self):
        stdout_logfile = self.__benchmark_log_dir.joinpath('gdb.debug.stdout.log')
        stderr_logfile = self.__benchmark_log_dir.joinpath('gdb.debug.stderr.log')

        gdb_port, telnet_port, debug_process = self.__debug_target()

        gdb = self._run_tool('/c/Program Files (x86)/GNU Tools Arm Embedded/9 2019-q4-major/bin/arm-none-eabi-gdb', [
            '--nx',
            '-iex', 'target remote localhost:{}'.format(gdb_port),
            '-ex', 'monitor reset init',
            '-ex', 'continue',
            '-ex', 'backtrace full',
            '-ex', 'info registers',
            '-ex', 'quit',
            self.__build.get_artifact()],
                        stdout_logfile, stderr_logfile,
                        wait_for_completion=False)

        try:
            gdb.wait(10)
        except subprocess.TimeoutExpired:
            telnet = telnetlib.Telnet(host="localhost", port=telnet_port)
            telnet.write(b"halt\n")
            gdb.wait(2)
            telnet.write(b"exit\n")
        
        if debug_process.poll() is None:
            debug_process.kill()
        if gdb.poll() is None:
            gdb.kill()


    def get_job_dir(self):
        return self.__job_base_dir

    def get_target_id(self):
        return self.__build.get_build_job().get_target_info()['Id']


def build_joblist(cfg, whitelist):
    queue = JobQueue()

    source_path = Path(config['Benchmarks']['SourcePath'])
    tests = [
        {'Id': category_path.name, 'Path': category_path,
         'Tests': [{'Id': test_path.name, 'Path': test_path} for test_path in category_path.iterdir() if
                   not test_path.joinpath('DISABLED').exists()]}
        for category_path in source_path.iterdir()
        if not category_path.joinpath('DISABLED').exists()]
    target_toolchain_permutation = [{'Target': target,
                                     'Toolchain': toolchain,
                                     'OptimizationGoal': goal}
                                    for toolchain in config['Toolchains'] if toolchain['Enabled']
                                    for goal in config['Benchmarks']['OptimizationGoals']
                                    for target in config['Targets'] if target['Enabled']]

    def is_disabled(build_config, category, test):
        if category['Id'] not in config['Benchmarks']['Config']:
            return False

        if test['Id'] not in config['Benchmarks']['Config'][category['Id']]:
            return False

        if 'Disabled' not in config['Benchmarks']['Config'][category['Id']][test['Id']]:
            return False

        if 'ToolchainId' in config['Benchmarks']['Config'][category['Id']][test['Id']]['Disabled']:
            DisabledToolchains = config['Benchmarks']['Config'][category['Id']][test['Id']]['Disabled']['ToolchainId']
            if build_config['Toolchain']['Id'] in DisabledToolchains:
                return True

        return False

    def is_whitelisted(whitelist, category, test):
        def matches(wl_entry, category, test):
            cat_begin, test_begin = wl_entry.split('.')
            return category['Id'].startswith(cat_begin) and \
                test['Id'].startswith(test_begin) #TODO wildcard?

        if len(whitelist) == 0:
            return True
        return any(matches(entry, category, test) for entry in whitelist)

    queue.add_jobs(
        *[BuildJob({'Config': build_config, 'Test': {'Category': category, 'Test': test}}, cfg) for category in tests for test
          in
          category['Tests'] for build_config in
          target_toolchain_permutation
          if not is_disabled(build_config, category, test) and is_whitelisted(whitelist, category, test)])
    return queue


def read_args():
    parser = argparse.ArgumentParser(description='Automated build-system and benchmarking scheduler.')
    parser.add_argument('-t', '--terminate-on-error', action='store_true', help='Immediately abort if an error occurs.')
    parser.add_argument('-c', '--clean', action='store_true', help='Wipe out previous progress.')
    parser.add_argument('-r', '--skip-completed', action='store_true', help='Skip already completed jobs.')
    parser.add_argument('-o', '--option', action='append', help='Overwrite configuration option.')
    parser.add_argument('-v', '--verbose', action='store_true', help='Print more verbose output, e.g. show skipped jobs.')
    parser.add_argument('--eta', action='store_true', help='Show estimated remaining time.')
    parser.add_argument('--detail', action='store_true', help='Print more detailed output, e.g. include Success/Failure/Skipped counts.')
    parser.add_argument('--runner-loop-wait', type=int, default=60, help='Wait-delay of a runners loop if its work queue is exhausted but not the overal workqueue.')
    parser.add_argument('-b', '--only-benchmarks', action='store_true', help='Only run open benchmark jobs. Bo not run new build jobs.')
    parser.add_argument('benchmarks', metavar='BENCHMARK', nargs='*', default=list(), help='An optional list of benchmarks to perform.')
    parser.add_argument('--runtype', help='Selects the type of benchmark todo (either time or stack-usage).', default='time')
    return parser.parse_args()


def read_config(args):
    class LogArgs:
        def __init__(self):
            self.debug = False
            self.verbose = False
            self.quiet = True
    log = ConsolePrinter(LogArgs())
    y = get_yaml_editor()
    config = get_yaml_data(y, log, 'config.yml')

    processor = Processor(log, config)

    if args.option is not None:
        for option in args.option:
            key, value = option.rsplit("=", 1)
            yaml_path = YAMLPath(key)
            yaml_value = yaml.safe_load(value)
            processor.set_value(yaml_path, yaml_value)

    return config


class StatusPrinter:
    def __init__(self, args, jobs):
        self.jobs = jobs
        self.args = args
        self.header_line = '{} {} {}'.format(
            'Processing',
            colored('{}', 'yellow'),
            'jobs')

        self.detail = '[{}/{}/{}]'.format(
            colored('{:4}', 'green'),
            colored('{:4}', 'red'),
            colored('{:4}', 'blue')) if args.detail else ''

        self.eta = '[{}]'.format('{}') if args.eta else ''

        self.status_line = '{} [{}/{}]{}{} {}'.format(
            colored('{:>6.2f}%', 'cyan'),
            colored('{:4}', 'white'),
            colored('{:4}', 'white'),
            self.detail,
            self.eta,
            colored('{}', 'yellow'))

        self.succeeded, self.failed, self.skipped = 0, 0, 0
        self.eta_info = list()
        self.last_no = 0

    def begin_job(self):
        self.start = datetime.datetime.now()
        self.last_no += 1
        return self.last_no

    def print_job_status(self, job, no, result_text, result_color, was_skipped = False, message = None):
        if (not was_skipped) or self.args.verbose:
            out = list(((100 * no / len(self.jobs)),
                no,
                len(self.jobs)))
            if self.args.detail:
                out += list((self.succeeded, self.failed, self.skipped))
            if self.args.eta:
                if len(self.eta_info) == 0:
                    out.append('?')
                else:
                    avg_now = sum([time for _,time in self.eta_info], datetime.timedelta(seconds=0))/len(self.eta_info)
                    jobs_todo = len(self.jobs) - no
                    eta = avg_now * jobs_todo
                    out.append(eta)
            out.append(job)
            if message:
                status_line = '{}: {} ({})'.format(self.status_line, colored(result_text, result_color), message)
            else:
                status_line = '{}: {}'.format(self.status_line, colored(result_text, result_color))
            print(status_line.format(*out), flush=True)

    def job_result_skipped(self, job, no):
        self.skipped += 1
        if self.args.verbose:
           self.print_job_status(job, no, 'SKIP', 'blue', was_skipped=True)

    def job_result_succeeded(self, job, no, message = None):
        self.succeeded += 1
        self.print_job_status(job, no, 'OK', 'green', message)
        self.eta_info.append((job, datetime.datetime.now() - self.start))

    def job_result_failed(self, job, no, message = None):
        self.failed += 1
        self.print_job_status(job, no, 'FAIL', 'red', message)


class Runner:
    def __init__(self, id, args, jobs, pred, status_printer):
        self.id = id
        self.args = args
        self.all_jobs = jobs
        self.status_printer = status_printer
        self.pred = pred
        self.runtype = self.args.runtype

    def run(self):
        interested_jobs = list(filter(self.pred, self.all_jobs.items()))
        todo_jobs = list(filter(lambda job: not job.has_run(), interested_jobs))
        done_jobs = list(filter(lambda job: job.has_run(), interested_jobs))
        global_done_jobs = list(filter(lambda job: job.has_run(), self.all_jobs.items()))
        print('[{}] has {} jobs todo ({} interested, {} done, {}/{} all)'.format(
            self.id, 
            len(todo_jobs),
            len(interested_jobs),
            len(done_jobs),
            len(global_done_jobs),
            len(self.all_jobs.items())))
        for job in todo_jobs:
            no = status_printer.begin_job()

            if job.should_skip(args):
                status_printer.job_result_skipped(job, no)
                job._has_run = True # hacky hacky hack...
                self.all_jobs.add_jobs(*job.get_follow_up_jobs())
                continue
                
            try:
                success, follow_up_jobs, message = job.run(self.runtye)
            except:
                success, follow_up_jobs, message = \
                    False, \
                    list(), \
                    '{} ({})'.format(sys.exc_info()[0].__name__, sys.exc_info()[1])

            if success:
                status_printer.job_result_succeeded(job, no, message)
                self.all_jobs.add_jobs(*follow_up_jobs)
            else:
                status_printer.job_result_failed(job, no, message)
            if not success and args.terminate_on_error:
                print('Terminating due to previous error.')
                break


class RunnerThread(threading.Thread):
    def __init__(self, id, args, jobs, pred, status_printer, loop_wait_time):
        super(RunnerThread, self).__init__()
        self.loop_wait_time = loop_wait_time
        self.runner = Runner(id, args, jobs, pred, status_printer)

    def run(self):

        while len(list(filter(lambda j: not j.has_run(), jobs.items()))) > 0:
            self.runner.run()
            time.sleep(self.loop_wait_time)


if __name__ == '__main__':
    args = read_args()
    config = read_config(args)

    new_wd = Path(config['Benchmarks']['WorkingDir'])

    if args.clean:
        print('Running cleanup')
        shutil.rmtree(new_wd)

    new_wd.mkdir(parents=True, exist_ok=True)
    os.chdir(Path(config['Benchmarks']['WorkingDir']))
    working_dir = os.getcwd()

    jobs = build_joblist(config, args.benchmarks)
    status_printer = StatusPrinter(args, jobs)

    def create_runner_for_jobs(id, pred):
        return RunnerThread(id, args, jobs, pred, status_printer, args.runner_loop_wait)

    runners = list()
    runners.append(
      create_runner_for_jobs(
        'BuildRunner', 
        lambda j: isinstance(j, BuildJob))) # Process Build Jobs
    
    class ByTarget:
        def __init__(self, target):
            self.target_id = target['Id']

        def __call__(self, job):
            return isinstance(job, BenchmarkJob) and \
                job.get_target_id() == self.target_id


    runners.extend(
        list([
            create_runner_for_jobs(
              'BenchmarkRunner@' + target['Id'], 
              ByTarget(target)) # Process Benchmark Jobs, one Runner per Target
            for target in config['Targets'] 
            if target['Enabled']
        ]))

    [runner.start() for runner in runners]
    [runner.join() for runner in runners]

    print('End.')
