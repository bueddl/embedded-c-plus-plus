#!/bin/bash

cd $(dirname $0)/..
find tmp -maxdepth 6 -type f -iname BuildJob -print -exec util/del.sh {} \;
