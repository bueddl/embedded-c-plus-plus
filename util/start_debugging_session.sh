#!/bin/bash

# <cat> <bench> <tgt> <cc> <opt>
# 01 01 L031 Clang size

if [[ $# -ne 5 ]]; then
  echo "Usage: $0 <cat> <bench> <tgt> <cc> <opt>"
  exit 1
fi

arg_cat=$1
arg_bench=$2
arg_tgt=$3
arg_cc=$4
arg_opt=$5

basedir=/c/BA/Research/template
tmpdir=$basedir/tmp

dir=$(echo $tmpdir/$arg_cat*/$arg_bench*/STM32$arg_tgt-$arg_cc-$arg_opt)
if test ! -d "$dir"; then
  echo "Directory does not exist"
  exit 1
fi

notesfile=$basedir/notes/last.txt

session="dev"

tmux kill-session $session
tmux new-session -d -s $session

tmux setw -g mouse on
tmux set -g pane-border-status top

function rename_pane() {
  tmux send-keys -t $1 "printf '\033]2;%s\033\\' '$2'" C-m 'clear' C-m
}

tmux rename-window -t 0 'Debugging'

# Window: Debugging
# +---------+---------+-------+
# |         | 1Telnet | Notes |
# |         +---------+  (4)  |
# |   GDB   | 2Shell  +-------|
# |   (0)   +---------+ Output|
# |         | 3OpenOCD|  (5)  |
# +---------+---------+-------+


tmux send-keys -t 'Debugging' 'zsh' C-m
rename_pane 'Debugging' 'GDB'

tmux split-window -h -t 'Debugging'
tmux send-keys -t 'Debugging' 'zsh' C-m
rename_pane 'Debugging' 'Control OpenOCD'

tmux split-window -h -t 'Debugging'
tmux send-keys -t 'Debugging' 'zsh' C-m
rename_pane 'Debugging' 'Notes'


tmux select-pane -t 1

tmux split-window -v -t 'Debugging'
tmux send-keys -t 'Debugging' 'zsh' C-m
rename_pane 'Debugging' 'Shell'

tmux split-window -v -t 'Debugging'
tmux send-keys -t 'Debugging' 'zsh' C-m
rename_pane 'Debugging' 'OpenOCD'


tmux select-pane -t 4

tmux split-window -v -t 'Debugging'
tmux send-keys -t 'Debugging' 'zsh' C-m
rename_pane 'Debugging' 'Output (Serial)'


tmux select-pane -t 3

if [[ "$arg_tgt" == "L031" ]]; then
  SERIAL=066AFF494849887767173322
  TTY=/dev/ttyS2
  TTYSPEED=115200
  CONFIG=flash/openocd/STM32L0xx/openocd.cfg

elif [[ "$arg_tgt" == "L432" ]]; then
  SERIAL=066AFF545051717867162827
  TTY=/dev/ttyS4
  TTYSPEED=115200
  CONFIG=flash/openocd/STM32L4xx/openocd.cfg

elif [[ "$arg_tgt" == "F031" ]]; then
  SERIAL=066BFF323535474B43134813
  TTY=/dev/ttyS7
  TTYSPEED=115200
  CONFIG=flash/openocd/STM32F0xx/openocd.cfg

elif [[ "$arg_tgt" == "F207" ]]; then
  SERIAL=0670FF495051717867114533
  TTY=/dev/ttyS5
  TTYSPEED=115200
  CONFIG=flash/openocd/STM32F2xx/openocd.cfg

elif [[ "$arg_tgt" == "F446" ]]; then
  SERIAL=0676FF535156827867092859
  TTY=/dev/ttyS3
  TTYSPEED=115200
  CONFIG=flash/openocd/STM32F4xx/openocd.cfg

elif [[ "$arg_tgt" == "F722" ]]; then
  SERIAL=066EFF515455777867052307
  TTY=/dev/ttyS6
  TTYSPEED=38400
  CONFIG=flash/openocd/STM32F7xx/openocd.cfg

elif [[ "$arg_tgt" == "L4P5" ]]; then
  Enabled: False
  SERIAL=0669FF343039564157202134
  TTY=/dev/ttyS8
  TTYSPEED=115200
  CONFIG=flash/openocd/STM32L4xx/openocd.cfg

elif [[ "$arg_tgt" == "F303" ]]; then
  SERIAL=066CFF575251717867161446
  TTY=/dev/ttyS9
  TTYSPEED=115200
  CONFIG=flash/openocd/STM32F3xx/openocd.cfg

elif [[ "$arg_tgt" == "F746" ]]; then
  SERIAL=0668FF343039564157243725
  TTY=/dev/ttyS10
  TTYSPEED=38400
  CONFIG=flash/openocd/STM32F7xx/openocd.cfg
fi
tmux send-keys -t 'Debugging' \
  "cd $dir" C-m \
  "SERIAL=$SERIAL " \
  "/mingw64/bin/openocd " \
  "-c 'tcl_port disabled' " \
  "-s /usr/share/openocd/scripts " \
  "-f $dir/sources/$CONFIG " \
  "-c 'init' " \
  "-c 'program tmp/build/template.elf' " \
  "-c 'reset halt' " \
  C-m

tmux select-pane -t 4
tmux send-keys -t 'Debugging' \
  "cd $dir" C-m \
  "nano '$notesfile'" C-m

tmux select-pane -t 2
tmux send-keys -t 'Debugging' \
  "cd $dir/tmp/build" C-m \
  "clear" C-m

tmux select-pane -t 5
tmux send-keys -t 'Debugging' \
  "stty -F $TTY $TTYSPEED cs8 -cstopb -parenb" C-m \
  "cat $TTY" C-m

tmux select-pane -t 1
tmux send-keys -t 'Debugging' \
  "sleep 2" C-m \
  "cd $dir" C-m \
  "telnet localhost 4444" C-m

tmux select-pane -t 0
tmux send-keys -t 'Debugging' \
  "cd $dir" C-m \
  "sleep 2" C-m \
  "arm-none-eabi-gdb-py tmp/build/template.elf " \
  "-nh " \
  "-x '$basedir/.gdbinit' " \
  "-ex 'target remote localhost:3333' " \
  "-ex 'monitor reset init' " C-m

tmux attach-session -t $session:0
