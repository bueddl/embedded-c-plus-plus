#!/bin/bash

# /template
# /data/raw
# /data/collected
# /data/evaluations


if [[ $# -ne 1 ]]; then
  echo "Usage: $0 <destination>"
  exit 1
fi

cd $(dirname $0)
dest="$1"

if test ! -d "$dest"; then
  echo "Destination is not a directory"
  exit 1
fi

basedir=..
tmpdir="$basedir/tmp"

for category_dir in $tmpdir/*; do
  category=$(basename "$category_dir")
  if test ! -d "$category_dir"; then
    continue
  fi
  
  echo "=== $category ==="
  for solution_dir in $category_dir/*; do
    solution=$(basename "$solution_dir")
    echo "-- Packing $solution"
    destdir="$dest/data/raw/$category"
    test ! -d $destdir && mkdir -p $destdir
    destfile="$destdir/$solution.tar"
    tar -cf $destfile -C $solution_dir .
  done
done
