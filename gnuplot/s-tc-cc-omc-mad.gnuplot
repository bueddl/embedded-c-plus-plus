set terminal svg \
  noenhanced \
  font "arial,13" \
  fontscale 1.0 \
  size 350,800
  
set style histogram rowstacked
set style data histogram
set datafile missing '-'

set style fill solid border rgb "black"
set auto x

if (y_logscale == 1) {
  set logscale y 10
  #set yrange [1:*]
  set auto y
} else {
  set yrange [0:*]
}
  
set ylabel "Speicherbedarf / Bytes" 
set ylabel offset 36,3,0
set ylabel rotate by 90 right
set zlabel rotate by 90 right
set cblabel rotate by 90 right
set rlabel rotate by 90 right

set xtics rotate by 90 right
set xtics scale 0
set ytics offset 27,0
set ytics rotate by 90 right

set boxwidth 0.7 absolute

set rmargin 6
set bmargin 6

set nokey 

set grid ytics mytics
set style line 12 lc rgb '#c0c0c0' lt -1 lw 1
set grid back ls 12
set grid
set datafile separator ","

#set title "Test" rotate by 90 right

#plot newhistogram "ROM", filename using 2:xtic(1) title col, \
#                               '' using 3 title col, \
#                               '' using 4 title col, \
#     newhistogram "RAM",       '' using 5:xtic(1) title col, \
#                               '' using 6 title col

plot newhistogram "", filename \
                               using "size_rom":xtic(1) title col, \
                            '' using "size_ram" title col, \
     newhistogram "",     \
                            '' using "speed_rom":xtic(1) title col, \
                            '' using "speed_ram" title col,

set output outfile

cats = "Size Speed"
columnsA = "ROM RAM"
columnsB = "ROM RAM"

kx1=0.5*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
kx2=0.95*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN

if (y_logscale == 1) {
  ky1=10**((0.95+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
  ky2=10**((0.7+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
} else {
  ky1=(0.95+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
  ky2=(0.7+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
}

set style rectangle fc rgb '#ffffff' fs solid 1.0 noborder
set object rectangle from kx1,ky1 to kx2,ky2

kxb=0.7
kxmt=(kxb-0.125)*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
kyb=(1.5*0.15 - 0.225)+0.85
if (y_logscale == 1) {
  kymt=10**(kyb*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
} else {
  kymt=kyb*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
}

set label "Optimierungsmodus" center at kxmt,kymt rotate by 90 offset 0,0
  
col_counter=1
do for [j=1:words(cats)] {
  if (j == 1) {
    columns = columnsA
  } else {
    columns = columnsB
  }
  
  cat_y_off = j*0.15 - 0.225
  kyb=cat_y_off+0.85
  
  kxt=(kxb-0.05)*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
  if (y_logscale == 1) {
    kyt=10**(kyb*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
  } else {
    kyt=kyb*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
  }
  
  cattitle=word(cats,j)
  set label cattitle center at kxt,kyt rotate by 90 offset 0,0
    
  do for [i=1:words(columns)] {
    k1xb=kxb+0.06*(i-1)+0.05
    k1x=k1xb*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
    k1x1=(k1xb+0.0125)*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
    k1x2=(k1xb-0.0125)*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN

    if (y_logscale == 1) {
      k1y=10**((cat_y_off+0.85+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
      k1ty=10**((cat_y_off+0.85+0.03-0.05)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
      k1ty1=10**((cat_y_off+0.93+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
      k1ty2=10**((cat_y_off+0.82+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
    } else {
      k1y=(cat_y_off+0.85+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
      k1ty=(cat_y_off+0.85+0.03-0.05)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
      k1ty1=(cat_y_off+0.87+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
      k1ty2=(cat_y_off+0.82+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
    }
    set label word(columns, i) right at k1x,k1ty rotate by 90 offset 0,0
    set object rectangle fc lt col_counter fs solid border -1 \
      from k1x1,k1ty1 \
      to   k1x2,k1ty2
    col_counter = col_counter + 1
  }
}

replot
