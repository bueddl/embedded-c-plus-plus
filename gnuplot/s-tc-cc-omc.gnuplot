if (small == 0) {
  height=450
} else {
  height=350;
}

set terminal svg \
  noenhanced \
  font "arial,13" \
  fontscale 1.0 \
  size height,800
  
set style data histogram
set style histogram cluster gap 1 errorbars
#set style histogram rowstacked
set errorbars linecolor black

set style fill solid border rgb "black"
set auto x

if (y_logscale == 1) {
  set logscale y 10
  #set yrange [1:*]
  set auto y
} else {
  set auto y
}
  
ylabelXOff = (height + 10) / 10
set ylabel "Speicherbedarf/Bytes" 
set ylabel offset ylabelXOff,3,0
set ylabel rotate by 90 right

xticksXOff = (height - 70) / 10
set xtics rotate by 90 right
set xtics scale 0
set ytics offset xticksXOff,0
set ytics rotate by 90 right

set rmargin 6
set bmargin 12

set nokey 

set grid ytics mytics
set style line 12 lc rgb '#c0c0c0' lt -1 lw 1
set grid back ls 12
set grid
set datafile separator ","

# text_min,text_max,text_mean,rodata_min,rodata_max,rodata_mean,data_min,data_max,data_mean,stack_min,stack_max,stack_mean
plot filename \
     using (column("speed_text_mean")+column("speed_rodata_mean")): \
           (column("speed_text_min")+column("speed_rodata_min")): \
           (column("speed_text_max")+column("speed_rodata_max")):xtic(1) title "Speed", \
  '' using (column("size_text_mean")+column("size_rodata_mean")): \
           (column("size_text_min")+column("size_rodata_min")): \
           (column("size_text_max")+column("size_rodata_max")) title "Size"

kxb=0.85
kx1=(kxb-0.15)*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
kx2=(kxb+0.1)*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN

if (y_logscale == 1) {
  ky1=10**((0.95+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
  ky2=10**((0.7+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
} else {
  ky1=(0.95+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
  ky2=(0.7+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
}

set style rectangle fc rgb '#ffffff' fs solid 1.0 noborder
set object rectangle from kx1,ky1 to kx2,ky2

kxmt=(kxb-0.125)*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
kyb=(1.5*0.15 - 0.225)+0.85
if (y_logscale == 1) {
  kymt=10**(kyb*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
} else {
  kymt=kyb*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
}

set label "Optimierungsmodus" center at kxmt,kymt rotate by 90 offset 0,0 front

k1x=0.81*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
k1x1=0.83*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
k1x2=0.79*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN

if (y_logscale == 1) {
  k1y=10**((0.85+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
  k1ty=10**((0.85+0.03-0.05)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
  k1ty1=10**((0.93+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
  k1ty2=10**((0.82+0.03)*log10(GPVAL_Y_MAX/GPVAL_Y_MIN)) * GPVAL_Y_MIN
} else {
  k1y=(0.85+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
  k1ty=(0.85+0.03-0.05)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
  k1ty1=(0.93+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
  k1ty2=(0.82+0.03)*(GPVAL_Y_MAX - GPVAL_Y_MIN)+GPVAL_Y_MIN
}
set label 'Speed' right at k1x,k1ty rotate by 90 offset 0,0
set object rectangle fc lt 1 fs solid border -1 \
  from k1x1,k1ty1 \
  to   k1x2,k1ty2

k2x=0.89*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
k2x1=0.87*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
k2x2=0.91*(GPVAL_X_MAX - GPVAL_X_MIN) + GPVAL_X_MIN
k2y=k1y
k2ty=k1ty
k2ty1=k1ty1
k2ty2=k1ty2
set label 'Size' right at k2x,k2ty rotate by 90 offset 0,0
set object rectangle fc lt 2 fs solid border -1 \
  from k2x1,k2ty1 \
  to   k2x2,k2ty2

set output outfile
replot

