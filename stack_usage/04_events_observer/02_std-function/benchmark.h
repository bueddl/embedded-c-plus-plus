#include <benchmark/benchmark.h>

#include <functional>

using callback_signature = void(int some, int args);

static constexpr auto max_callbacks = 16u;

class callback_storage
{
    struct callback_slot
    {
        std::function<callback_signature> fn;
        bool in_use = false;
    };
public:

    bool install(std::function<callback_signature> fn)
    {
        int i;
        for (i = 0; slots_[i].in_use; ++i)
            if (i == max_callbacks)
                return false;
        
        slots_[i].fn = fn;
        slots_[i].in_use = 1;
        return true;
    }

    void execute(int some, int args)
    {
        for (int i = 0; i < max_callbacks; ++i) {
            if (!slots_[i].in_use)
                continue;
            
            slots_[i].fn(some, args);
        }
    }

private:
    callback_slot slots_[max_callbacks];
};
