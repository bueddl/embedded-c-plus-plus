#include "benchmark.h"

// ---

static void InstallCallbackStatefulClosure(benchmark::State &state)
{
  

  void *user;

   {
    callback_storage storage = {};

    
    
    storage.install([user](int some, int args) {
        benchmark::DoNotOptimize(some);
        benchmark::DoNotOptimize(args);
        benchmark::DoNotOptimize(user);
    });

    
    benchmark::DoNotOptimize(storage);
  }

  
}
BENCHMARK(InstallCallbackStatefulClosure);


static void ExecuteCallbackStatefulClosure(benchmark::State &state)
{
  

  void *user;

  callback_storage storage = {};
  storage.install([user](int some, int args) {
      benchmark::DoNotOptimize(some);
      benchmark::DoNotOptimize(args);
      benchmark::DoNotOptimize(user);
  });

  benchmark::DoNotOptimize(storage);

  

  
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallbackStatefulClosure);

// ---

static void InstallCallbackStatefulClosureLarge(benchmark::State &state)
{
  

  void *user;
  char value[76] = {0};

   {
    callback_storage storage = {};

    
    
    storage.install([user, value](int some, int args) {
      benchmark::DoNotOptimize(some);
      benchmark::DoNotOptimize(args);
      benchmark::DoNotOptimize(user);
      benchmark::DoNotOptimize(value);
    });

    
    benchmark::DoNotOptimize(storage);
  }

  
}
BENCHMARK(InstallCallbackStatefulClosureLarge);


static void ExecuteCallbackStatefulClosureLarge(benchmark::State &state)
{
  

  void *user;
  char value[76] = {0};

  callback_storage storage = {};
  storage.install([user, value](int some, int args) {
    benchmark::DoNotOptimize(some);
    benchmark::DoNotOptimize(args);
    benchmark::DoNotOptimize(user);
    benchmark::DoNotOptimize(value);
  });

  benchmark::DoNotOptimize(storage);

  

  
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallbackStatefulClosureLarge);

// ---

#if BENCHMARK_CC != CC_ARMCC

#include <functional>

void bind_callback(int some, int args, void *user) 
{
  benchmark::DoNotOptimize(some);
  benchmark::DoNotOptimize(args);
  benchmark::DoNotOptimize(user);
}

static void InstallCallbackBindCallWrapper(benchmark::State &state)
{
  

  void *user;

   {
    callback_storage storage = {};

    

    using namespace std::placeholders;
    
    storage.install(std::bind(bind_callback, _1, _2, nullptr));

    
    benchmark::DoNotOptimize(storage);
  }

  
}
BENCHMARK(InstallCallbackBindCallWrapper);


static void ExecuteCallbackBindCallWrapper(benchmark::State &state)
{
  

  callback_storage storage = {};
  using namespace std::placeholders;  
  storage.install(std::bind(bind_callback, _1, _2, nullptr));

  benchmark::DoNotOptimize(storage);

  

  
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallbackBindCallWrapper);

#endif

// ---

class Callback
{
public:
  int state;

  void operator()(int some, int args) noexcept /* not const due to userstate */
  {
    benchmark::DoNotOptimize(some);
    benchmark::DoNotOptimize(args);
    benchmark::DoNotOptimize(state);
    benchmark::DoNotOptimize(this);
  }
};

static void InstallCallbackFunctionObject(benchmark::State &state)
{
  

  void *user;

   {
    callback_storage storage = {};

    
    
    Callback callback{42};
    storage.install(callback);

    
    benchmark::DoNotOptimize(storage);
  }

  
}
BENCHMARK(InstallCallbackFunctionObject);


static void ExecuteCallbackFunctionObject(benchmark::State &state)
{
  

  callback_storage storage = {};
  Callback callback{42};
  storage.install(callback);

  benchmark::DoNotOptimize(storage);

  

  
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallbackFunctionObject);

// ---
