#include "benchmark.h"

// ---

static void InstallCallbackStatefulClosureLarge(benchmark::State &state)
{
  

  void *user;
  char value[76] = {0};

   {
    callback_storage storage = {};

    
    
    storage.install([user, value](int some, int args) {
      benchmark::DoNotOptimize(some);
      benchmark::DoNotOptimize(args);
      benchmark::DoNotOptimize(user);
      benchmark::DoNotOptimize(value);
    });

    
    benchmark::DoNotOptimize(storage);
  }

  
}
BENCHMARK(InstallCallbackStatefulClosureLarge);


static void ExecuteCallbackStatefulClosureLarge(benchmark::State &state)
{
  

  void *user;
  char value[76] = {0};

  callback_storage storage = {};
  storage.install([user, value](int some, int args) {
    benchmark::DoNotOptimize(some);
    benchmark::DoNotOptimize(args);
    benchmark::DoNotOptimize(user);
    benchmark::DoNotOptimize(value);
  });

  benchmark::DoNotOptimize(storage);

  

  
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallbackStatefulClosureLarge);

// ---
