#include <benchmark/benchmark.h>

class callback
{
public:
    virtual ~callback() = default;

    virtual void execute(int some, int args) = 0;
};

static constexpr auto max_callbacks = 16u;

class callback_storage
{
    struct callback_slot
    {
        callback *cb = nullptr;
        bool in_use = false;
    };
public:
    bool install(callback &cb)
    {
        int i;
        for (i = 0; slots_[i].in_use; ++i)
            if (i == max_callbacks)
                return false;
        
        slots_[i].cb = &cb;
        slots_[i].in_use = 1;
        return true;
    }

    void execute(int some, int args)
    {
        for (int i = 0; i < max_callbacks; ++i) {
            if (!slots_[i].in_use)
                continue;
            
            slots_[i].cb->execute(some, args);
        }
    }

private:
    callback_slot slots_[max_callbacks];
};
