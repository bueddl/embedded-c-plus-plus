#include "benchmark.h"

class my_callback : public callback
{
public:
    void execute(int some, int args) override
    {
        dummy_work();
        benchmark::DoNotOptimize(some);
        benchmark::DoNotOptimize(args);
        benchmark::DoNotOptimize(this);
    }
};

static void InstallCallback(benchmark::State &state)
{
  

   {
        callback_storage storage;

    
    
    my_callback my_cb;
      storage.install(my_cb);

    
    benchmark::DoNotOptimize(storage);
  }

  
}
BENCHMARK(InstallCallback);


static void ExecuteCallback(benchmark::State &state)
{
  

    callback_storage storage;
  my_callback my_cb;
  storage.install(my_cb);

  benchmark::DoNotOptimize(storage);

  

  
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallback);