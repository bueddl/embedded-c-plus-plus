#include <benchmark/benchmark.h>

typedef void (*callback_fn)(int some, int args, void *user);

#define MAX_CALLBACKS 16

typedef struct callback
{
    callback_fn function;
    void *user;
    int in_use;
} callback_t;

typedef struct callback_storage
{
    callback_t slots[MAX_CALLBACKS];
} callback_storage_t;

int callback_install(callback_storage_t *storage, callback_fn function, void *user);

void callback_execute(callback_storage_t *storage, int some, int args);
