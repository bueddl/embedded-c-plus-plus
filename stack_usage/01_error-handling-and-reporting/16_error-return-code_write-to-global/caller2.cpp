#include "benchmark.h"

int caller2(bool do_err)
{
  Dtor d;
  int e = caller3(do_err);
  if(e) 
    return e;
  global_int = 0;
  return 0;
}
