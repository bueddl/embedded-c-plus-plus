#include "benchmark.h"

int caller6(bool do_err)
{
  Dtor d;
  int e = caller7(do_err);
  if(e) 
    return e;
  global_int = 0;
  return 0;
}
