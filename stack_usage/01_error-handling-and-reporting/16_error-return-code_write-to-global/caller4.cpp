#include "benchmark.h"

int caller4(bool do_err)
{
  Dtor d;
  int e = caller5(do_err);
  if(e) 
    return e;
  global_int = 0;
  return 0;
}
