#include "benchmark.h"


int global_int = 0;

static void HappyPath(benchmark::State &state)
{
   {
    (void)caller0(false);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    (void)caller0(true);
  }
}
BENCHMARK(SadPath);
