#include "benchmark.h"

int caller5(bool do_err)
{
  Dtor d;
  int e = caller6(do_err);
  if(e) 
    return e;
  global_int = 0;
  return 0;
}
