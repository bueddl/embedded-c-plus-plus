#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
  error_struct e;
  int val = 0;
   {
    caller0(&val, false, e);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  error_struct e;
  int val = 0;
   {
    caller0(&val, true, e);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
