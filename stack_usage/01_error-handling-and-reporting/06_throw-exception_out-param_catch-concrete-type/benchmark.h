#pragma once

#include <benchmark/benchmark.h>
#include <exception>

class err_exception : public std::exception 
{
public:
  int val;
  explicit err_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

struct Dtor 
{
  ~Dtor();
};

void callee(bool do_err);
void caller7(int *val, bool do_err);
void caller6(int *val, bool do_err);
void caller5(int *val, bool do_err);
void caller4(int *val, bool do_err);
void caller3(int *val, bool do_err);
void caller2(int *val, bool do_err);
void caller1(int *val, bool do_err);
void caller0(int *val, bool do_err);
