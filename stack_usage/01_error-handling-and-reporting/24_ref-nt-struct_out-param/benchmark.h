#pragma once

#include <benchmark/benchmark.h>

struct error_struct 
{
  void *error = nullptr;
  void *domain = nullptr;
  BENCHMARK_NEVER_INLINE 
  ~error_struct() {}
};

struct Dtor 
{
  ~Dtor();
};


extern int error_info;


extern int error_domain;

error_struct callee(bool do_err);
error_struct caller7(bool do_err, int *val);
error_struct caller6(bool do_err, int *val);
error_struct caller5(bool do_err, int *val);
error_struct caller4(bool do_err, int *val);
error_struct caller3(bool do_err, int *val);
error_struct caller2(bool do_err, int *val);
error_struct caller1(bool do_err, int *val);
error_struct caller0(bool do_err, int *val);
