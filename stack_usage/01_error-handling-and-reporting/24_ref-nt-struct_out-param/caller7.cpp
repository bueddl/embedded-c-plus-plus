#include "benchmark.h"

error_struct caller7(bool do_err, int *val)
{
  Dtor d;
  error_struct e = callee(do_err);
  if(e.error) 
    return e;
  *val = 1;
  return e;
}
