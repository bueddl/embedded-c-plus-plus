#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
  error_struct e;
   {
    int val = caller0(false, e);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  error_struct e;
  int val = 0;
   {
    int val = caller0(true, e);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
