#include "benchmark.h"

int caller7(bool do_err, error_struct &e)
{
  Dtor d;
  callee(do_err, e);
  if(e.error) 
    return 0;
  return 1;
}
