#include "benchmark.h"

int caller4(bool do_err, error_struct &e)
{
  Dtor d;
  int val = caller5(do_err, e);
  if(e.error) 
    return 0;
  return val;
}
