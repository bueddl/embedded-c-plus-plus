#pragma once

#include <benchmark/benchmark.h>

struct error_struct 
{
  void *error = nullptr;
  void *domain = nullptr;
};

struct Dtor 
{
  ~Dtor();
};


extern int error_info;


extern int error_domain;

void callee(bool do_err, error_struct &e);
int caller7(bool do_err, error_struct &e);
int caller6(bool do_err, error_struct &e);
int caller5(bool do_err, error_struct &e);
int caller4(bool do_err, error_struct &e);
int caller3(bool do_err, error_struct &e);
int caller2(bool do_err, error_struct &e);
int caller1(bool do_err, error_struct &e);
int caller0(bool do_err, error_struct &e);
