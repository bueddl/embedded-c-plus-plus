#include "benchmark.h"

int caller6(bool do_err, error_struct &e)
{
  Dtor d;
  int val = caller7(do_err, e);
  if(e.error) 
    return 0;
  return val;
}
