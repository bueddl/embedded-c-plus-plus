#include "benchmark.h"

tl::expected<int, error_struct> caller4(bool do_err)
{
  Dtor d;
  tl::expected<int, error_struct> e = caller5(do_err);
  if(!e) 
    return e;
  return *e+1;
}
