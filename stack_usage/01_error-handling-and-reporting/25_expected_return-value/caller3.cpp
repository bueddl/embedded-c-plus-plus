#include "benchmark.h"

tl::expected<int, error_struct> caller3(bool do_err)
{
  Dtor d;
  tl::expected<int, error_struct> e = caller4(do_err);
  if(!e) 
    return e;
  return *e+1;
}
