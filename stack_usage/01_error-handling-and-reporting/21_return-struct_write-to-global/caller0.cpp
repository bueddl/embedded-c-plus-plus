#include "benchmark.h"

error_struct caller0(bool do_err)
{
  Dtor d;
  error_struct e = caller1(do_err);
  if(e.error) 
    return e;
  global_int = 0;
  return e;
}
