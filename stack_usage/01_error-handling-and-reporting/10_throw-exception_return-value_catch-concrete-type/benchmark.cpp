#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
   {
    try {
      int val = caller0(false);
      benchmark::DoNotOptimize(val);
    } catch (err_exception &e) {
      benchmark::DoNotOptimize(e);
    }
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    try {
      int val = caller0(true);
      benchmark::DoNotOptimize(val);
    } catch (err_exception &e) {
      benchmark::DoNotOptimize(e);
    }
  }
}
BENCHMARK(SadPath);
