#include "benchmark.h"


static void HappyPath(benchmark::State &state)
{
   {
    try {
      int val = caller0(false);
      benchmark::DoNotOptimize(val);
    } catch (err0_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err1_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err2_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err3_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err4_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err5_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err6_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err7_exception &e) {
      benchmark::DoNotOptimize(e);
    }
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    try {
      int val = caller0(true);
      benchmark::DoNotOptimize(val);
    } catch (err0_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err1_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err2_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err3_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err4_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err5_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err6_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err7_exception &e) {
      benchmark::DoNotOptimize(e);
    }
  }
}
BENCHMARK(SadPath);
