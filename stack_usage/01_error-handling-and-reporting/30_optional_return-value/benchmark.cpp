#include "benchmark.h"


static void HappyPath(benchmark::State &state)
{
   {
    std::experimental::optional<int> val = caller0(false);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    std::experimental::optional<int> val = caller0(true);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
