#include "benchmark.h"

std::experimental::optional<int> caller4(bool do_err)
{
  Dtor d;
  std::experimental::optional<int> r = caller5(do_err);
  if (!r)
    return {};
  return *r+1;
}
