#include "benchmark.h"

std::experimental::optional<int> caller0(bool do_err)
{
  Dtor d;
  std::experimental::optional<int> r = caller1(do_err);
  if (!r)
    return {};
  return *r+1;
}
