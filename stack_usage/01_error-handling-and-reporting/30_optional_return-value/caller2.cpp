#include "benchmark.h"

std::experimental::optional<int> caller2(bool do_err)
{
  Dtor d;
  std::experimental::optional<int> r = caller3(do_err);
  if (!r)
    return {};
  return *r+1;
}
