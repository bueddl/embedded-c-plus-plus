#pragma once

#include "optional.hpp"
#include <benchmark/benchmark.h>

struct Dtor 
{
  ~Dtor();
};

std::experimental::optional<int> callee(bool do_err);
std::experimental::optional<int> caller7(bool do_err);
std::experimental::optional<int> caller6(bool do_err);
std::experimental::optional<int> caller5(bool do_err);
std::experimental::optional<int> caller4(bool do_err);
std::experimental::optional<int> caller3(bool do_err);
std::experimental::optional<int> caller2(bool do_err);
std::experimental::optional<int> caller1(bool do_err);
std::experimental::optional<int> caller0(bool do_err);
