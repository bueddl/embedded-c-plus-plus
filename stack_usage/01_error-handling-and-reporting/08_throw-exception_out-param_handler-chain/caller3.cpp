#include "benchmark.h"

void caller3(int *val, bool do_err)
{
  Dtor d;
  int out_val = 0;
  caller4(&out_val, do_err);
  *val = out_val;
}
