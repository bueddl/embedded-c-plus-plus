#pragma once

#include <benchmark/benchmark.h>
#include <exception>

class err0_exception : public std::exception 
{
public:
  int val;
  explicit err0_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err1_exception : public std::exception 
{
public:
  int val;
  explicit err1_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err2_exception : public std::exception 
{
public:
  int val;
  explicit err2_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err3_exception : public std::exception 
{
public:
  int val;
  explicit err3_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err4_exception : public std::exception 
{
public:
  int val;
  explicit err4_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err5_exception : public std::exception 
{
public:
  int val;
  explicit err5_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err6_exception : public std::exception 
{
public:
  int val;
  explicit err6_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err7_exception : public std::exception 
{
public:
  int val;
  explicit err7_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

struct Dtor 
{
  ~Dtor();
};

void callee(bool do_err);
void caller7(int *val, bool do_err);
void caller6(int *val, bool do_err);
void caller5(int *val, bool do_err);
void caller4(int *val, bool do_err);
void caller3(int *val, bool do_err);
void caller2(int *val, bool do_err);
void caller1(int *val, bool do_err);
void caller0(int *val, bool do_err);
