#include "benchmark.h"

result<int> caller7(bool do_err)
{
  Dtor d;
  OUTCOME_TRYV(callee(do_err));
  return outcome::success();
}
