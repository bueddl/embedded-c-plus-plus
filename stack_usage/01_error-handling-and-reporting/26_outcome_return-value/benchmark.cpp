#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
   {
    result<int> r = caller0(false);
    benchmark::DoNotOptimize(r);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    result<int> r = caller0(true);
    benchmark::DoNotOptimize(r);
  }
}
BENCHMARK(SadPath);
