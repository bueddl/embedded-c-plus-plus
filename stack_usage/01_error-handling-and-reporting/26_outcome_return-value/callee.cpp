#include "benchmark.h"

result<int> callee(bool do_err)
{
  if(do_err)
    return outcome::experimental::errc::argument_out_of_domain;
  return outcome::success();
}
