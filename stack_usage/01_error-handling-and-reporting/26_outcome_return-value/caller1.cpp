#include "benchmark.h"

result<int> caller1(bool do_err)
{
  Dtor d;
  OUTCOME_TRYV(caller2(do_err));
  return outcome::success();
}
