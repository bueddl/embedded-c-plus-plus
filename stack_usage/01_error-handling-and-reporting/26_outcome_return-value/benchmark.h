#pragma once

#include <stddef.h>

inline char *strerror_r(int errnum, char *buf, size_t buflen)
{
    return "";
}

#define OUTCOME_DISABLE_EXECINFO
#include "outcome-experimental.hpp"
#include <benchmark/benchmark.h>

struct Dtor 
{
  ~Dtor();
};

namespace outcome = OUTCOME_V2_NAMESPACE;

template <class T>
using result = outcome::experimental::status_result<T>;

result<int> callee(bool do_err);
result<int> caller7(bool do_err);
result<int> caller6(bool do_err);
result<int> caller5(bool do_err);
result<int> caller4(bool do_err);
result<int> caller3(bool do_err);
result<int> caller2(bool do_err);
result<int> caller1(bool do_err);
result<int> caller0(bool do_err);
