#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
  int val = 0;
   {
    try {
      caller0(&val, false);
    } catch (...) {
    }
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  int val = 0;
   {
    try {
      caller0(&val, true);
    } catch (...) {
    }
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
