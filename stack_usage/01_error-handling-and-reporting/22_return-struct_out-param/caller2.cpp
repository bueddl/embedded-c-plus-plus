#include "benchmark.h"

error_struct caller2(bool do_err, int *val)
{
  Dtor d;
  int out_val = 0;
  error_struct e = caller3(do_err, &out_val);
  if(e.error) 
    return e;
  *val = out_val;
  return e;
}
