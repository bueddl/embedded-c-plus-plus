#include "benchmark.h"


static void HappyPath(benchmark::State &state)
{
  int val = 0;
   {
    (void)caller0(&val, false);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  int val = 0;
   {
    (void)caller0(&val, true);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
