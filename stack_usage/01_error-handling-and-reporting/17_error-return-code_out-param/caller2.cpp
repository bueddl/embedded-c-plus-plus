#include "benchmark.h"

int caller2(int *val, bool do_err)
{
  Dtor d;
  int out_val = 0;
  int e = caller3(&out_val, do_err);
  if(e) 
    return e;
  *val = out_val;
  return 0;
}
