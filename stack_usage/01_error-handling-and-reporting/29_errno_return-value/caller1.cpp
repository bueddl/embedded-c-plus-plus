#include "benchmark.h"

int caller1(bool do_err)
{
  Dtor d;
  int val = caller2(do_err);
  if(errno) 
    return 0;
  return val;
}
