#include "benchmark.h"

int caller3(bool do_err)
{
  Dtor d;
  int val = caller4(do_err);
  if(errno) 
    return 0;
  return val;
}
