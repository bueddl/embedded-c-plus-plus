#pragma once

#include <benchmark/benchmark.h>

struct Dtor 
{
  ~Dtor();
};


extern int errno;

void callee(bool do_err);
int caller7(bool do_err);
int caller6(bool do_err);
int caller5(bool do_err);
int caller4(bool do_err);
int caller3(bool do_err);
int caller2(bool do_err);
int caller1(bool do_err);
int caller0(bool do_err);
