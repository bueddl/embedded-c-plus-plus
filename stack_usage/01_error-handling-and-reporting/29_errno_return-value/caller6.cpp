#include "benchmark.h"

int caller6(bool do_err)
{
  Dtor d;
  int val = caller7(do_err);
  if(errno) 
    return 0;
  return val;
}
