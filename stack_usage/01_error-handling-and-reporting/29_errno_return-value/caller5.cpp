#include "benchmark.h"

int caller5(bool do_err)
{
  Dtor d;
  int val = caller6(do_err);
  if(errno) 
    return 0;
  return val;
}
