#include "benchmark.h"

error_struct caller1(bool do_err)
{
  Dtor d;
  error_struct e = caller2(do_err);
  if(e.error) 
    return e;
  global_int = 0;
  return e;
}
