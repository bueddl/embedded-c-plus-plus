#include "benchmark.h"

error_struct caller4(bool do_err)
{
  Dtor d;
  error_struct e = caller5(do_err);
  if(e.error) 
    return e;
  global_int = 0;
  return e;
}
