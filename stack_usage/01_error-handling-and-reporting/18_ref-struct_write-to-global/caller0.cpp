#include "benchmark.h"

void caller0(bool do_err, error_struct &e)
{
  Dtor d;
  caller1(do_err, e);
  if(e.error) 
    return;
  global_int = 0;
}
