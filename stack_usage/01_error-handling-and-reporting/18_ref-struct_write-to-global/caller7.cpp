#include "benchmark.h"

void caller7(bool do_err, error_struct &e)
{
  Dtor d;
  callee(do_err, e);
  if(e.error) 
    return;
  global_int = 0;
}
