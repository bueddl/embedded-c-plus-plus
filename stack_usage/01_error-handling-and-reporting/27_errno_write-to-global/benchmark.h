#pragma once

#include <benchmark/benchmark.h>

struct Dtor 
{
  ~Dtor();
};


extern int global_int;


extern int errno;

void callee(bool do_err);
void caller7(bool do_err);
void caller6(bool do_err);
void caller5(bool do_err);
void caller4(bool do_err);
void caller3(bool do_err);
void caller2(bool do_err);
void caller1(bool do_err);
void caller0(bool do_err);
