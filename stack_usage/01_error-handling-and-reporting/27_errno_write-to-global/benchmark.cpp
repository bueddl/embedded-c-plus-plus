#include "benchmark.h"


int global_int = 0;

int errno = 0;

static void HappyPath(benchmark::State &state)
{
   {
    caller0(false);
    benchmark::DoNotOptimize(errno);
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    caller0(true);
    benchmark::DoNotOptimize(errno);
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(SadPath);
