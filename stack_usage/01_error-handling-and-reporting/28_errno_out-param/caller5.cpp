#include "benchmark.h"

void caller5(int *val, bool do_err)
{
  Dtor d;
  int out_val = 0;
  caller6(&out_val, do_err);
  if(errno) 
    return;
  *val = out_val;
}
