#include "benchmark.h"


int global_int = 0;

static void HappyPath(benchmark::State &state)
{
  int val = 0;
   {
    (void)caller0(&val, false);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);
