#include "benchmark.h"

jmp_buf buf;
int global_int = 0;

static void HappyPath(benchmark::State &state)
{
   {
    int ret = setjmp(buf);
    if (ret == 0) {
      caller0(false);
      benchmark::DoNotOptimize(global_int);
    } else {
      benchmark::DoNotOptimize(ret);
    }
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    int ret = setjmp(buf);
    if (ret == 0) {
      caller0(true);
      benchmark::DoNotOptimize(global_int);
    } else {
      benchmark::DoNotOptimize(ret);
    }
  }
}
BENCHMARK(SadPath);
