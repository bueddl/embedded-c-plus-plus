#include "benchmark.h"

jmp_buf buf;

static void HappyPath(benchmark::State &state)
{
   {
    int ret = setjmp(buf);
    if (ret == 0) {
      int val = caller0(false);
      benchmark::DoNotOptimize(val);
    } else {
      benchmark::DoNotOptimize(ret);
    }
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    int ret = setjmp(buf);
    if (ret == 0) {
      int val = caller0(true);
      benchmark::DoNotOptimize(val);
    } else {
      benchmark::DoNotOptimize(ret);
    }
  }
}
BENCHMARK(SadPath);
