#include "benchmark.h"

int caller1(bool do_err)
{
  Dtor d;
  return caller2(do_err) + 1;
}
