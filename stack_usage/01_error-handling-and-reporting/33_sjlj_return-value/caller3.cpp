#include "benchmark.h"

int caller3(bool do_err)
{
  Dtor d;
  return caller4(do_err) + 1;
}
