#include "benchmark.h"

static void Serialize(benchmark::State &state)
{
  CAN_Payload_CurrentEngineData payload;

   {
    char buf[8];
    BinaryWriter writer{buf};
    writer.write(payload);
    benchmark::DoNotOptimize(buf);
  }
}
BENCHMARK(Serialize);
