#pragma once

#include <benchmark/benchmark.h>
#include <stdint.h>

template<typename... Ts> struct make_void { typedef void type;};
template<typename... Ts> using void_t = typename make_void<Ts...>::type;

template<typename T>
T& declval();

// write int as LE
class BinaryWriter
{
public:
  template<unsigned int Size>
  BinaryWriter(char (&buf)[Size])
    : buf_{buf},
      size_{Size}
  {}

  void write(uint8_t value)
  {
    if (!ensure_capacity(1))
      return;

    buf_[pos_++] = value;
  }

  void write(int8_t value)
  {
    if (!ensure_capacity(1))
      return;

    buf_[pos_++] = value;
  }

  void write(uint16_t value)
  {
    if (!ensure_capacity(2))
      return;

    buf_[pos_++] = value & 0xff;
    buf_[pos_++] = (value >> 8) & 0xff;
  }

  void write(int16_t value)
  {
    if (!ensure_capacity(2))
      return;

    buf_[pos_++] = value & 0xff;
    buf_[pos_++] = (value >> 8) & 0xff;
  }

  void write(uint32_t value)
  {
    if (!ensure_capacity(4))
      return;

    buf_[pos_++] = value & 0xff;
    buf_[pos_++] = (value >> 8) & 0xff;
    buf_[pos_++] = (value >> 16) & 0xff;
    buf_[pos_++] = (value >> 24) & 0xff;
  }

  void write(int32_t value)
  {
    if (!ensure_capacity(4))
      return;

    buf_[pos_++] = value & 0xff;
    buf_[pos_++] = (value >> 8) & 0xff;
    buf_[pos_++] = (value >> 16) & 0xff;
    buf_[pos_++] = (value >> 24) & 0xff;
  }

  template<typename T, 
    typename = void_t<decltype(serialize(declval<BinaryWriter>(), declval<T>()))>>
  void write(T const& t)
  {
    serialize(*this, t);
  }

private:
  char *buf_;
  unsigned int size_;
  unsigned int pos_ = 0;
  bool bad_flag_ = false;

  bool ensure_capacity(unsigned int required_size)
  {
    return bad_flag_ = (required_size + pos_ < size_);
  }
};

class CAN_Payload
{
};

class CAN_Payload_CurrentEngineData final : public CAN_Payload
{
public:
  friend void serialize(BinaryWriter &writer, CAN_Payload_CurrentEngineData const& payload)
  {
    writer.write(payload.rpm);
    writer.write(payload.temp);
  }

private:
  int32_t rpm;
  short temp;
};

class CAN_Payload_AcceleratorPadelPosition final : public CAN_Payload
{
public:
  friend void serialize(BinaryWriter &writer, CAN_Payload_AcceleratorPadelPosition const& payload)
  {
    writer.write(payload.position);
  }

private:
  int8_t position;
};

class CAN_Payload_DoorOpenStatus final : public CAN_Payload
{
public:
  friend void serialize(BinaryWriter &writer, CAN_Payload_DoorOpenStatus const& payload)
  {
    writer.write(static_cast<uint8_t>(payload.closed));
  }

private:
  bool closed;
};
