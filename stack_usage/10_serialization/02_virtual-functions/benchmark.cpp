#include "benchmark.h"

void serialize_generic(BinaryWriter &writer, CAN_Payload const& payload)
{
  payload.serialize(writer);
}

static void SerializeGeneric(benchmark::State &state)
{
  CAN_Payload_CurrentEngineData payload;

   {
    char buf[8];
    BinaryWriter writer{buf};
    serialize_generic(writer, payload);
    benchmark::DoNotOptimize(buf);
  }
}
BENCHMARK(SerializeGeneric);

void serialize_final(BinaryWriter &writer, CAN_Payload_CurrentEngineData const& payload)
{
  payload.serialize(writer);
}

static void SerializeFinal(benchmark::State &state)
{
  CAN_Payload_CurrentEngineData payload;

   {
    char buf[8];
    BinaryWriter writer{buf};
    serialize_final(writer, payload);
    benchmark::DoNotOptimize(buf);
  }
}
BENCHMARK(SerializeFinal);
