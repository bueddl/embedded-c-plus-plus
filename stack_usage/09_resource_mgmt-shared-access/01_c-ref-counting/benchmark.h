#pragma once

#include <benchmark/benchmark.h>

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#define container_of(ptr, type, member) \
    ((type *)((char *)(ptr) - offsetof(type, member)))

// "embedded" ref count style, as used by many projects such as:
// - linux (OS kernel)  
//   https://github.com/torvalds/linux/blob/master/lib/refcount.c
// - qemu (virtualization software)  
//   https://github.com/qemu/qemu/blob/3a63b24a1bbf166e6f455fe43a6bbd8dea413d92/include/ui/console.h
// - ISC bind (nameserver)  
//   https://gitlab.isc.org/isc-projects/bind9/-/blob/72b322cde0e51c4c87df0c9e3226deac95dfb4ce/lib/isc/include/isc/refcount.h
//
//
// based on https://nullprogram.com/blog/2015/02/17/
//
// ARMv8: LDR/STR of single register is atomic (ARMv8 Instruction Set oVerview, 5.2 Memory Access)

struct ref {
  void (*free)(const struct ref *);
  int count;
};

static inline void
ref_inc(const struct ref *ref)
{
  ((struct ref *)ref)->count++;
}

static inline void
ref_dec(const struct ref *ref)
{
  if (--((struct ref *)ref)->count == 0)
    ref->free(ref);
}

struct MyData
{
  char payload[0x10];
  struct ref refcount;
};

struct MyData *MyData_create();
void use(MyData *data);
