#include "benchmark.h"

void use(MyData *data)
{
    benchmark::DoNotOptimize(data);
    benchmark::DoNotOptimize(*data);

    ref_dec(&data->refcount);
}
