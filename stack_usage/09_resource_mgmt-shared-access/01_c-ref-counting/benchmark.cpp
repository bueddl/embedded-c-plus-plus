#include "benchmark.h"

static void Construct(benchmark::State &state)
{
  

   {
    
    MyData *data = MyData_create();
    benchmark::DoNotOptimize(data);

      // Note: destruct of data deliberately not measured this way!
    ref_dec(&data->refcount);
  }

  
}
BENCHMARK(Construct);

static void PassToUser(benchmark::State &state)
{
  
  MyData *data = MyData_create();
  benchmark::DoNotOptimize(data);
    
   {
    
    ref_inc(&data->refcount);
    use(data);
    
    benchmark::DoNotOptimize(data);
  }
  
  ref_dec(&data->refcount);
  
}
BENCHMARK(PassToUser);

static void ConstructDestruct(benchmark::State &state)
{
   {
    MyData *data = MyData_create();
    benchmark::DoNotOptimize(data);
    ref_dec(&data->refcount);
  }
}
BENCHMARK(ConstructDestruct);
