#include "benchmark.h"

static void Construct(benchmark::State &state)
{
  

   {
    
    std::shared_ptr<MyData> data = std::make_shared<MyData>();
    benchmark::DoNotOptimize(data);

     // Note: destruct of data deliberately not measured this way!
  }

  
}
BENCHMARK(Construct);

static void PassToUser(benchmark::State &state)
{
  std::shared_ptr<MyData> data;
  benchmark::DoNotOptimize(data);
    
   {
    use(data);
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(PassToUser);

static void ConstructDestruct(benchmark::State &state)
{
   {
    std::shared_ptr<MyData> data = std::make_shared<MyData>();
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(ConstructDestruct);
