#include <benchmark/benchmark.h>

void f();
void g();

typedef void (*callback_t)(int some, int args);

void sender(callback_t cb);
