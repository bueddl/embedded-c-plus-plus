#include "benchmark.h"

void my_callback(int a, int b)
{
  benchmark::DoNotOptimize(a);
  benchmark::DoNotOptimize(b);
}

static void FreeFunction(benchmark::State &state)
{
   {
    sender(my_callback);
  }
}
BENCHMARK(FreeFunction);

// ---

struct Dummy
{
  static void callback(int a, int b)
  {
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(b);
  }
};

static void StaticMemberFunction(benchmark::State &state)
{
   {
    sender(Dummy::callback);
  }
}
BENCHMARK(StaticMemberFunction);

// ---

static void StatelessClosure(benchmark::State &state)
{
   {
    sender([] (int a, int b) {
      benchmark::DoNotOptimize(a);
      benchmark::DoNotOptimize(b);
    });
  }
}
BENCHMARK(StatelessClosure);

// ---
