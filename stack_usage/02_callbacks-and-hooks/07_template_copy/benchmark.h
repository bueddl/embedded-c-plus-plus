#include <benchmark/benchmark.h>

void f();
void g();

template<typename F>
void sender(F cb)
{
    f();
    cb(0, 1);
    g();
}
