#include "benchmark.h"

class SimpleCallback : public Callback
{
public:
  void execute(int a, int b) override
  {
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(b);
    
  }
};

static void CallbackNoCaptures(benchmark::State &state) 
{
   {
    SimpleCallback my_cb;
    sender(my_cb);
  }
}
BENCHMARK(CallbackNoCaptures);

// ---

class CallbackWithState : public Callback
{
public:
  int value;

  explicit CallbackWithState(int val)
    : value{val}
  {}

  void execute(int a, int b) override
  {
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(b);
    benchmark::DoNotOptimize(value);
    
  }
};

static void CallbackWithCaptures(benchmark::State &state) 
{
  int capture;

   {
    CallbackWithState my_cb{capture};
    sender(my_cb);
  }
}
BENCHMARK(CallbackWithCaptures);
