#include "benchmark.h"

static void Construct(benchmark::State &state)
{
  

   {
    
    MyData *data = new MyData();

    benchmark::DoNotOptimize(data);
    

    delete data;
  }

  
}
BENCHMARK(Construct);

static void PassToUser(benchmark::State &state)
{
  MyData *data = nullptr;
    
   {
    benchmark::DoNotOptimize(data);
    use(data);
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(PassToUser);

static void ConstructDestruct(benchmark::State &state)
{
   {
    MyData *data = new MyData;
    benchmark::DoNotOptimize(data);
    delete data;
  }
}
BENCHMARK(ConstructDestruct);
