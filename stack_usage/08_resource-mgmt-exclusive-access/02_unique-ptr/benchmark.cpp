#include "benchmark.h"

static void Construct(benchmark::State &state)
{
  

   {
    
    std::unique_ptr<MyData> data = std::make_unique<MyData>();
    benchmark::DoNotOptimize(data);

    
  }

  
}
BENCHMARK(Construct);

void use(std::unique_ptr<MyData> data);


static void PassToUser(benchmark::State &state)
{
  std::unique_ptr<MyData> data;
    
   {
    benchmark::DoNotOptimize(data);
    use(std::move(data));
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(PassToUser);

static void ConstructDestruct(benchmark::State &state)
{
   {
    std::unique_ptr<MyData> data = std::make_unique<MyData>();
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(ConstructDestruct);
