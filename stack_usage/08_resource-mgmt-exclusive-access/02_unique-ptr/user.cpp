#include "benchmark.h"

void use(std::unique_ptr<MyData> data)
{
    benchmark::DoNotOptimize(data);
}
