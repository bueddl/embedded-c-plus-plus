#include "benchmark.h"

int init_module(bool do_err)
{
  int c0, c1, c2, c3;
  int ret;

  benchmark::DoNotOptimize(do_err); 

  ret = init_component0(false, &c0);
  benchmark::DoNotOptimize(ret);
  if (ret == -1)
    goto CLEANUP0;

  ret = init_component1(false, &c1);
  benchmark::DoNotOptimize(ret);
  if (ret == -1)
    goto CLEANUP1;

  ret = init_component2(false, &c2);
  benchmark::DoNotOptimize(ret);
  if (ret == -1)
    goto CLEANUP2;

  ret = init_component3(do_err, &c3);
  benchmark::DoNotOptimize(ret);
  if (ret == -1)
    goto CLEANUP3;

  benchmark::DoNotOptimize(c0);
  benchmark::DoNotOptimize(c1);
  benchmark::DoNotOptimize(c2);
  benchmark::DoNotOptimize(c3);
  dummy_work();
  return 0;

  cleanup_component3(c3);
CLEANUP3:
  cleanup_component2(c2);
CLEANUP2:
  cleanup_component1(c1);
CLEANUP1:
  cleanup_component0(c0);
CLEANUP0:

  return -1;
}

static void HappyPath(benchmark::State &state)
{
   {
    int ret = init_module(false);
    benchmark::DoNotOptimize(ret);    
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    int ret = init_module(true);
    benchmark::DoNotOptimize(ret);    
  }
}
BENCHMARK(SadPath);
