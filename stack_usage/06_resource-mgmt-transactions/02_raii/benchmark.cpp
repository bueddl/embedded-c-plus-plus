#include "benchmark.h"

int init_module(bool do_err, benchmark::State &state)
{
  benchmark::DoNotOptimize(do_err); 

  Component0 c0(false);
  if (!c0)
    return -1;

  Component1 c1(false);
  if (!c1)
    return -1;

  Component2 c2(false);
  if (!c2)
    return -1;

  Component3 c3(do_err);
  if (!c3) {
    
    return -1;
  }

  benchmark::DoNotOptimize(c0);
  benchmark::DoNotOptimize(c1);
  benchmark::DoNotOptimize(c2);
  benchmark::DoNotOptimize(c3);
  dummy_work();

  

  return 0;
}

static void HappyPath(benchmark::State &state)
{
   {
    int ret = init_module(false, state);
    benchmark::DoNotOptimize(ret);    

    
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
   {
    int ret = init_module(true, state);
    benchmark::DoNotOptimize(ret);    

    
  }
}
BENCHMARK(SadPath);
