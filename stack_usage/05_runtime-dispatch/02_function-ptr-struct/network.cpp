#include "benchmark.h"

can_packet_t* Network_Receive_NextPacket()
{
  static can_packet_t packet = {
    &current_engine_data_payload_ops,
    NULL
  };

  return &packet;
}
