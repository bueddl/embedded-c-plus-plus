#pragma once
#include <benchmark/benchmark.h>

#include "variant.hpp"

struct CurrentEngineData
{
  int rpm;
  short temp;
};

struct AcceleratorPadelPosition
{
  char position;
};

struct DoorOpenStatus
{
  bool closed;
};

using Payload =
  mpark::variant<
    CurrentEngineData,
    AcceleratorPadelPosition,
    DoorOpenStatus>;
  
struct CANPacket
{
  // ... can stuff
  Payload payload;
};

template<typename... Fs>
struct overloaded : Fs...
{
  using Fs::operator()...;
};

template<typename... Fs>
auto overload(Fs&&... fs) -> overloaded<Fs...>
{
  return {fs...};
}

CANPacket& Network_Receive_NextPacket();
