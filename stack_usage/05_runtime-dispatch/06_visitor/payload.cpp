#include "benchmark.h"

void Payload_CurrentEngineData::apply(IVisitor &visitor) const
{
  visitor.visit(*this);
}

void Payload_AcceleratorPadelPosition::apply(IVisitor &visitor) const
{
  visitor.visit(*this);
}

void Payload_DoorOpenStatus::apply(IVisitor &visitor) const
{
  visitor.visit(*this);
}
