#include "benchmark.h"

void PacketReceivedHandler::visit(Payload_CurrentEngineData const &node)
{
  benchmark::DoNotOptimize(node);
  benchmark::DoNotOptimize(node.rpm);
  benchmark::DoNotOptimize(node.temp);
}

void PacketReceivedHandler::visit(Payload_AcceleratorPadelPosition const &node)
{
  benchmark::DoNotOptimize(node);
  benchmark::DoNotOptimize(node.position);
}

void PacketReceivedHandler::visit(Payload_DoorOpenStatus const &node)
{
  benchmark::DoNotOptimize(node);
  benchmark::DoNotOptimize(node.closed);
}
