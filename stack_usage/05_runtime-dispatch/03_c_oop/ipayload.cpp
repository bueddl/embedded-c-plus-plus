#include "benchmark.h"

void IPayload_HandleReceive(struct IPayload *payload)
{
  benchmark::DoNotOptimize(payload);
}

const struct IPayloadOps OpsForIPayload = {IPayload_HandleReceive};
