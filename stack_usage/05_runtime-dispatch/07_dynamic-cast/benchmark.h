#pragma once

#include <benchmark/benchmark.h>
// --

struct Payload_Base
{
  virtual ~Payload_Base() = default;
};

struct CurrentEngineData : Payload_Base
{
  int rpm;
  short temp;
};

struct AcceleratorPadelPosition : Payload_Base
{
  char position;
};

struct DoorOpenStatus : Payload_Base
{
  bool closed;
};
  
struct CANPacket
{
  // ... can stuff
  Payload_Base *payload;
};

CANPacket& Network_Receive_NextPacket();
