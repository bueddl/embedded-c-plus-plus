#include "benchmark.h"

CANPacket& Network_Receive_NextPacket()
{
  static CurrentEngineData payload;
  static CANPacket packet;
  packet.payload = &payload;
  return packet;
}
