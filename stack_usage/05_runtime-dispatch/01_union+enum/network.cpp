#include "benchmark.h"

static can_packet_t packet;  

can_packet_t* Network_Receive_NextPacket()
{
  packet.payload.type = PAYLOAD_CURRENT_ENGINE_DATA;
  packet.payload.data.current_engine_data.rpm = 3500;
  packet.payload.data.current_engine_data.temp = 1000;
  return &packet;
}
