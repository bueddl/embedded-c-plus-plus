#include "benchmark.h"

class FirstPredicate : public IPredicate
{
public:
  virtual bool matches(int elem) const noexcept override
  {
    return elem >= 15 && elem < 50; // [15;50)
  }
};

static void NonFinalPredicate(benchmark::State &state)
{
  int const arr[] = {69, 5, 93, 85, 61, 86, 82, 25, 72, 55, 77, 31, 87, 51, 50, 2, 98, 8, 23, 49, 21, 29, 36, 89, 100, 22, 95, 19, 46, 26, 76, 64, 28, 68, 78, 71, 13, 35, 16, 52, 10, 44, 57, 15, 80, 24, 27, 81, 59, 18};
   {
    using std::begin; 
    using std::end;

    benchmark::DoNotOptimize(arr);
    auto count = count_if(begin(arr), end(arr), FirstPredicate{});
    benchmark::DoNotOptimize(count);
  }
}
BENCHMARK(NonFinalPredicate);

// ---

class SecondPredicate final : public IPredicate
{
public:
  virtual bool matches(int elem) const noexcept override
  {
    return elem >= 15 && elem < 50; // [15;50)
  }
};

static void FinalPredicate(benchmark::State &state)
{
  int const arr[] = {69, 5, 93, 85, 61, 86, 82, 25, 72, 55, 77, 31, 87, 51, 50, 2, 98, 8, 23, 49, 21, 29, 36, 89, 100, 22, 95, 19, 46, 26, 76, 64, 28, 68, 78, 71, 13, 35, 16, 52, 10, 44, 57, 15, 80, 24, 27, 81, 59, 18};
   {
    using std::begin; 
    using std::end;

    benchmark::DoNotOptimize(arr);
    auto count = count_if(begin(arr), end(arr), SecondPredicate{});
    benchmark::DoNotOptimize(count);
  }
}
BENCHMARK(FinalPredicate);
