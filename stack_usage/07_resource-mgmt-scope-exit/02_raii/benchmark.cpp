#include "benchmark.h"

int global_int = 0;

namespace detail {

template<typename T>
struct finalize final {
  ~finalize() {
      fn();
  }

  T fn;
};
}

template<typename T>
auto finalize(T&& fn) -> detail::finalize<T>
{
  return {fn};
}

void cleanup()
{
    dummy_work(); // run at exit of scope
}

void process(int val)
{
  benchmark::DoNotOptimize(val);

  auto f = finalize([] {
      cleanup();
  });

  global_int = 0;

  if (val == 0)
    return;

  global_int = 1;

  if (val == 1)
    return;

  global_int = 2;
}

static void Finalize(benchmark::State &state)
{
   {
    process(0);
    benchmark::DoNotOptimize(global_int);    
  }
}
BENCHMARK(Finalize);
