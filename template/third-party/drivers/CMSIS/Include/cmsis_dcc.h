/**************************************************************************//**
 * @file     cmsis_dcc.h
 * @brief    CMSIS compiler DCC (WindRiver Diab) header file
 * @version  V0.1.1
 * @date     30.05.2020
 ******************************************************************************/
/*
 * Copyright (c) Sebastian Büttner.
 */

#ifndef __CMSIS_DCC_H
#define __CMSIS_DCC_H


#if !defined(__VERSION_NUMBER__ ) || (__VERSION_NUMBER__  < 5971)
  #error "Please use WindRiver Diab Compiler Version 5.9.7.1 or newer."
#endif

#include <diab/armasm.h>


/* CMSIS compiler specific defines */
#ifndef   __ASM
  #define __ASM                                  __asm
#endif
#ifndef   __INLINE
  #define __INLINE                               inline
#endif
#ifndef   __STATIC_INLINE
  #define __STATIC_INLINE                        static inline
#endif
#ifndef   __STATIC_FORCEINLINE                 
  #define __STATIC_FORCEINLINE                   __STATIC_INLINE
#endif           
#ifndef   __NO_RETURN
  #define __NO_RETURN                            __attribute__((noreturn))
#endif
#ifndef   __USED
  #define __USED                                 __attribute__((used))
#endif
#ifndef   __WEAK
  #define __WEAK                                 __attribute__((weak))
  #define __weak                                 __WEAK
#endif
#ifndef   __PACKED
  #define __PACKED                               __attribute__((packed))
#endif
#ifndef   __PACKED_STRUCT
  #define __PACKED_STRUCT                        __PACKED struct
#endif
#ifndef   __PACKED_UNION
  #define __PACKED_UNION                         __PACKED union
#endif
#ifndef   __UNALIGNED_UINT32        /* deprecated */
  #define __UNALIGNED_UINT32(x)                  (*((__packed uint32_t *)(x)))
#endif
#ifndef   __UNALIGNED_UINT16_WRITE
  #define __UNALIGNED_UINT16_WRITE(addr, val)    ((*((__packed uint16_t *)(addr))) = (val))
#endif
#ifndef   __UNALIGNED_UINT16_READ
  #define __UNALIGNED_UINT16_READ(addr)          (*((const __packed uint16_t *)(addr)))
#endif
#ifndef   __UNALIGNED_UINT32_WRITE
  #define __UNALIGNED_UINT32_WRITE(addr, val)    ((*((__packed uint32_t *)(addr))) = (val))
#endif
#ifndef   __UNALIGNED_UINT32_READ
  #define __UNALIGNED_UINT32_READ(addr)          (*((const __packed uint32_t *)(addr)))
#endif
#ifndef   __ALIGNED
  #define __ALIGNED(x)                           __attribute__((aligned(x)))
#endif
#ifndef   __RESTRICT
  #warning No compiler specific method to specify __RESTRICT. Ingoring __RESTRICT.
  #define __RESTRICT                             
#endif
#ifndef   __RAMFUNC
  #define __RAM_FUNC                             __attribute__((section(".RamFunc")))
#endif

__STATIC_FORCEINLINE void __enable_irq(void)
{
  __ASM volatile ("cpsie i");
}


__STATIC_FORCEINLINE void __disable_irq(void)
{
  __ASM volatile ("cpsid i");
}

#define __NOP()                                  __nop()

#define __WFI()                                  __wfi()

#define __WFE()                                  __wfe()

#define __SEV()                                  __sev()

#define __ISB()                                  __isb(0xF)

#define __DSB()                                  __dsb(0xF)

#define __DMB()                                  __dmb(0xF)

#define __REV(value)                             __rev(value)
#define __REV16(value)                           __rev16(value)
#define __REVSH(value)                           __revsh(value)

#endif /* __CMSIS_DCC_H */

