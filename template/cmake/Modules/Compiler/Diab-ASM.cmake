include(Compiler/DIAB)

set(CMAKE_ASM_COMPILE_OBJECT  "<CMAKE_ASM_COMPILER> <FLAGS> -o <OBJECT> <SOURCE>") # <DEFINES> excluded due to handling issues (strings need be escaped, dunno how to do)

set(CMAKE_ASM_SOURCE_FILE_EXTENSIONS s;S)
