set(_compiler_id_pp_test "defined(__DCC__) && defined(_DIAB_TOOL) && defined(__VERSION_NUMBER__)")

set(_compiler_id_version_compute "
#if defined(__VERSION_NUMBER__)
  /* __VERSION_NUMBER__ = MNPX */
# define @PREFIX@COMPILER_VERSION_MAJOR @MACRO_DEC@(__VERSION_NUMBER__ / 1000)
# define @PREFIX@COMPILER_VERSION_MINOR @MACRO_DEC@((__VERSION_NUMBER__ / 100) % 10)
# define @PREFIX@COMPILER_VERSION_PATCH @MACRO_DEC@((__VERSION_NUMBER__ / 10)  % 10)
#endif")
