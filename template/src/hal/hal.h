#pragma once

#include "../error.h"

#ifdef __cplusplus
extern "C" {
#endif

#if defined STM32L031xx
	#include "stm32l0xx_hal.h"
	
#elif defined STM32L432xx || defined STM32L4P5xx
	#include "stm32l4xx_hal.h"
	
#elif defined STM32F031x6
	#include "stm32f0xx_hal.h"
	
#elif defined STM32F207xx
	#include "stm32f2xx_hal.h"
  
#elif defined STM32F303xE
	#include "stm32f3xx_hal.h"
  
#elif defined STM32F446xx
	#include "stm32f4xx_hal.h"
	
#elif defined STM32F722xx || defined STM32F746xx
	#include "stm32f7xx_hal.h"

#else
	#error "No target device selected."

#endif

extern UART_HandleTypeDef huart;

void SystemClock_Config(void);
void MX_UART_Init(void);
void MX_GPIO_Init(void);

#if defined STM32L031xx
	#define VCP_TX_Pin GPIO_PIN_2
	#define VCP_TX_GPIO_Port GPIOA
	#define TMS_Pin GPIO_PIN_13
	#define TMS_GPIO_Port GPIOA
	#define TCK_Pin GPIO_PIN_14
	#define TCK_GPIO_Port GPIOA
	#define VCP_RX_Pin GPIO_PIN_15
	#define VCP_RX_GPIO_Port GPIOA
	#define LD3_Pin GPIO_PIN_3
	#define LD3_GPIO_Port GPIOB
	
#elif defined STM32L432xx
	#define MCO_Pin GPIO_PIN_0
	#define MCO_GPIO_Port GPIOA
	#define VCP_TX_Pin GPIO_PIN_2
	#define VCP_TX_GPIO_Port GPIOA
	#define SWDIO_Pin GPIO_PIN_13
	#define SWDIO_GPIO_Port GPIOA
	#define SWCLK_Pin GPIO_PIN_14
	#define SWCLK_GPIO_Port GPIOA
	#define VCP_RX_Pin GPIO_PIN_15
	#define VCP_RX_GPIO_Port GPIOA
	#define LD3_Pin GPIO_PIN_3
	#define LD3_GPIO_Port GPIOB

#elif defined STM32F031x6
	#define VCP_TX_Pin GPIO_PIN_2
	#define VCP_TX_GPIO_Port GPIOA
	#define SWDIO_Pin GPIO_PIN_13
	#define SWDIO_GPIO_Port GPIOA
	#define SWCLK_Pin GPIO_PIN_14
	#define SWCLK_GPIO_Port GPIOA
	#define VCP_RX_Pin GPIO_PIN_15
	#define VCP_RX_GPIO_Port GPIOA
	#define LD3_Pin GPIO_PIN_3
	#define LD3_GPIO_Port GPIOB
	
#elif defined STM32F207xx
	#define USER_Btn_Pin GPIO_PIN_13
	#define USER_Btn_GPIO_Port GPIOC
	#define MCO_Pin GPIO_PIN_0
	#define MCO_GPIO_Port GPIOH
	#define RMII_MDC_Pin GPIO_PIN_1
	#define RMII_MDC_GPIO_Port GPIOC
	#define RMII_REF_CLK_Pin GPIO_PIN_1
	#define RMII_REF_CLK_GPIO_Port GPIOA
	#define RMII_MDIO_Pin GPIO_PIN_2
	#define RMII_MDIO_GPIO_Port GPIOA
	#define RMII_CRS_DV_Pin GPIO_PIN_7
	#define RMII_CRS_DV_GPIO_Port GPIOA
	#define RMII_RXD0_Pin GPIO_PIN_4
	#define RMII_RXD0_GPIO_Port GPIOC
	#define RMII_RXD1_Pin GPIO_PIN_5
	#define RMII_RXD1_GPIO_Port GPIOC
	#define LD1_Pin GPIO_PIN_0
	#define LD1_GPIO_Port GPIOB
	#define RMII_TXD1_Pin GPIO_PIN_13
	#define RMII_TXD1_GPIO_Port GPIOB
	#define LD3_Pin GPIO_PIN_14
	#define LD3_GPIO_Port GPIOB
	#define STLK_RX_Pin GPIO_PIN_8
	#define STLK_RX_GPIO_Port GPIOD
	#define STLK_TX_Pin GPIO_PIN_9
	#define STLK_TX_GPIO_Port GPIOD
	#define USB_PowerSwitchOn_Pin GPIO_PIN_6
	#define USB_PowerSwitchOn_GPIO_Port GPIOG
	#define USB_OverCurrent_Pin GPIO_PIN_7
	#define USB_OverCurrent_GPIO_Port GPIOG
	#define USB_SOF_Pin GPIO_PIN_8
	#define USB_SOF_GPIO_Port GPIOA
	#define USB_VBUS_Pin GPIO_PIN_9
	#define USB_VBUS_GPIO_Port GPIOA
	#define USB_ID_Pin GPIO_PIN_10
	#define USB_ID_GPIO_Port GPIOA
	#define USB_DM_Pin GPIO_PIN_11
	#define USB_DM_GPIO_Port GPIOA
	#define USB_DP_Pin GPIO_PIN_12
	#define USB_DP_GPIO_Port GPIOA
	#define TMS_Pin GPIO_PIN_13
	#define TMS_GPIO_Port GPIOA
	#define TCK_Pin GPIO_PIN_14
	#define TCK_GPIO_Port GPIOA
	#define RMII_TX_EN_Pin GPIO_PIN_11
	#define RMII_TX_EN_GPIO_Port GPIOG
	#define RMII_TXD0_Pin GPIO_PIN_13
	#define RMII_TXD0_GPIO_Port GPIOG
	#define SWO_Pin GPIO_PIN_3
	#define SWO_GPIO_Port GPIOB
	#define LD2_Pin GPIO_PIN_7
	#define LD2_GPIO_Port GPIOB
  
#elif defined STM32F303xE
  #define USER_Btn_Pin GPIO_PIN_13
  #define USER_Btn_GPIO_Port GPIOC
  #define MCO_Pin GPIO_PIN_0
  #define MCO_GPIO_Port GPIOF
  #define LD1_Pin GPIO_PIN_0
  #define LD1_GPIO_Port GPIOB
  #define LD3_Pin GPIO_PIN_14
  #define LD3_GPIO_Port GPIOB
  #define STLK_RX_Pin GPIO_PIN_8
  #define STLK_RX_GPIO_Port GPIOD
  #define STLK_TX_Pin GPIO_PIN_9
  #define STLK_TX_GPIO_Port GPIOD
  #define USB_PowerSwitchOn_Pin GPIO_PIN_6
  #define USB_PowerSwitchOn_GPIO_Port GPIOG
  #define USB_OverCurrent_Pin GPIO_PIN_7
  #define USB_OverCurrent_GPIO_Port GPIOG
  #define USB_DM_Pin GPIO_PIN_11
  #define USB_DM_GPIO_Port GPIOA
  #define USB_DP_Pin GPIO_PIN_12
  #define USB_DP_GPIO_Port GPIOA
  #define TMS_Pin GPIO_PIN_13
  #define TMS_GPIO_Port GPIOA
  #define TCK_Pin GPIO_PIN_14
  #define TCK_GPIO_Port GPIOA
  #define SWO_Pin GPIO_PIN_3
  #define SWO_GPIO_Port GPIOB
  #define LD2_Pin GPIO_PIN_7
  #define LD2_GPIO_Port GPIOB

#elif defined STM32F446xx
	#define USER_Btn_Pin GPIO_PIN_13
	#define USER_Btn_GPIO_Port GPIOC
	#define MCO_Pin GPIO_PIN_0
	#define MCO_GPIO_Port GPIOH
	#define LD1_Pin GPIO_PIN_0
	#define LD1_GPIO_Port GPIOB
	#define LD3_Pin GPIO_PIN_14
	#define LD3_GPIO_Port GPIOB
	#define STLK_RX_Pin GPIO_PIN_8
	#define STLK_RX_GPIO_Port GPIOD
	#define STLK_TX_Pin GPIO_PIN_9
	#define STLK_TX_GPIO_Port GPIOD
	#define USB_PowerSwitchOn_Pin GPIO_PIN_6
	#define USB_PowerSwitchOn_GPIO_Port GPIOG
	#define USB_OverCurrent_Pin GPIO_PIN_7
	#define USB_OverCurrent_GPIO_Port GPIOG
	#define USB_SOF_Pin GPIO_PIN_8
	#define USB_SOF_GPIO_Port GPIOA
	#define USB_VBUS_Pin GPIO_PIN_9
	#define USB_VBUS_GPIO_Port GPIOA
	#define USB_ID_Pin GPIO_PIN_10
	#define USB_ID_GPIO_Port GPIOA
	#define USB_DM_Pin GPIO_PIN_11
	#define USB_DM_GPIO_Port GPIOA
	#define USB_DP_Pin GPIO_PIN_12
	#define USB_DP_GPIO_Port GPIOA
	#define TMS_Pin GPIO_PIN_13
	#define TMS_GPIO_Port GPIOA
	#define TCK_Pin GPIO_PIN_14
	#define TCK_GPIO_Port GPIOA
	#define LD2_Pin GPIO_PIN_7
	#define LD2_GPIO_Port GPIOB

#elif defined STM32F722xx
	#define USER_Btn_Pin GPIO_PIN_13
	#define USER_Btn_GPIO_Port GPIOC
	#define MCO_Pin GPIO_PIN_0
	#define MCO_GPIO_Port GPIOH
	#define LD1_Pin GPIO_PIN_0
	#define LD1_GPIO_Port GPIOB
	#define LD3_Pin GPIO_PIN_14
	#define LD3_GPIO_Port GPIOB
	#define STLK_RX_Pin GPIO_PIN_8
	#define STLK_RX_GPIO_Port GPIOD
	#define STLK_TX_Pin GPIO_PIN_9
	#define STLK_TX_GPIO_Port GPIOD
	#define USB_PowerSwitchOn_Pin GPIO_PIN_6
	#define USB_PowerSwitchOn_GPIO_Port GPIOG
	#define USB_OverCurrent_Pin GPIO_PIN_7
	#define USB_OverCurrent_GPIO_Port GPIOG
	#define USB_SOF_Pin GPIO_PIN_8
	#define USB_SOF_GPIO_Port GPIOA
	#define USB_VBUS_Pin GPIO_PIN_9
	#define USB_VBUS_GPIO_Port GPIOA
	#define USB_ID_Pin GPIO_PIN_10
	#define USB_ID_GPIO_Port GPIOA
	#define USB_DM_Pin GPIO_PIN_11
	#define USB_DM_GPIO_Port GPIOA
	#define USB_DP_Pin GPIO_PIN_12
	#define USB_DP_GPIO_Port GPIOA
	#define TMS_Pin GPIO_PIN_13
	#define TMS_GPIO_Port GPIOA
	#define TCK_Pin GPIO_PIN_14
	#define TCK_GPIO_Port GPIOA
	#define SWO_Pin GPIO_PIN_3
	#define SWO_GPIO_Port GPIOB
	#define LD2_Pin GPIO_PIN_7
	#define LD2_GPIO_Port GPIOB
  
#elif defined STM32L4P5xx

  #define USER_Btn_Pin GPIO_PIN_13
  #define USER_Btn_GPIO_Port GPIOC
  #define MCO_Pin GPIO_PIN_0
  #define MCO_GPIO_Port GPIOF
  #define LD1_Pin GPIO_PIN_0
  #define LD1_GPIO_Port GPIOB
  #define LD3_Pin GPIO_PIN_14
  #define LD3_GPIO_Port GPIOB
  #define STLK_RX_Pin GPIO_PIN_8
  #define STLK_RX_GPIO_Port GPIOD
  #define STLK_TX_Pin GPIO_PIN_9
  #define STLK_TX_GPIO_Port GPIOD
  #define USB_PowerSwitchOn_Pin GPIO_PIN_6
  #define USB_PowerSwitchOn_GPIO_Port GPIOG
  #define USB_OverCurrent_Pin GPIO_PIN_7
  #define USB_OverCurrent_GPIO_Port GPIOG
  #define USB_DM_Pin GPIO_PIN_11
  #define USB_DM_GPIO_Port GPIOA
  #define USB_DP_Pin GPIO_PIN_12
  #define USB_DP_GPIO_Port GPIOA
  #define TMS_Pin GPIO_PIN_13
  #define TMS_GPIO_Port GPIOA
  #define TCK_Pin GPIO_PIN_14
  #define TCK_GPIO_Port GPIOA
  #define SWO_Pin GPIO_PIN_3
  #define SWO_GPIO_Port GPIOB
  #define LD2_Pin GPIO_PIN_7
  #define LD2_GPIO_Port GPIOB
  
#elif defined STM32F746xx

  #define USER_Btn_Pin GPIO_PIN_13
  #define USER_Btn_GPIO_Port GPIOC
  #define MCO_Pin GPIO_PIN_0
  #define MCO_GPIO_Port GPIOH
  #define RMII_MDC_Pin GPIO_PIN_1
  #define RMII_MDC_GPIO_Port GPIOC
  #define RMII_REF_CLK_Pin GPIO_PIN_1
  #define RMII_REF_CLK_GPIO_Port GPIOA
  #define RMII_MDIO_Pin GPIO_PIN_2
  #define RMII_MDIO_GPIO_Port GPIOA
  #define RMII_CRS_DV_Pin GPIO_PIN_7
  #define RMII_CRS_DV_GPIO_Port GPIOA
  #define RMII_RXD0_Pin GPIO_PIN_4
  #define RMII_RXD0_GPIO_Port GPIOC
  #define RMII_RXD1_Pin GPIO_PIN_5
  #define RMII_RXD1_GPIO_Port GPIOC
  #define LD1_Pin GPIO_PIN_0
  #define LD1_GPIO_Port GPIOB
  #define RMII_TXD1_Pin GPIO_PIN_13
  #define RMII_TXD1_GPIO_Port GPIOB
  #define LD3_Pin GPIO_PIN_14
  #define LD3_GPIO_Port GPIOB
  #define STLK_RX_Pin GPIO_PIN_8
  #define STLK_RX_GPIO_Port GPIOD
  #define STLK_TX_Pin GPIO_PIN_9
  #define STLK_TX_GPIO_Port GPIOD
  #define USB_PowerSwitchOn_Pin GPIO_PIN_6
  #define USB_PowerSwitchOn_GPIO_Port GPIOG
  #define USB_OverCurrent_Pin GPIO_PIN_7
  #define USB_OverCurrent_GPIO_Port GPIOG
  #define USB_SOF_Pin GPIO_PIN_8
  #define USB_SOF_GPIO_Port GPIOA
  #define USB_VBUS_Pin GPIO_PIN_9
  #define USB_VBUS_GPIO_Port GPIOA
  #define USB_ID_Pin GPIO_PIN_10
  #define USB_ID_GPIO_Port GPIOA
  #define USB_DM_Pin GPIO_PIN_11
  #define USB_DM_GPIO_Port GPIOA
  #define USB_DP_Pin GPIO_PIN_12
  #define USB_DP_GPIO_Port GPIOA
  #define TMS_Pin GPIO_PIN_13
  #define TMS_GPIO_Port GPIOA
  #define TCK_Pin GPIO_PIN_14
  #define TCK_GPIO_Port GPIOA
  #define RMII_TX_EN_Pin GPIO_PIN_11
  #define RMII_TX_EN_GPIO_Port GPIOG
  #define RMII_TXD0_Pin GPIO_PIN_13
  #define RMII_TXD0_GPIO_Port GPIOG
  #define SW0_Pin GPIO_PIN_3
  #define SW0_GPIO_Port GPIOB
  #define LD2_Pin GPIO_PIN_7
  #define LD2_GPIO_Port GPIOB

#endif


#ifdef __cplusplus
}
#endif
