#include "hal/hal.h"
#include "main.h"
#include "benchmark/benchmark.h"

struct System
{
  System()
  {
    HAL_Init();
    SystemClock_Config();
    MX_GPIO_Init();
  }
};

struct SerialOutput
{
  SerialOutput()
  {
    MX_UART_Init();
  }
  
  template<int N>
  void transmit(char const (&msg)[N], bool newline = false)
  {
    HAL_UART_Transmit(&huart, (uint8_t*)msg, N - 1, 1000);
    if (newline)
      transmit("\r\n", false);
  }
  
  template<int N>
  void transmit(char (&msg)[N], bool newline = false)
  {
    transmit(static_cast<char const(&)[N]>(msg), newline);
  }
  
  void transmit(char const *msg, bool newline = false)
  {
    int len = 0;
    for (auto ch = msg; *ch != '\0'; ++ch)
      ++len;
    
    HAL_UART_Transmit(&huart, (uint8_t*)msg, len, 1000);
    if (newline)
      transmit("\r\n", false);
  }

  void transmit(uint32_t value, int width = 0, bool newline = false)
  {
    char buffer[20];
    for (auto i = 0u; i < sizeof(buffer); ++i)
      buffer[i] = ' ';
    
    char *out = &buffer[sizeof(buffer) - 1];
    do {
      *out-- = '0' + (value % 10);
      value /= 10;
    } while ((value != 0));
    ++out;
    
    auto len = sizeof(buffer) - (out - buffer);
    if (width > 0 && len < width) {
      len = width;
      out = &buffer[sizeof(buffer) - len];
    }
    
    HAL_UART_Transmit(&huart, (uint8_t*)out, len, 1000);
    if (newline)
      transmit("\r\n", false);
  }
};

struct Device
{
  System system;
  SerialOutput ser;
  
  void setStatusLed(bool state = true) const
  {
    HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, state ? GPIO_PIN_SET : GPIO_PIN_RESET);
  }
  
  void toggleStatusLed() const
  {
    HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
  }
};

// ---


class SerialReporter final : public benchmark::Reporter
{
public:
  explicit SerialReporter(SerialOutput &ser)
    : ser{ser}
  {}

  virtual void beginOfReport(int total) override
  {
    ser.transmit("***** BENCHMARK INFO ******", true);
    
    ser.transmit("Device          : ");
    ser.transmit(TARGET_MCU, true);
    
    ser.transmit("Toolchain       : ");
    ser.transmit(TEST_TOOLCHAIN, true);
    ser.transmit("Optimization    : ");
    ser.transmit(TEST_OPTIMIZATION_GOAL, true);
    
    ser.transmit("Category        : ");
    ser.transmit(TEST_CATEGORY, true);
    
    ser.transmit("Benchmark       : ");
    ser.transmit(TEST_BENCHMARK, true);
    
    ser.transmit("Iterations limit: ");
    ser.transmit(TEST_ITERATIONS_LIMIT, 0, true);
    
    ser.transmit("Step count      : ");
    ser.transmit(total, 0, true);
    
    ser.transmit("CycleTimer      : ");
    ser.transmit(benchmark::CycleTimer::id, true);
    
    
    ser.transmit("*** BEGIN OF BENCHMARKS ***", true);
  }
  
  virtual void endOfReport() override
  {
    ser.transmit("**** END OF BENCHMARKS ****", true);
  }
  
  virtual void beginOfBenchmark(char const *id, int no, int total) override
  {
    ser.transmit("=== (");
    ser.transmit(no);
    ser.transmit("/");
    ser.transmit(total);
    ser.transmit(") [");
    ser.transmit(id);
    ser.transmit("] ===", true);
  }
  
  virtual void endOfBenchmark(char const *id, int no, int total, int stack_usage = -1) override
  {
    ser.transmit("### Stack usage: ");
    ser.transmit(stack_usage);
    ser.transmit(" ###", true);
  }
  
  virtual void beginOfMeasurement(int no, int total, int iterations) override
  {
    ser.transmit("[");
    ser.transmit(no, 2);
    ser.transmit("/");
    ser.transmit(total, 2);
    ser.transmit("] iterations: ");
    ser.transmit(iterations, 5);
  }
  
  virtual void endOfMeasurement(int no, int total, int cycles) override
  {
    ser.transmit(", count: ");
    ser.transmit(cycles, 10, true);
  }  
  
private:
  SerialOutput ser;
};


// ---

int main()
{
  Device dev;
  dev.setStatusLed(true);
  
  SerialReporter reporter{dev.ser};
  benchmark::BenchmarkRunner runner{reporter};
  runner.run();
  
  dev.setStatusLed(false);
  
  while (1);
}
