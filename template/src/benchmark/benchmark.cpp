#include "benchmark.h"

#if BENCHMARK_CC == CC_GNU

namespace __gnu_cxx {
    void __verbose_terminate_handler() 
    {
      HardFault_Handler();
    }
}

extern "C" void __cxa_pure_virtual(void) {
  while (1) ;
}
#endif

namespace benchmark
{

Benchmark BenchmarkRunner::benchmarks[BenchmarkRunner::max_benchmarks];
uint32_t BenchmarkRunner::benchmark_count = 0u;

#if __CORTEX_M <= 2 || defined STM32F303xE || defined STM32F746xx
char const* CycleTimer::id = "SysTick Counter";
#else
char const* CycleTimer::id = "Data Watchpoint and Trace Unit";
#endif

void UseCharPointer(char const volatile*) {}
  
}
