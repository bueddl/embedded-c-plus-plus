#pragma once

#include "hal/hal.h"

#define CC_GNU 1
#define CC_ARMCC 2
#define CC_ARMCLANG 3
#define CC_DIAB 4
#define CC_IAR 5
#define CC_CLANG 6

#if defined __GNUC__ && !defined __ARMCC_VERSION
    // GCC & Clang
    #define BENCHMARK_ALWAYS_INLINE __attribute__((always_inline))
    #define BENCHMARK_NEVER_INLINE __attribute__((noinline))
    #define BENCHMARK_NAKED __attribute__((naked))
    #define BENCHMARK_CC CC_GNU

#elif defined __ARMCC_VERSION && __ARMCC_VERSION < 6000000
    // Keil/ARMCC
    #define BENCHMARK_ALWAYS_INLINE __forceinline
    #define BENCHMARK_NEVER_INLINE __declspec(noinline)
    #define BENCHMARK_NAKED /* no equivalent? */
    #define BENCHMARK_CC CC_ARMCC
    #define BENCHMARK_HAS_NO_INLINE_ASSEMBLY

#elif defined __ARMCC_VERSION && __ARMCC_VERSION >= 6000000 
    // Keil/ARMClang
    #define BENCHMARK_ALWAYS_INLINE __attribute__((always_inline))
    #define BENCHMARK_NEVER_INLINE __attribute__((noinline))
    #define BENCHMARK_NAKED __attribute__((naked))
    #define BENCHMARK_CC CC_ARMCLANG

#elif defined __DCC__ && defined _DIAB_TOOL
    // WindRiver Diab
    #define BENCHMARK_ALWAYS_INLINE __attribute__((always_inline))
    #define BENCHMARK_NEVER_INLINE __attribute__((noinline))
    #define BENCHMARK_NAKED /* TODO */
    #define BENCHMARK_CC CC_DIAB
    #define BENCHMARK_HAS_NO_INLINE_ASSEMBLY

#elif defined __ICCARM__ 
    // IAR
    #define BENCHMARK_ALWAYS_INLINE _Pragma("inline=forced")
    #define BENCHMARK_NEVER_INLINE _Pragma("inline=never")
    #define BENCHMARK_NAKED /* TODO */
    #define BENCHMARK_CC CC_IAR
    #define BENCHMARK_HAS_NO_INLINE_ASSEMBLY

#else
   #error "unknown compiler"
#endif

#if BENCHMARK_CC == CC_GNU || BENCHMARK_CC == CC_CLANG
extern "C" char _Min_Stack_Size;
extern "C" char _estack;
#elif BENCHMARK_CC == CC_ARMCC || BENCHMARK_CC == CC_ARMCLANG
extern "C" char STACK$$Base;
extern "C" char STACK$$Limit;
#elif BENCHMARK_CC == CC_IAR
// not needed, use __section_begin operator instead (see EWARM Dev C/C++ UG, p.505)
#else
  #error "Don't know how to retreive stack size for this compiler."
#endif

#define _nop asm volatile("nop")

BENCHMARK_ALWAYS_INLINE 
static void dummy_work()
{
    _nop;
    _nop;
    _nop;
    _nop;
    _nop;
}

#if BENCHMARK_CC == CC_GNU

extern "C" void HardFault_Handler(void);

namespace __gnu_cxx {
    void __verbose_terminate_handler(); 
}

extern "C" void __cxa_pure_virtual(void);
#endif


namespace benchmark
{

#if __CORTEX_M <= 2 || defined STM32F303xE || defined STM32F746xx

struct CycleTimer
{
  static char const *id;
  uint32_t begin = 0;
  uint32_t count = 0;
  
  CycleTimer()
  {
    SysTick->CTRL &= 
        (1u << SysTick_CTRL_CLKSOURCE_Pos) & SysTick_CTRL_CLKSOURCE_Msk
      | (1u << SysTick_CTRL_ENABLE_Pos)    & SysTick_CTRL_ENABLE_Msk;
    SysTick->LOAD = (1u << 24u) - 1u;
  }

  inline void start()
  {
    count = 0;
    resume();
  }

  inline uint32_t stop()
  {
    pause();
    return count;
  }

  inline void pause()
  {
    auto end = static_cast<uint32_t>(SysTick->VAL & SysTick_VAL_CURRENT_Msk);
    uint32_t measure_delay = (begin - end) & SysTick_VAL_CURRENT_Msk;
    __enable_irq();
    count += measure_delay;
  }

  inline void resume()
  {
    __disable_irq();
    SysTick->VAL = 0;
    begin = static_cast<uint32_t>(SysTick->VAL & SysTick_VAL_CURRENT_Msk);
  }
};

#else

struct CycleTimer
{
  static char const *id;
  uint32_t count = 0;

  CycleTimer()
  {
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
  }

  inline void start()
  {
    count = 0;
    resume();
  }

  inline uint32_t stop()
  {
    pause();
    return count;
  }

  inline void pause()
  {
    DWT->CTRL = 0x40000000;
    __enable_irq();
    count += DWT->CYCCNT;
  }

  inline void resume()
  {
    __disable_irq();
    DWT->CTRL = 0x40000001;
    DWT->CYCCNT = 0;
  }
};

#endif


// Stack Watermarking
// ref: http://www.keil.com/appnotes/files/apnt_316.pdf
struct StackChecker
{
  constexpr static char marker_byte = 0x42;

  BENCHMARK_ALWAYS_INLINE
  char* get_sp() const
  {
    #if BENCHMARK_CC == CC_GNU || BENCHMARK_CC == CC_CLANG || BENCHMARK_CC == CC_ARMCLANG
    char *stack_pointer = (char*)-1;
    asm("mov %[stack_pointer], sp" : [stack_pointer] "=r"(stack_pointer) );
    return stack_pointer;
    #elif BENCHMARK_CC == CC_ARMCC
    return (char*)__current_sp();
    #elif BENCHMARK_CC == CC_IAR
    return (char*)__get_SP();
    #else
      #error "Not implemented."
    #endif
  }

  char* end_of_stack() const
  {
    #if BENCHMARK_CC == CC_GNU || BENCHMARK_CC == CC_CLANG
    return &_estack - (int)&_Min_Stack_Size;
    #elif BENCHMARK_CC == CC_ARMCC || BENCHMARK_CC == CC_ARMCLANG
    return &STACK$$Base;
    #elif BENCHMARK_CC == CC_IAR
    #pragma section="CSTACK"
    return (char*)__section_begin("CSTACK");
    #else
      #error "Not implemented."
    #endif
  }

#if BENCHMARK_CC == CC_GNU || BENCHMARK_CC == CC_CLANG || BENCHMARK_CC == CC_ARMCLANG
// for GCC/Clang/ARMClang
#pragma GCC push_options
#pragma GCC optimize ("O0")

#elif BENCHMARK_CC == CC_ARMCC
// for ARMCC
#pragma push
#pragma O0

#elif BENCHMARK_CC == CC_IAR
// for IAR (only affects this function)
#pragma optimize=none
#endif
  BENCHMARK_NEVER_INLINE
  void setup()
  {
    char *stack_end = end_of_stack();
    for (char* sp = get_sp() - 4; sp >= stack_end; --sp) {
      *sp = marker_byte;
    }
  }
#if BENCHMARK_CC == CC_ARMCC
// for ARMCC
#pragma pop

#elif BENCHMARK_CC == CC_GNU || BENCHMARK_CC == CC_CLANG || BENCHMARK_CC == CC_ARMCLANG
// for GCC/Clang
#pragma GCC pop_options
#endif

// ------

#if BENCHMARK_CC == CC_GNU || BENCHMARK_CC == CC_CLANG || BENCHMARK_CC == CC_ARMCLANG
// for GCC/Clang/ARMClang
#pragma GCC push_options
#pragma GCC optimize ("O0")

#elif BENCHMARK_CC == CC_ARMCC
// for ARMCC
#pragma push
#pragma O0

#elif BENCHMARK_CC == CC_IAR
// for IAR (only affects this function)
#pragma optimize=none
#endif
  BENCHMARK_NEVER_INLINE
  int stack_usage()
  {
    char *end_of_used_stack = end_of_stack();
    char *sp = get_sp();  
    for (; *end_of_used_stack == marker_byte; ++end_of_used_stack)
      if (end_of_used_stack == sp) return 0;
    if (end_of_used_stack == end_of_stack())
      return -1;
    return sp - end_of_used_stack;
  }
#if BENCHMARK_CC == CC_ARMCC
// for ARMCC
#pragma pop

#elif BENCHMARK_CC == CC_GNU || BENCHMARK_CC == CC_CLANG || BENCHMARK_CC == CC_ARMCLANG
// for GCC/Clang
#pragma GCC pop_options
#endif

};


class Reporter
{
public:
  virtual void beginOfReport(int total) = 0;
  virtual void endOfReport() = 0;
  virtual void beginOfBenchmark(char const *id, int no, int total) = 0;
  virtual void endOfBenchmark(char const *id, int no, int total, int stack_usage = -1) = 0;
  virtual void beginOfMeasurement(int no, int total, int iterations) = 0;
  virtual void endOfMeasurement(int no, int total, int cycles) = 0;
};
  
struct State;

using benchmark_fn = void(State &bench);

struct Benchmark
{
  char const *id;
  benchmark_fn *fn;
};

struct State
{
    uint32_t N;
    CycleTimer &timer;

    struct DummyIterator
    {
        uint32_t value;

        explicit DummyIterator(uint32_t value)
            : value{value}
        {}

        friend bool operator!=(DummyIterator lhs, DummyIterator rhs)
        { return lhs.value != rhs.value; }

        DummyIterator operator++()
        {
            ++value;
            return *this;
        }

        uint32_t operator*() const
        { return value; }
    };

    DummyIterator begin()
    { return DummyIterator{0}; }

    DummyIterator end()
    { return DummyIterator{N}; }
};

struct BenchmarkRunner
{  
  static const auto max_benchmarks = 8u;

  static Benchmark benchmarks[max_benchmarks];
  static uint32_t benchmark_count;
  Reporter &reporter;
  
  void run()
  {
    CycleTimer timer;

    uint32_t total = 1;
    for (uint32_t i = 1; i < TEST_ITERATIONS_LIMIT; i <<= 1u)
      ++total;

    reporter.beginOfReport(total);
    
    for (int no = 0; no < benchmark_count; ++no) {
      auto &benchmark = benchmarks[no];
      
      reporter.beginOfBenchmark(benchmark.id, no + 1u, benchmark_count);
      StackChecker stack_checker;
      int stack_usage = -1;
 
      for (uint32_t iteration_no = 0; iteration_no < total; ++iteration_no) {
        uint32_t iterations = 1u << (iteration_no);
        State state{iterations, timer};
    
        reporter.beginOfMeasurement(iteration_no + 1u, total, iterations);
        if (iteration_no == 0)
          stack_checker.setup();     
        timer.start();
        
        benchmark.fn(state);
        
        auto count = timer.stop();
        if (iteration_no == 0)
          stack_usage = stack_checker.stack_usage();
        reporter.endOfMeasurement(iteration_no + 1u, total, count);
      }
      
      reporter.endOfBenchmark(benchmark.id, no + 1u, benchmark_count, stack_usage);
    }
    
    reporter.endOfReport();
  }
};

inline Benchmark& RegisterBenchmark(char const *id, benchmark_fn *fn)
{
    BenchmarkRunner::benchmarks[BenchmarkRunner::benchmark_count] = Benchmark{id, fn};
    return BenchmarkRunner::benchmarks[BenchmarkRunner::benchmark_count++];
}

#define INTERN_TO_STRING(x) #x
#define TO_STRING(x) INTERN_TO_STRING(x)
#define BENCHMARK(x) \
  auto &Benchmark_ ## x = RegisterBenchmark(TO_STRING(x), x);



// Taken from google benchmark and slightly modified to work with the compilers used in this BA.
// https://github.com/google/benchmark/blob/master/include/benchmark/benchmark.h
void UseCharPointer(char const volatile*);

#ifndef BENCHMARK_HAS_NO_INLINE_ASSEMBLY

template <class Tp>
BENCHMARK_ALWAYS_INLINE
inline void DoNotOptimize(Tp const& value) {
  asm volatile("" : : "r,m"(value) : "memory");
}

template<class Tp>
BENCHMARK_ALWAYS_INLINE
inline void DoNotOptimize(Tp& value) {
#if defined(__clang__)
  asm volatile("" : "+r,m"(value) : : "memory");
#else
  asm volatile("" : "+m,r"(value) : : "memory");
#endif
}

// Force the compiler to flush pending writes to global memory. Acts as an
// effective read/write barrier
BENCHMARK_ALWAYS_INLINE
inline void ClobberMemory() {
  asm volatile("" : : : "memory");
}

#elif BENCHMARK_CC == CC_IAR

BENCHMARK_ALWAYS_INLINE
template<class Tp>
inline void DoNotOptimize(Tp const& value) {
  UseCharPointer(&reinterpret_cast<char const volatile&>(value));
}

#else

template<class Tp>
BENCHMARK_ALWAYS_INLINE
inline void DoNotOptimize(Tp const& value) {
  UseCharPointer(&reinterpret_cast<char const volatile&>(value));
}
#endif
  
}
