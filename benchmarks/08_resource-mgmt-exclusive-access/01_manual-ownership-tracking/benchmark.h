#pragma once

#include <benchmark/benchmark.h>

struct MyData
{
  char payload[0x10];
};

void use(MyData *data);
