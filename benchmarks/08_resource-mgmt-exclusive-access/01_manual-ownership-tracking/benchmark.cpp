#include "benchmark.h"

static void Construct(benchmark::State &state)
{
  state.timer.pause();

  for (auto _ : state) {
    state.timer.resume();
    MyData *data = new MyData();

    benchmark::DoNotOptimize(data);
    state.timer.pause();

    delete data;
  }

  state.timer.resume();
}
BENCHMARK(Construct);

static void PassToUser(benchmark::State &state)
{
  MyData *data = nullptr;
    
  for (auto _ : state) {
    benchmark::DoNotOptimize(data);
    use(data);
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(PassToUser);

static void ConstructDestruct(benchmark::State &state)
{
  for (auto _ : state) {
    MyData *data = new MyData;
    benchmark::DoNotOptimize(data);
    delete data;
  }
}
BENCHMARK(ConstructDestruct);
