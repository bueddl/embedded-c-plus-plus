#include "benchmark.h"

static void Construct(benchmark::State &state)
{
  state.timer.pause();

  for (auto _ : state) {
    state.timer.resume();
    std::unique_ptr<MyData> data = std::make_unique<MyData>();
    benchmark::DoNotOptimize(data);

    state.timer.pause();
  }

  state.timer.resume();
}
BENCHMARK(Construct);

void use(std::unique_ptr<MyData> data);


static void PassToUser(benchmark::State &state)
{
  std::unique_ptr<MyData> data;
    
  for (auto _ : state) {
    benchmark::DoNotOptimize(data);
    use(std::move(data));
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(PassToUser);

static void ConstructDestruct(benchmark::State &state)
{
  for (auto _ : state) {
    std::unique_ptr<MyData> data = std::make_unique<MyData>();
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(ConstructDestruct);
