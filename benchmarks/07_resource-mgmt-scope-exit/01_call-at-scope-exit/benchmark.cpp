#include "benchmark.h"

int global_int = 0;

void cleanup()
{
    dummy_work(); // run at exit of scope
}

void process(int val)
{
  benchmark::DoNotOptimize(val);

  global_int = 0;

  if (val == 0) {
    cleanup();
    return;
  }

  global_int = 1;

  if (val == 1) {
    cleanup();
    return;
  }

  global_int = 2;

  cleanup();
  return;
}

static void ManualCleanup(benchmark::State &state)
{
  for (auto _ : state) {
    process(0);
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(ManualCleanup);
