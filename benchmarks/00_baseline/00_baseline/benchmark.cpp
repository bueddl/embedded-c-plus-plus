#include <benchmark/benchmark.h>

static void Baseline(benchmark::State &state) 
{
  for (auto _ : state)
    dummy_work();
}
BENCHMARK(Baseline);
