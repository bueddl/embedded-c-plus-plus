#pragma once

#include <benchmark/benchmark.h>
#include <cstddef>
#include <functional>

#if BENCHMARK_CC == CC_ARMCC
namespace std
{
  template<typename T, std::size_t N>
  constexpr T* begin(T (&arr)[N]) noexcept
  {
    return &arr[0];
  }

  template<typename T, std::size_t N>
  constexpr T* end(T (&arr)[N]) noexcept
  {
    return &arr[N];
  }
}
#else
#include <iterator>
#endif

template<typename PredicateType>
size_t count_if(int const *begin, int const *end, PredicateType &&pred)
{
  size_t count = 0;
  for (int const *iter = begin; iter != end; ++iter) {
    if (pred(*iter))
      ++count;
  }
  return count;
}