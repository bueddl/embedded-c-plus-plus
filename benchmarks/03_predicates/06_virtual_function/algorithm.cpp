#include "benchmark.h"

size_t count_if(int const *begin, int const *end, PredicateType pred)
{
  size_t count = 0;
  for (int const *iter = begin; iter != end; ++iter) {
    if (pred.matches(*iter))
      ++count;
  }
  return count;
}