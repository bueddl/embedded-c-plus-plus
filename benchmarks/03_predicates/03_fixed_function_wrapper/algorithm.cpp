#include "benchmark.h"

size_t count_if(int const *begin, int const *end, PredicateType pred)
{
  size_t count = 0;
  for (int const *iter = begin; iter != end; ++iter) {
    if (pred(*iter))
      ++count;
  }
  return count;
}

size_t large_count_if(int const *begin, int const *end, basic_function<bool(int), 80> pred)
{
  size_t count = 0;
  for (int const *iter = begin; iter != end; ++iter) {
    if (pred(*iter))
      ++count;
  }
  return count;
}
