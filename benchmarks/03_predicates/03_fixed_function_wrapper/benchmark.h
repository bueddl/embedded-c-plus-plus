#pragma once

#include <benchmark/benchmark.h>
#include <cstddef>

#if BENCHMARK_CC == CC_ARMCC
namespace std
{
  template<typename T, std::size_t N>
  constexpr T* begin(T (&arr)[N]) noexcept
  {
    return &arr[0];
  }

  template<typename T, std::size_t N>
  constexpr T* end(T (&arr)[N]) noexcept
  {
    return &arr[N];
  }
}
#else
#include <iterator>
#endif

#include <new>

#if BENCHMARK_CC == CC_ARMCC
namespace std
{

template<typename T>
struct remove_reference
{
  using type = T;
};

template<typename T>
struct remove_reference<T&>
{
  using type = T;
};

template<typename T>
struct remove_reference<T&&>
{
  using type = T;
};

template<typename T>
inline T&& forward(typename remove_reference<T>::type& t) noexcept
{
    return static_cast<T&&>(t);
}

template<typename T>
inline T&& forward(typename remove_reference<T>::type&& t) noexcept
{
    return static_cast<T&&>(t);
}

using nullptr_t = decltype(nullptr);

}
#endif

template<typename Sig, unsigned int BufferSize>
class basic_function;

template<typename R, typename... As, unsigned int BufferSize>
class basic_function<R(As...), BufferSize>
{
public:
  using function_type = R(As...);

  basic_function() noexcept = default;
  basic_function(std::nullptr_t) noexcept
  {}

  basic_function(basic_function const&) noexcept = default;
  basic_function(basic_function&&) noexcept = default;

  basic_function(function_type *fn_ptr) noexcept
  {
    from_function_ptr(fn_ptr);
  }

  template<typename F>
  basic_function(F&& fn) noexcept
  {
    from_functor(std::forward<F>(fn));
  }

  basic_function& operator=(basic_function const&) = default;
  basic_function& operator=(basic_function&&) noexcept = default;

  basic_function& operator=(function_type *fn_ptr) noexcept
  {
    from_function_ptr(fn_ptr);
    return *this;
  }

  R operator()(As... args) noexcept
  {
    return helper_(*this, args...);
  }

  explicit operator bool() const noexcept
  { return helper_ != nullptr; }

private:
  using helper_type = R(basic_function const&, As...);

  helper_type *helper_ = nullptr;
  char storage_[BufferSize];

  void from_function_ptr(function_type *fn_ptr) noexcept
  {
    new (storage_) function_type*{fn_ptr};
    helper_ = [](basic_function const& self, As... args) -> R {
      return (*reinterpret_cast<function_type* const*>(&self.storage_))(args...);
    };
  }

  template<typename F>
  void from_functor(F&& fn) noexcept
  {
    static_assert(sizeof(F) <= BufferSize, "basic_function: buffer to small, increase BufferSize");

    new (storage_) F{std::forward<F>(fn)};
    helper_ = [](basic_function const &self, As... args) -> R {
      return (*reinterpret_cast<F const*>(&self.storage_))(args...);
    };
  }
};

template<typename Sig>
using function = basic_function<Sig, 8>;

using PredicateType = function<bool(int elem)>;

size_t count_if(int const *begin, int const *end, PredicateType pred);

size_t large_count_if(int const *begin, int const *end, basic_function<bool(int elem), 80>);
