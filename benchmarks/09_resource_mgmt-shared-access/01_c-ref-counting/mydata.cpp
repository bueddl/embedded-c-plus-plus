#include "benchmark.h"

static void
MyData_free(const struct ref *ref)
{
    struct MyData *mydata = container_of(ref, struct MyData, refcount);
    free(mydata);
}

struct MyData *MyData_create()
{
    struct MyData *mydata = (struct MyData*)malloc(sizeof(*mydata));
    memset((void*)&mydata->payload, 0, sizeof(mydata->payload));
    mydata->refcount.free = MyData_free;
    mydata->refcount.count = 1;
    return mydata;
}
