#include "benchmark.h"

static void Construct(benchmark::State &state)
{
  state.timer.pause();

  for (auto _ : state) {
    state.timer.resume();
    MyData *data = MyData_create();
    benchmark::DoNotOptimize(data);

    state.timer.pause();  // Note: destruct of data deliberately not measured this way!
    ref_dec(&data->refcount);
  }

  state.timer.resume();
}
BENCHMARK(Construct);

static void PassToUser(benchmark::State &state)
{
  state.timer.pause();
  MyData *data = MyData_create();
  benchmark::DoNotOptimize(data);
    
  for (auto _ : state) {
    state.timer.resume();
    ref_inc(&data->refcount);
    use(data);
    state.timer.pause();
    benchmark::DoNotOptimize(data);
  }
  
  ref_dec(&data->refcount);
  state.timer.resume();
}
BENCHMARK(PassToUser);

static void ConstructDestruct(benchmark::State &state)
{
  for (auto _ : state) {
    MyData *data = MyData_create();
    benchmark::DoNotOptimize(data);
    ref_dec(&data->refcount);
  }
}
BENCHMARK(ConstructDestruct);
