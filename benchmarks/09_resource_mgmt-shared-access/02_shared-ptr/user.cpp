#include "benchmark.h"

void use(std::shared_ptr<MyData> data)
{
    benchmark::DoNotOptimize(data);
}
