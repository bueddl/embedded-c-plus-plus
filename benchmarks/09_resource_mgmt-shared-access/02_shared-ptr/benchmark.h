#pragma once

#include <benchmark/benchmark.h>
#include <memory>

struct MyData
{
  char payload[0x10];
};

void use(std::shared_ptr<MyData> data);
