#include "benchmark.h"

static void Construct(benchmark::State &state)
{
  state.timer.pause();

  for (auto _ : state) {
    state.timer.resume();
    std::shared_ptr<MyData> data = std::make_shared<MyData>();
    benchmark::DoNotOptimize(data);

    state.timer.pause(); // Note: destruct of data deliberately not measured this way!
  }

  state.timer.resume();
}
BENCHMARK(Construct);

static void PassToUser(benchmark::State &state)
{
  std::shared_ptr<MyData> data;
  benchmark::DoNotOptimize(data);
    
  for (auto _ : state) {
    use(data);
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(PassToUser);

static void ConstructDestruct(benchmark::State &state)
{
  for (auto _ : state) {
    std::shared_ptr<MyData> data = std::make_shared<MyData>();
    benchmark::DoNotOptimize(data);
  }
}
BENCHMARK(ConstructDestruct);
