#include "benchmark.h"

static void DynamicCast(benchmark::State &state)
{
  auto& packet = Network_Receive_NextPacket();

  for (auto _ : state) {
    if (auto *current_engine_data = dynamic_cast<CurrentEngineData const*>(packet.payload)) {
      benchmark::DoNotOptimize(current_engine_data);
      benchmark::DoNotOptimize(current_engine_data->rpm);
      benchmark::DoNotOptimize(current_engine_data->temp);
    } else 
    if (auto *acceleration_pedal_position = dynamic_cast<AcceleratorPadelPosition const*>(packet.payload)) {
      benchmark::DoNotOptimize(acceleration_pedal_position);
      benchmark::DoNotOptimize(acceleration_pedal_position->position);
    } else
    if (auto *door_open_status = dynamic_cast<DoorOpenStatus const*>(packet.payload)) {
      benchmark::DoNotOptimize(door_open_status);
      benchmark::DoNotOptimize(door_open_status->closed);
    }
  }
}
BENCHMARK(DynamicCast);
