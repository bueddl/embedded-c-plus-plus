#include "benchmark.h"

static void Inheritance(benchmark::State &state)
{
  auto& packet = Network_Receive_NextPacket();

  for (auto _ : state)
    packet.payload->handle_received();
}
BENCHMARK(Inheritance);
