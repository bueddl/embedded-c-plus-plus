#include "benchmark.h"

CANPacket& Network_Receive_NextPacket()
{
  static Payload_CurrentEngineData payload{};
  static CANPacket packet{&payload};
  return packet;
}
