#pragma once
#include <benchmark/benchmark.h>

struct Payload_Base
{
  virtual ~Payload_Base() {}
  virtual void handle_received() const = 0;
};

struct CANPacket
{
  // ... can stuff
  Payload_Base *payload;
};

struct Payload_CurrentEngineData : Payload_Base
{
  int rpm;
  short temp;

  void handle_received() const override
  {
    benchmark::DoNotOptimize(this);
    benchmark::DoNotOptimize(rpm);
    benchmark::DoNotOptimize(temp);
  }
};

struct Payload_AcceleratorPadelPosition : Payload_Base
{
  char position;

  void handle_received() const override
  {
    benchmark::DoNotOptimize(this);
    benchmark::DoNotOptimize(position);
  }
};

struct Payload_DoorOpenStatus : Payload_Base
{
  bool closed;

  void handle_received() const override
  {
    benchmark::DoNotOptimize(this);
    benchmark::DoNotOptimize(closed);
  }
};

CANPacket& Network_Receive_NextPacket();
