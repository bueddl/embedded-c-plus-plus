#include "benchmark.h"

static void VisitorDoubleDispatch(benchmark::State &state)
{
  PacketReceivedHandler received_handler;
  auto& packet = Network_Receive_NextPacket();

  for (auto _ : state)
    packet.payload->apply(received_handler);
}
BENCHMARK(VisitorDoubleDispatch);
