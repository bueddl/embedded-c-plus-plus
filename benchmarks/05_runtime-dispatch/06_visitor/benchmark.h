#pragma once
#include <benchmark/benchmark.h>
// --

class IVisitor;

struct Payload_Base
{
  virtual void apply(IVisitor &visitor) const = 0;
};

struct CANPacket
{
  // ... can stuff
  Payload_Base *payload;
};

struct Payload_CurrentEngineData : Payload_Base
{
  int rpm;
  short temp;

  void apply(IVisitor &visitor) const override;
};

struct Payload_AcceleratorPadelPosition : Payload_Base
{
  char position;

  void apply(IVisitor &visitor) const override;
};

struct Payload_DoorOpenStatus : Payload_Base
{
  bool closed;

  void apply(IVisitor &visitor) const override;
};


class IVisitor
{
public:
  virtual void visit(Payload_CurrentEngineData const &) = 0;
  virtual void visit(Payload_AcceleratorPadelPosition const &) = 0;
  virtual void visit(Payload_DoorOpenStatus const &) = 0;
};

class PacketReceivedHandler : public IVisitor
{
  void visit(Payload_CurrentEngineData const &node) override;
  void visit(Payload_AcceleratorPadelPosition const &node) override;
  void visit(Payload_DoorOpenStatus const &node) override;
};

CANPacket& Network_Receive_NextPacket();
