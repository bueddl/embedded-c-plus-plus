#include "benchmark.h"

static void Variant(benchmark::State &state)
{
  auto handle_received = overload(
    [](CurrentEngineData const &current_engine_data)
    {
      benchmark::DoNotOptimize(current_engine_data);
      benchmark::DoNotOptimize(current_engine_data.rpm);
      benchmark::DoNotOptimize(current_engine_data.temp);
    },
    [](AcceleratorPadelPosition const &acceleration_pedal_position)
    {
      benchmark::DoNotOptimize(acceleration_pedal_position);
      benchmark::DoNotOptimize(acceleration_pedal_position.position);
    },
    [](DoorOpenStatus const &door_open_status)
    {
      benchmark::DoNotOptimize(door_open_status);
      benchmark::DoNotOptimize(door_open_status.closed);
    },
    [](auto)
    {
      // others
    }
  );

  auto& packet = Network_Receive_NextPacket();

  for (auto _ : state)
    mpark::visit(handle_received, packet.payload);
}
BENCHMARK(Variant);
