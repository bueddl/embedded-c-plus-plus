#include "benchmark.h"

int current_engine_data_payload_handle_receive(void *user)
{
  current_engine_data_payload_t *payload = (current_engine_data_payload_t*)user;
  benchmark::DoNotOptimize(payload);
  return 0;
}

const struct payload_ops current_engine_data_payload_ops = {
  current_engine_data_payload_handle_receive
};

// ---

int accelerator_padel_position_payload_handle_receive(void *user)
{
  accelerator_padel_position_payload_t *payload = (accelerator_padel_position_payload_t*)user;
  benchmark::DoNotOptimize(payload);
  return 0;
}

const struct payload_ops accelerator_padel_position_payload_ops = {
  accelerator_padel_position_payload_handle_receive
};

// ---

int door_open_status_payload_handle_receive(void *user)
{
  door_open_status_payload_t *payload = (door_open_status_payload_t*)user;
  benchmark::DoNotOptimize(payload);
  return 0;
}

const struct payload_ops door_open_status_payload_ops = {
  door_open_status_payload_handle_receive
};

// ---
