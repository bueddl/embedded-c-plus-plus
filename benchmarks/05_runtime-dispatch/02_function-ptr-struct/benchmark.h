#pragma once

#include <benchmark/benchmark.h>

typedef struct current_engine_data_payload 
{
  int rpm;
  short temp;
} current_engine_data_payload_t;

typedef struct accelerator_padel_position_payload 
{
  char position;
} accelerator_padel_position_payload_t;

typedef struct door_open_status_payload 
{
  bool closed;
} door_open_status_payload_t;

struct payload_ops {
  int (*handle_receive)(void *user);
  // ...
};

typedef struct payload {
  const struct payload_ops *ops;
  void *decoded_payload;
  // ...
} packet_payload_t;

typedef struct can_packet
{
  // can packet...
  packet_payload_t payload;
} can_packet_t;

can_packet_t* Network_Receive_NextPacket();

extern const struct payload_ops current_engine_data_payload_ops;
extern const struct payload_ops accelerator_padel_position_payload_ops;
extern const struct payload_ops door_open_status_payload_ops;
