#include "benchmark.h"

static void DispatchCode(benchmark::State &state)
{
  can_packet_t *packet = Network_Receive_NextPacket();

  for (auto _ : state)
    packet->payload->ops->handle_receive(packet->payload);
}
BENCHMARK(DispatchCode);
