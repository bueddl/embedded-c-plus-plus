#include "benchmark.h"

can_packet_t *Network_Receive_NextPacket()
{
  static CurrentEngineDataPayload payload = {
    &OpsForCurrentEngineDataPayload,
    {
      42,
      1337
    }
  };

  static can_packet_t packet = {
    (IPayload*)&payload
  };

  return &packet;
}