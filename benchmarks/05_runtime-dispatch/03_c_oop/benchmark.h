#pragma once

#include <benchmark/benchmark.h>

struct IPayload;

typedef struct can_packet
{
  IPayload *payload;
} can_packet_t;

can_packet_t *Network_Receive_NextPacket();

struct IPayloadOps {
    void (*handle_receive)(struct IPayload*);
};

struct IPayload {
    struct IPayloadOps const *ops;
};

extern const struct IPayloadOps OpsForIPayload;
extern const struct CurrentEngineDataPayloadOps OpsForCurrentEngineDataPayload;
extern const struct AcceleratorPadelPositionPayloadOps OpsForAcceleratorPadelPositionPayload;
extern const struct DoorOpenStatusPayloadOps OpsForDoorOpenStatusPayload;

// ---

struct CurrentEngineDataPayloadData
{
  int rpm;
  short temp;
};

struct CurrentEngineDataPayload;

struct CurrentEngineDataPayloadOps
{
  struct IPayloadOps base;
};

struct CurrentEngineDataPayload
{
  CurrentEngineDataPayloadOps const *ops;
  CurrentEngineDataPayloadData data;
};

// ---

struct AcceleratorPadelPositionPayloadData 
{
  char position;
};

struct AcceleratorPadelPositionPayload;

struct AcceleratorPadelPositionPayloadOps
{
  struct IPayloadOps base;
};

struct AcceleratorPadelPositionPayload
{
  CurrentEngineDataPayloadOps const *ops;
  CurrentEngineDataPayloadData data;
};

// ---

struct DoorOpenStatusPayloadData 
{
  bool closed;
};

struct DoorOpenStatusPayload;

struct DoorOpenStatusPayloadOps
{
  struct IPayloadOps base;
};

struct DoorOpenStatusPayload
{
  CurrentEngineDataPayloadOps const *ops;
  CurrentEngineDataPayloadData data;
};
