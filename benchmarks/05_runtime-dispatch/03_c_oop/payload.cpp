#include "benchmark.h"

void CurrentEngineDataPayload_HandleReceive(struct IPayload *base)
{
  struct CurrentEngineDataPayload *payload = (struct CurrentEngineDataPayload *)base;
  benchmark::DoNotOptimize(payload);
}

const struct CurrentEngineDataPayloadOps OpsForCurrentEngineDataPayload = {
  CurrentEngineDataPayload_HandleReceive};

void AcceleratorPadelPositionPayload_HandleReceive(struct IPayload *base)
{
  struct AcceleratorPadelPositionPayload *payload = (struct AcceleratorPadelPositionPayload *)base;
  benchmark::DoNotOptimize(payload);
}

const struct AcceleratorPadelPositionPayloadOps OpsForAcceleratorPadelPositionPayload = {AcceleratorPadelPositionPayload_HandleReceive};

void DoorOpenStatusPayload_HandleReceive(struct IPayload *base)
{
  struct DoorOpenStatusPayload *payload = (struct DoorOpenStatusPayload *)base;
  benchmark::DoNotOptimize(payload);
}

const struct DoorOpenStatusPayloadOps OpsForDoorOpenStatusPayload = {DoorOpenStatusPayload_HandleReceive};
