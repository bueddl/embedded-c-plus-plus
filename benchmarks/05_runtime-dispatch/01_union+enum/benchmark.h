#pragma once

#include <benchmark/benchmark.h>

typedef enum payload_type
{
  PAYLOAD_CURRENT_ENGINE_DATA,
  PAYLOAD_ACCELERATOR_PADEL_POSITION,
  PAYLOAD_DOOR_OPEN_STATUS,
} payload_type_e;

typedef struct current_engine_data_payload 
{
  int rpm;
  short temp;
} current_engine_data_payload_t;

typedef struct accelerator_padel_position_payload 
{
  char position;
} accelerator_padel_position_payload_t;

typedef struct door_open_status_payload 
{
  bool closed;
} door_open_status_payload_t;

typedef struct packet_payload
{
  payload_type_e type;
  union {
    current_engine_data_payload_t current_engine_data;
    accelerator_padel_position_payload_t accelerator_padel_position;
    door_open_status_payload_t door_open_status;
  } data;
} packet_payload_t;

typedef struct can_packet
{
  // can packet...
  packet_payload_t payload;
} can_packet_t;

can_packet_t* Network_Receive_NextPacket();
