#include "benchmark.h"

BENCHMARK_NEVER_INLINE
void handle_current_engine_data(current_engine_data_payload_t *current_engine_data)
{
  benchmark::DoNotOptimize(current_engine_data);
  benchmark::DoNotOptimize(current_engine_data->rpm);
  benchmark::DoNotOptimize(current_engine_data->temp);
}

BENCHMARK_NEVER_INLINE
void handle_accelerator_padel_position(accelerator_padel_position_payload_t *accelerator_padel_position)
{
  benchmark::DoNotOptimize(accelerator_padel_position);
  benchmark::DoNotOptimize(accelerator_padel_position->position);
}

BENCHMARK_NEVER_INLINE
void handle_door_open_status_payload(door_open_status_payload_t *door_open_status)
{
  benchmark::DoNotOptimize(door_open_status);
  benchmark::DoNotOptimize(door_open_status->closed);
}

void handle_received(packet_payload_t *payload)
{
  switch (payload->type) {
  case PAYLOAD_CURRENT_ENGINE_DATA: {
    current_engine_data_payload_t *payload_data = &payload->data.current_engine_data;
    handle_current_engine_data(payload_data);
    break;
  }

  case PAYLOAD_ACCELERATOR_PADEL_POSITION: {
    accelerator_padel_position_payload_t *payload_data = &payload->data.accelerator_padel_position;
    handle_accelerator_padel_position(payload_data);
    break;
  }

  case PAYLOAD_DOOR_OPEN_STATUS: {
    door_open_status_payload_t *payload_data = &payload->data.door_open_status;
    handle_door_open_status_payload(payload_data);
    break;
  }
  }
}

static void UnionAndEnum(benchmark::State &state)
{
  can_packet_t *packet = Network_Receive_NextPacket();

  for (auto _ : state)
    handle_received(&packet->payload);
}
BENCHMARK(UnionAndEnum);
