#include <benchmark/benchmark.h>

void f();
void g();

class Callback
{
public:
    virtual ~Callback() = default;
    virtual void execute(int some, int args) = 0;
};

void sender(Callback &cb);
