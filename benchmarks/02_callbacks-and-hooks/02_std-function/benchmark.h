#include <benchmark/benchmark.h>

#include <functional>

void f();
void g();

void sender(std::function<void(int, int)> cb);
