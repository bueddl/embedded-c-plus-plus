#include "benchmark.h"

void sender(function<void(int, int)> cb)
{
    f();
    dummy_work();
    g();
}

void large_sender(basic_function<void(int, int), 80> cb)
{
    f();
    dummy_work();
    g();
}

