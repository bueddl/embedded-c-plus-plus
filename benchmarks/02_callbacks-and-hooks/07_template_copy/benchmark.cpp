#include "benchmark.h"

void my_callback(int a, int b)
{
  benchmark::DoNotOptimize(a);
  benchmark::DoNotOptimize(b);
  
}

static void FreeFunction(benchmark::State &state)
{
  for (auto _ : state) {
    sender(my_callback);
  }
}
BENCHMARK(FreeFunction);

// ---

struct Dummy
{
  static void callback(int a, int b)
  {
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(b);
    
  }
};

static void StaticMemberFunction(benchmark::State &state)
{
  for (auto _ : state) {
    sender(Dummy::callback);
  }
}
BENCHMARK(StaticMemberFunction);

// ---

static void StatelessClosure(benchmark::State &state)
{
  for (auto _ : state) {
    sender([] (int a, int b) {
      benchmark::DoNotOptimize(a);
      benchmark::DoNotOptimize(b);
      
    });
  }
}
BENCHMARK(StatelessClosure);

// ---

static void StatefulClosure(benchmark::State &state)
{
  int val = 0;

  for (auto _ : state) {
    sender([&val] (int a, int b) {
      benchmark::DoNotOptimize(val);
      benchmark::DoNotOptimize(a);
      benchmark::DoNotOptimize(b);
      
    });
  }
}
BENCHMARK(StatefulClosure);

// ---

static void StatefulClosureLarge(benchmark::State &state)
{
  char val[80] = {0};

  for (auto _ : state) {
    sender([val] (int a, int b) {
      benchmark::DoNotOptimize(val);
      benchmark::DoNotOptimize(a);
      benchmark::DoNotOptimize(b);
      
    });
  }
}
BENCHMARK(StatefulClosureLarge);

// ---

void my_other_callback(int a, int b, int c)
{
  benchmark::DoNotOptimize(a);
  benchmark::DoNotOptimize(b);
  benchmark::DoNotOptimize(c);
  
}

#if BENCHMARK_CC != CC_ARMCC
#include <functional>

static void BindCallWrapper(benchmark::State &state)
{
  for (auto _ : state) {
    using namespace std::placeholders;
    sender(std::bind(my_other_callback, _1, _2, 42));
  }
}
BENCHMARK(BindCallWrapper);

#endif

// ---

class NoMembersFunctor
{
public:
  void operator()(int a, int b) const noexcept
  {
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(b);
    
  }
};

static void StatelessFunctionObject(benchmark::State &state) 
{
  for (auto _ : state)
    sender(NoMembersFunctor{});
}
BENCHMARK(StatelessFunctionObject);

// ---

class SimpleMembersFunctor
{
public:
  int value;

  void operator()(int a, int b) const noexcept
  {
    benchmark::DoNotOptimize(a);
    benchmark::DoNotOptimize(b);
    benchmark::DoNotOptimize(value);
    
  }
};

static void StatefulFunctionObject(benchmark::State &state) 
{
  int capture;

  for (auto _ : state)
    sender(SimpleMembersFunctor{capture});
}
BENCHMARK(StatefulFunctionObject);
