#include "benchmark.h"

tl::expected<int, error_struct> caller7(bool do_err)
{
  Dtor d;
  tl::expected<int, error_struct> e = callee(do_err);
  if(!e) 
    return e;
  return 1;
}
