#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    tl::expected<int, error_struct> e = caller0(false);
    benchmark::DoNotOptimize(e);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  for (auto _ : state) {
    tl::expected<int, error_struct> e = caller0(true);
    benchmark::DoNotOptimize(e);
  }
}
BENCHMARK(SadPath);
