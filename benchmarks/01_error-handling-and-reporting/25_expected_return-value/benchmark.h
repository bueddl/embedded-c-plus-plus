#pragma once

#include "expected.hpp"
#include <benchmark/benchmark.h>

struct error_struct 
{
  void *error = nullptr;
  void *domain = nullptr;
};

struct Dtor 
{
  ~Dtor();
};


extern int error_info;
extern int error_domain;

tl::expected<int, error_struct> callee(bool do_err);
tl::expected<int, error_struct> caller7(bool do_err);
tl::expected<int, error_struct> caller6(bool do_err);
tl::expected<int, error_struct> caller5(bool do_err);
tl::expected<int, error_struct> caller4(bool do_err);
tl::expected<int, error_struct> caller3(bool do_err);
tl::expected<int, error_struct> caller2(bool do_err);
tl::expected<int, error_struct> caller1(bool do_err);
tl::expected<int, error_struct> caller0(bool do_err);
