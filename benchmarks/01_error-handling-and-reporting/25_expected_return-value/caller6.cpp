#include "benchmark.h"

tl::expected<int, error_struct> caller6(bool do_err)
{
  Dtor d;
  tl::expected<int, error_struct> e = caller7(do_err);
  if(!e) 
    return e;
  return *e+1;
}
