#include "benchmark.h"

int error_info = 1;
int error_domain = 99;

tl::expected<int, error_struct> callee(bool do_err)
{
  error_struct e;
  if(do_err) {
    error_struct e;
    e.error = &error_info;
    e.domain = &error_domain;
    return tl::unexpected<error_struct>{e};
  }
  return {0};
}
