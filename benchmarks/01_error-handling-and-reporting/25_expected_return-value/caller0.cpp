#include "benchmark.h"

tl::expected<int, error_struct> caller0(bool do_err)
{
  Dtor d;
  tl::expected<int, error_struct> e = caller1(do_err);
  if(!e) 
    return e;
  return *e+1;
}
