#include "benchmark.h"

tl::expected<int, error_struct> caller2(bool do_err)
{
  Dtor d;
  tl::expected<int, error_struct> e = caller3(do_err);
  if(!e) 
    return e;
  return *e+1;
}
