#include "benchmark.h"

result<int> caller0(bool do_err)
{
  Dtor d;
  OUTCOME_TRYV(caller1(do_err));
  return outcome::success();
}
