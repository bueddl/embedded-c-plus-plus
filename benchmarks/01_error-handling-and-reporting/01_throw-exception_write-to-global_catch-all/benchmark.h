#pragma once

#include <benchmark/benchmark.h>
#include <exception>

class err_exception : public std::exception 
{
public:
  int val;
  explicit err_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

struct Dtor 
{
  ~Dtor();
};


extern int global_int;

void callee(bool do_err);
void caller7(bool do_err);
void caller6(bool do_err);
void caller5(bool do_err);
void caller4(bool do_err);
void caller3(bool do_err);
void caller2(bool do_err);
void caller1(bool do_err);
void caller0(bool do_err);
