#include "benchmark.h"

int global_int = 0;

static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    try {
      caller0(false);
    } catch (...) {
    }
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  for (auto _ : state) {
    try {
      caller0(true);
    } catch (...) {
    }
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(SadPath);
