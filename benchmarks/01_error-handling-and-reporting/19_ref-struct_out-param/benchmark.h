#pragma once

#include <benchmark/benchmark.h>

struct error_struct 
{
  void *error = nullptr;
  void *domain = nullptr;
};

struct Dtor 
{
  ~Dtor();
};


extern int error_info;


extern int error_domain;

void callee(bool do_err, error_struct &e);
void caller7(int *val, bool do_err, error_struct &e);
void caller6(int *val, bool do_err, error_struct &e);
void caller5(int *val, bool do_err, error_struct &e);
void caller4(int *val, bool do_err, error_struct &e);
void caller3(int *val, bool do_err, error_struct &e);
void caller2(int *val, bool do_err, error_struct &e);
void caller1(int *val, bool do_err, error_struct &e);
void caller0(int *val, bool do_err, error_struct &e);
