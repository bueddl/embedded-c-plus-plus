#include "benchmark.h"

void caller2(int *val, bool do_err, error_struct &e)
{
  Dtor d;
  int out_val = 0;
  caller3(&out_val, do_err, e);
  if(e.error) 
    return;
  *val = out_val;
}
