#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
  error_struct e;
  int val = 0;
  for (auto _ : state) {
    caller0(&val, false, e);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  error_struct e;
  int val = 0;
  for (auto _ : state) {
    caller0(&val, true, e);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
