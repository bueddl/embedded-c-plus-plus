#pragma once

#include <benchmark/benchmark.h>

struct Dtor 
{
  ~Dtor();
};

int callee(bool do_err);
int caller7(int *val, bool do_err);
int caller6(int *val, bool do_err);
int caller5(int *val, bool do_err);
int caller4(int *val, bool do_err);
int caller3(int *val, bool do_err);
int caller2(int *val, bool do_err);
int caller1(int *val, bool do_err);
int caller0(int *val, bool do_err);
