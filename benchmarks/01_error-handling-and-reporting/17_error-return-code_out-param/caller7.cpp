#include "benchmark.h"

int caller7(int *val, bool do_err)
{
  Dtor d;
  int e = callee(do_err);
  if(e) 
    return e;
  *val = 1;
  return 0;
}
