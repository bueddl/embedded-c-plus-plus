#include "benchmark.h"

error_struct caller3(bool do_err, int *val)
{
  Dtor d;
  int out_val = 0;
  error_struct e = caller4(do_err, &out_val);
  if(e.error) 
    return e;
  *val = out_val;
  return e;
}
