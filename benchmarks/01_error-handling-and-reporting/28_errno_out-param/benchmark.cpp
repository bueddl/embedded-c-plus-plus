#include "benchmark.h"


int errno = 0;

static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    int val = 0;
    caller0(&val, false);
    benchmark::DoNotOptimize(errno);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  for (auto _ : state) {
    int val = 0;
    caller0(&val, true);
    benchmark::DoNotOptimize(errno);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
