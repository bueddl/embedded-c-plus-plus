#include "benchmark.h"

void caller7(int *val, bool do_err)
{
  Dtor d;
  callee(do_err);
  if(errno) 
    return;
  *val = 1;
}
