#pragma once

#include <benchmark/benchmark.h>

struct Dtor 
{
  ~Dtor();
};


extern int errno;

void callee(bool do_err);
void caller7(int *val, bool do_err);
void caller6(int *val, bool do_err);
void caller5(int *val, bool do_err);
void caller4(int *val, bool do_err);
void caller3(int *val, bool do_err);
void caller2(int *val, bool do_err);
void caller1(int *val, bool do_err);
void caller0(int *val, bool do_err);
