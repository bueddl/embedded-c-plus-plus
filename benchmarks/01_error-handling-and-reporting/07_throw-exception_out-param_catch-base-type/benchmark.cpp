#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
  int val = 0;
  for (auto _ : state) {
    try {
      caller0(&val, false);
    } catch (std::exception const& e) {
    }
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  int val = 0;
  for (auto _ : state) {
    try {
      caller0(&val, true);
    } catch (std::exception const& e) {
      benchmark::DoNotOptimize(e);
    }
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
