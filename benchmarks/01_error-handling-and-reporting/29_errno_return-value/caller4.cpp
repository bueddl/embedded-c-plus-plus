#include "benchmark.h"

int caller4(bool do_err)
{
  Dtor d;
  int val = caller5(do_err);
  if(errno) 
    return 0;
  return val;
}
