#include "benchmark.h"

int caller0(bool do_err)
{
  Dtor d;
  int val = caller1(do_err);
  if(errno) 
    return 0;
  return val;
}
