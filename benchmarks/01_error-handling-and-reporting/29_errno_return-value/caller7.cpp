#include "benchmark.h"

int caller7(bool do_err)
{
  Dtor d;
  callee(do_err);
  if(errno) 
    return 0;
  return 1;
}
