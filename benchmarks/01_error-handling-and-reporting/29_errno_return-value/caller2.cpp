#include "benchmark.h"

int caller2(bool do_err)
{
  Dtor d;
  int val = caller3(do_err);
  if(errno) 
    return 0;
  return val;
}
