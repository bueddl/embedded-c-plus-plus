#include "benchmark.h"


static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    (void)caller0(false);
  }
}
BENCHMARK(HappyPath);
