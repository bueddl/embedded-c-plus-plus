#pragma once

#include <benchmark/benchmark.h>
#include <exception>

class err0_exception : public std::exception 
{
public:
  int val;
  explicit err0_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err1_exception : public std::exception 
{
public:
  int val;
  explicit err1_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err2_exception : public std::exception 
{
public:
  int val;
  explicit err2_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err3_exception : public std::exception 
{
public:
  int val;
  explicit err3_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err4_exception : public std::exception 
{
public:
  int val;
  explicit err4_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err5_exception : public std::exception 
{
public:
  int val;
  explicit err5_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err6_exception : public std::exception 
{
public:
  int val;
  explicit err6_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

class err7_exception : public std::exception 
{
public:
  int val;
  explicit err7_exception(int e) : val(e) {}
  const char *what() const noexcept override { return ""; }
};

struct Dtor 
{
  ~Dtor();
};


extern int global_int;

void callee(bool do_err);
void caller7(bool do_err);
void caller6(bool do_err);
void caller5(bool do_err);
void caller4(bool do_err);
void caller3(bool do_err);
void caller2(bool do_err);
void caller1(bool do_err);
void caller0(bool do_err);
