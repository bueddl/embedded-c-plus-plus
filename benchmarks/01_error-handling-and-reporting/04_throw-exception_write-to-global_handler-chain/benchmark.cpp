#include "benchmark.h"

int global_int = 0;

static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    try {
      caller0(false);
    } catch (err0_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err1_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err2_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err3_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err4_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err5_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err6_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err7_exception &e) {
      benchmark::DoNotOptimize(e);
    }
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  for (auto _ : state) {
    try {
      caller0(true);
    } catch (err0_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err1_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err2_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err3_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err4_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err5_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err6_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err7_exception &e) {
      benchmark::DoNotOptimize(e);
    }
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(SadPath);
