#include "benchmark.h"

int global_int = 0;

static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    try {
      caller0(false);
    } catch (std::exception const& e) {
    }
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  for (auto _ : state) {
    try {
      caller0(true);
    } catch (std::exception const& e) {
      benchmark::DoNotOptimize(e);
    }
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(SadPath);
