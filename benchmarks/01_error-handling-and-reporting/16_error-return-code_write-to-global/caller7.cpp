#include "benchmark.h"

int caller7(bool do_err)
{
  Dtor d;
  int e = callee(do_err);
  if(e) 
    return e;
  global_int = 0;
  return 0;
}
