#include "benchmark.h"

int caller0(bool do_err)
{
  Dtor d;
  int e = caller1(do_err);
  if(e) 
    return e;
  global_int = 0;
  return 0;
}
