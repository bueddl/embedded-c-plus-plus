#include "benchmark.h"


static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    try {
      int val = caller0(false);
      benchmark::DoNotOptimize(val);
    } catch (std::exception const& e) {
    }
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  for (auto _ : state) {
    try {
      int val = caller0(true);
      benchmark::DoNotOptimize(val);
    } catch (std::exception const& e) {
      benchmark::DoNotOptimize(e);
    }
  }
}
BENCHMARK(SadPath);
