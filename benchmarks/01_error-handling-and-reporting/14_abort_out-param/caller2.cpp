#include "benchmark.h"

void caller2(int *val, bool do_err)
{
  Dtor d;
  int out_val = 0;
  caller3(&out_val, do_err);
  *val = out_val;
}
