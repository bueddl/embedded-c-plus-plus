#include "benchmark.h"

std::experimental::optional<int> caller6(bool do_err)
{
  Dtor d;
  std::experimental::optional<int> r = caller7(do_err);
  if (!r)
    return {};
  return *r+1;
}
