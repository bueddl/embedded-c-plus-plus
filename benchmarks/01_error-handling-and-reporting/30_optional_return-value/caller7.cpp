#include "benchmark.h"

std::experimental::optional<int> caller7(bool do_err)
{
  Dtor d;
  std::experimental::optional<int> r = callee(do_err);
  if (!r)
    return {};
  return *r+1;
}
