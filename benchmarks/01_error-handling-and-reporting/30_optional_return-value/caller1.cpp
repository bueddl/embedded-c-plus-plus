#include "benchmark.h"

std::experimental::optional<int> caller1(bool do_err)
{
  Dtor d;
  std::experimental::optional<int> r = caller2(do_err);
  if (!r)
    return {};
  return *r+1;
}
