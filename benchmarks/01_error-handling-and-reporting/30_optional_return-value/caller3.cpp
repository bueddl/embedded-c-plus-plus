#include "benchmark.h"

std::experimental::optional<int> caller3(bool do_err)
{
  Dtor d;
  std::experimental::optional<int> r = caller4(do_err);
  if (!r)
    return {};
  return *r+1;
}
