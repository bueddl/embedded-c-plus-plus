#include "benchmark.h"


std::experimental::optional<int> callee(bool do_err)
{
  if(do_err)
    return {};
  return 1;
}
