#include "benchmark.h"

std::experimental::optional<int> caller5(bool do_err)
{
  Dtor d;
  std::experimental::optional<int> r = caller6(do_err);
  if (!r)
    return {};
  return *r+1;
}
