#include "benchmark.h"

error_struct caller7(bool do_err)
{
  Dtor d;
  error_struct e = callee(do_err);
  if(e.error) 
    return e;
  global_int = 0;
  return e;
}
