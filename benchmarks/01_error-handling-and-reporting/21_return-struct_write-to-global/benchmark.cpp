#include "benchmark.h"

int global_int = 0;

static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    error_struct e = caller0(false);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  for (auto _ : state) {
    error_struct e = caller0(true);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(global_int);
  }
}
BENCHMARK(SadPath);
