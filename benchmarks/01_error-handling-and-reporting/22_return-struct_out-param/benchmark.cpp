#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    int val = 0;
    error_struct e = caller0(false, &val);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  for (auto _ : state) {
    int val = 0;
    error_struct e = caller0(true, &val);
    benchmark::DoNotOptimize(e);
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
