#pragma once

#include <benchmark/benchmark.h>

struct error_struct 
{
  void *error = nullptr;
  void *domain = nullptr;
};

struct Dtor 
{
  ~Dtor();
};


extern int global_int;


extern int error_info;


extern int error_domain;

void callee(bool do_err, error_struct &e);
void caller7(bool do_err, error_struct &e);
void caller6(bool do_err, error_struct &e);
void caller5(bool do_err, error_struct &e);
void caller4(bool do_err, error_struct &e);
void caller3(bool do_err, error_struct &e);
void caller2(bool do_err, error_struct &e);
void caller1(bool do_err, error_struct &e);
void caller0(bool do_err, error_struct &e);
