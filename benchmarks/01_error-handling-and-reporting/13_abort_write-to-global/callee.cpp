#include "benchmark.h"

void HAL_NVIC_SystemReset();

void abort()
{
	HAL_NVIC_SystemReset();
}

void callee(bool do_err)
{
  if(do_err)
    abort();
}
