#include <exception>
#include "benchmark.h"

static void HappyPath(benchmark::State &state)
{
  int val = 0;
  for (auto _ : state) {
    try {
      caller0(&val, false);
    } catch (err0_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err1_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err2_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err3_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err4_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err5_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err6_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err7_exception &e) {
      benchmark::DoNotOptimize(e);
    }
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  int val = 0;
  for (auto _ : state) {
    try {
      caller0(&val, true);
    } catch (err0_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err1_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err2_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err3_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err4_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err5_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err6_exception &e) {
      benchmark::DoNotOptimize(e);
    } catch (err7_exception &e) {
      benchmark::DoNotOptimize(e);
    }
    benchmark::DoNotOptimize(val);
  }
}
BENCHMARK(SadPath);
