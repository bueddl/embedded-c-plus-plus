#include "benchmark.h"

jmp_buf buf;

static void HappyPath(benchmark::State &state)
{
  for (auto _ : state) {
    int ret = setjmp(buf);
    if (ret == 0) {
      int val = 0;
      caller0(&val, false);
      benchmark::DoNotOptimize(val);
    } else {
      benchmark::DoNotOptimize(ret);
    }
  }
}
BENCHMARK(HappyPath);

static void SadPath(benchmark::State &state)
{
  for (auto _ : state) {
    int ret = setjmp(buf);
    if (ret == 0) {
      int val = 0;
      caller0(&val, true);
      benchmark::DoNotOptimize(val);
    } else {
      benchmark::DoNotOptimize(ret);
    }
  }
}
BENCHMARK(SadPath);
