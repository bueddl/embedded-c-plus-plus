#pragma once

#include <benchmark/benchmark.h>

#include <stdint.h>

// write int as LE
class BinaryWriter
{
public:
  template<unsigned int Size>
  BinaryWriter(char (&buf)[Size])
    : buf_{buf},
      size_{Size}
  {}

  void WriteUint8(uint8_t value)
  {
    if (!ensure_capacity(1))
      return;

    buf_[pos_++] = value;
  }

  void WriteInt8(int8_t value)
  {
    if (!ensure_capacity(1))
      return;

    buf_[pos_++] = value;
  }

  void WriteUint16(uint16_t value)
  {
    if (!ensure_capacity(2))
      return;

    buf_[pos_++] = value & 0xff;
    buf_[pos_++] = (value >> 8) & 0xff;
  }

  void WriteInt16(int16_t value)
  {
    if (!ensure_capacity(2))
      return;

    buf_[pos_++] = value & 0xff;
    buf_[pos_++] = (value >> 8) & 0xff;
  }

  void WriteUint32(uint32_t value)
  {
    if (!ensure_capacity(4))
      return;

    buf_[pos_++] = value & 0xff;
    buf_[pos_++] = (value >> 8) & 0xff;
    buf_[pos_++] = (value >> 16) & 0xff;
    buf_[pos_++] = (value >> 24) & 0xff;
  }

  void WriteInt32(int32_t value)
  {
    if (!ensure_capacity(4))
      return;

    buf_[pos_++] = value & 0xff;
    buf_[pos_++] = (value >> 8) & 0xff;
    buf_[pos_++] = (value >> 16) & 0xff;
    buf_[pos_++] = (value >> 24) & 0xff;
  }

private:
  char *buf_;
  unsigned int size_;
  unsigned int pos_ = 0;
  bool bad_flag_ = false;

  bool ensure_capacity(unsigned int required_size)
  {
    return bad_flag_ = (required_size + pos_ < size_);
  }
};

class Serializable
{
public:
  virtual void serialize(BinaryWriter &writer) const = 0;
};

class CAN_Payload : public Serializable
{

};

class CAN_Payload_CurrentEngineData final : public CAN_Payload
{
public:
  void serialize(BinaryWriter &writer) const override
  {
    writer.WriteInt32(rpm);
    writer.WriteInt16(temp);
  }

private:
  int rpm;
  short temp;
};

class CAN_Payload_AcceleratorPadelPosition final : public CAN_Payload
{
public:
  void serialize(BinaryWriter &writer) const override
  {
    writer.WriteInt8(position);
  }

private:
  char position;
};

class CAN_Payload_DoorOpenStatus final : public CAN_Payload
{
public:
  void serialize(BinaryWriter &writer) const override
  {
    writer.WriteUint8(closed);
  }

private:
  bool closed;
};
