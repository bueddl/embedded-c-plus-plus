#include "benchmark.h"

void Serialize_CurrentEngineData(void *buf, struct CAN_Payload_CurrentEngineData *payload)
{
  memcpy(buf, payload, sizeof(*payload));
}

void Serialize_AcceleratorPadelPosition(void *buf, struct CAN_Payload_AcceleratorPadelPosition *payload)
{
  memcpy(buf, payload, sizeof(*payload));
}

void Serialize_DoorOpenStatus(void *buf, struct CAN_Payload_DoorOpenStatus *payload)
{
  memcpy(buf, payload, sizeof(*payload));
}
