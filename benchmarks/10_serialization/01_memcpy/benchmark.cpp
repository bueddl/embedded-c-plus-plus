#include "benchmark.h"

static void Serialize(benchmark::State &state)
{
  CAN_Payload_CurrentEngineData payload = {};

  for (auto _ : state) {
    char buf[8];
    Serialize_CurrentEngineData(buf, &payload);
    benchmark::DoNotOptimize(buf);
  }
}
BENCHMARK(Serialize);
