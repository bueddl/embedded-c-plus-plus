#pragma once

#include <benchmark/benchmark.h>
#include <string.h>

struct CAN_Payload_CurrentEngineData
{
  int rpm;
  short temp;
};

struct CAN_Payload_AcceleratorPadelPosition
{
  char position;
};

struct CAN_Payload_DoorOpenStatus
{
  bool closed;
};

void Serialize_CurrentEngineData(void *buf, struct CAN_Payload_CurrentEngineData *payload);
void Serialize_AcceleratorPadelPosition(void *buf, struct CAN_Payload_AcceleratorPadelPosition *payload);
void Serialize_DoorOpenStatus(void *buf, struct CAN_Payload_DoorOpenStatus *payload);
