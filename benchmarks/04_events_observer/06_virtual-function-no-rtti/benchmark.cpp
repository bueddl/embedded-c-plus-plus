#include "benchmark.h"

class my_callback : public callback
{
public:
    void execute(int some, int args) override
    {
        dummy_work();
        benchmark::DoNotOptimize(some);
        benchmark::DoNotOptimize(args);
        benchmark::DoNotOptimize(this);
    }
};

static void InstallCallback(benchmark::State &state)
{
  state.timer.pause();

  for (auto _ : state) {
        callback_storage storage;

    state.timer.resume();
    
    my_callback my_cb;
      storage.install(my_cb);

    state.timer.pause();
    benchmark::DoNotOptimize(storage);
  }

  state.timer.resume();
}
BENCHMARK(InstallCallback);


static void ExecuteCallback(benchmark::State &state)
{
  state.timer.pause();

    callback_storage storage;
  my_callback my_cb;
  storage.install(my_cb);

  benchmark::DoNotOptimize(storage);

  state.timer.resume();

  for (auto _ : state)
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallback);