#include "benchmark.h"

// ---

static void InstallCallbackStatefulClosureLarge(benchmark::State &state)
{
  state.timer.pause();

  void *user;
  char value[76] = {0};

  for (auto _ : state) {
    callback_storage storage = {};

    state.timer.resume();
    
    storage.install([user, value](int some, int args) {
      benchmark::DoNotOptimize(some);
      benchmark::DoNotOptimize(args);
      benchmark::DoNotOptimize(user);
      benchmark::DoNotOptimize(value);
    });

    state.timer.pause();
    benchmark::DoNotOptimize(storage);
  }

  state.timer.resume();
}
BENCHMARK(InstallCallbackStatefulClosureLarge);


static void ExecuteCallbackStatefulClosureLarge(benchmark::State &state)
{
  state.timer.pause();

  void *user;
  char value[76] = {0};

  callback_storage storage = {};
  storage.install([user, value](int some, int args) {
    benchmark::DoNotOptimize(some);
    benchmark::DoNotOptimize(args);
    benchmark::DoNotOptimize(user);
    benchmark::DoNotOptimize(value);
  });

  benchmark::DoNotOptimize(storage);

  state.timer.resume();

  for (auto _ : state)
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallbackStatefulClosureLarge);

// ---
