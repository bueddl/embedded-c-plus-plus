#include "benchmark.h"

int callback_install(callback_storage_t *storage, callback_fn function, void *user)
{
    int i;

    for (i = 0; storage->slots[i].in_use; ++i)
        if (i == MAX_CALLBACKS)
            return -1;
    
    storage->slots[i].function = function;
    storage->slots[i].user = user;
    storage->slots[i].in_use = 1;
    return 0;
}

void callback_execute(callback_storage_t *storage, int some, int args)
{
    for (int i = 0; i < MAX_CALLBACKS; ++i) {
        if (!storage->slots[i].in_use)
            continue;
        
        storage->slots[i].function(
            some, 
            args,
            storage->slots[i].user);
    }
}
