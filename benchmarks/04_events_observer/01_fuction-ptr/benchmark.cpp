#include "benchmark.h"

void my_callback(int some, int args, void *user)
{
    benchmark::DoNotOptimize(some);
    benchmark::DoNotOptimize(args);
    benchmark::DoNotOptimize(user);
}

static void InstallCallbackFreeFunction(benchmark::State &state)
{
  state.timer.pause();

  for (auto _ : state) {
    callback_storage storage = {};

    state.timer.resume();
    
    callback_install(&storage, my_callback, 0);

    state.timer.pause();
    benchmark::DoNotOptimize(storage);
  }

  state.timer.resume();
}
BENCHMARK(InstallCallbackFreeFunction);


static void ExecuteCallbackFreeFunction(benchmark::State &state)
{
  state.timer.pause();

  callback_storage storage = {};
  callback_install(&storage, my_callback, 0);

  benchmark::DoNotOptimize(storage);

  state.timer.resume();

  for (auto _ : state)
    callback_execute(&storage, 0, 1);
}
BENCHMARK(ExecuteCallbackFreeFunction);

// ---

class Dummy
{
public:
  static void callback(int some, int args, void *user)
  {
      benchmark::DoNotOptimize(some);
      benchmark::DoNotOptimize(args);
      benchmark::DoNotOptimize(user);
  }
};

static void InstallCallbackStaticMemberFunction(benchmark::State &state)
{
  state.timer.pause();

  for (auto _ : state) {
    callback_storage storage = {};

    state.timer.resume();
    
    callback_install(&storage, Dummy::callback, 0);

    state.timer.pause();
    benchmark::DoNotOptimize(storage);
  }

  state.timer.resume();
}
BENCHMARK(InstallCallbackStaticMemberFunction);


static void ExecuteCallbackStaticMemberFunction(benchmark::State &state)
{
  state.timer.pause();

  callback_storage storage = {};
  callback_install(&storage, my_callback, 0);

  benchmark::DoNotOptimize(storage);

  state.timer.resume();

  for (auto _ : state)
    callback_execute(&storage, 0, 1);
}
BENCHMARK(ExecuteCallbackStaticMemberFunction);

// ---

static void InstallCallbackStatelessClosure(benchmark::State &state)
{
  state.timer.pause();

  for (auto _ : state) {
    callback_storage storage = {};

    state.timer.resume();
    
    callback_install(&storage, [](int some, int args, void *user) {
        benchmark::DoNotOptimize(some);
        benchmark::DoNotOptimize(args);
        benchmark::DoNotOptimize(user);
    }, 0);

    state.timer.pause();
    benchmark::DoNotOptimize(storage);
  }

  state.timer.resume();
}
BENCHMARK(InstallCallbackStatelessClosure);


static void ExecuteCallbackStatelessClosure(benchmark::State &state)
{
  state.timer.pause();

  callback_storage storage = {};
  callback_install(&storage, my_callback, 0);

  benchmark::DoNotOptimize(storage);

  state.timer.resume();

  for (auto _ : state)
    callback_execute(&storage, 0, 1);
}
BENCHMARK(ExecuteCallbackStatelessClosure);

// ---
