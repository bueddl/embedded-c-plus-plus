#include "benchmark.h"

// ---

static void InstallCallbackStatefulClosure(benchmark::State &state)
{
  state.timer.pause();

  void *user;

  for (auto _ : state) {
    callback_storage storage = {};

    state.timer.resume();
    
    storage.install([user](int some, int args) {
        benchmark::DoNotOptimize(some);
        benchmark::DoNotOptimize(args);
        benchmark::DoNotOptimize(user);
    });

    state.timer.pause();
    benchmark::DoNotOptimize(storage);
  }

  state.timer.resume();
}
BENCHMARK(InstallCallbackStatefulClosure);


static void ExecuteCallbackStatefulClosure(benchmark::State &state)
{
  state.timer.pause();

  void *user;

  callback_storage storage = {};
  storage.install([user](int some, int args) {
      benchmark::DoNotOptimize(some);
      benchmark::DoNotOptimize(args);
      benchmark::DoNotOptimize(user);
  });

  benchmark::DoNotOptimize(storage);

  state.timer.resume();

  for (auto _ : state)
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallbackStatefulClosure);

// ---

#if BENCHMARK_CC != CC_ARMCC

#include <functional>

void bind_callback(int some, int args, void *user) 
{
  benchmark::DoNotOptimize(some);
  benchmark::DoNotOptimize(args);
  benchmark::DoNotOptimize(user);
}

static void InstallCallbackBindCallWrapper(benchmark::State &state)
{
  state.timer.pause();

  void *user;

  for (auto _ : state) {
    callback_storage storage = {};

    state.timer.resume();

    using namespace std::placeholders;
    
    storage.install(std::bind(bind_callback, _1, _2, nullptr));

    state.timer.pause();
    benchmark::DoNotOptimize(storage);
  }

  state.timer.resume();
}
BENCHMARK(InstallCallbackBindCallWrapper);


static void ExecuteCallbackBindCallWrapper(benchmark::State &state)
{
  state.timer.pause();

  callback_storage storage = {};
  using namespace std::placeholders;  
  storage.install(std::bind(bind_callback, _1, _2, nullptr));

  benchmark::DoNotOptimize(storage);

  state.timer.resume();

  for (auto _ : state)
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallbackBindCallWrapper);

#endif

// ---

class Callback
{
public:
  int state;

  void operator()(int some, int args) const noexcept
  {
    benchmark::DoNotOptimize(some);
    benchmark::DoNotOptimize(args);
    benchmark::DoNotOptimize(state);
    benchmark::DoNotOptimize(this);
  }
};

static void InstallCallbackFunctionObject(benchmark::State &state)
{
  state.timer.pause();

  void *user;

  for (auto _ : state) {
    callback_storage storage = {};

    state.timer.resume();
    
    storage.install(Callback{42});

    state.timer.pause();
    benchmark::DoNotOptimize(storage);
  }

  state.timer.resume();
}
BENCHMARK(InstallCallbackFunctionObject);


static void ExecuteCallbackFunctionObject(benchmark::State &state)
{
  state.timer.pause();

  callback_storage storage = {};
  storage.install(Callback{42});

  benchmark::DoNotOptimize(storage);

  state.timer.resume();

  for (auto _ : state)
    storage.execute(0, 1);
}
BENCHMARK(ExecuteCallbackFunctionObject);

// ---
