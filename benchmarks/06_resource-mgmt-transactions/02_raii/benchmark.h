#pragma once

#include <benchmark/benchmark.h>

struct Component0
{
    int val;

    explicit Component0(bool do_err);

    explicit operator bool() const noexcept
    { return val != -1; }

    ~Component0();
};

struct Component1
{
    int val;

    explicit Component1(bool do_err);

    explicit operator bool() const noexcept
    { return val != -1; }

    ~Component1();
};

struct Component2
{
    int val;

    explicit Component2(bool do_err);

    explicit operator bool() const noexcept
    { return val != -1; }

    ~Component2();
};

struct Component3
{
    int val;

    explicit Component3(bool do_err);

    explicit operator bool() const noexcept
    { return val != -1; }

    ~Component3();
};
