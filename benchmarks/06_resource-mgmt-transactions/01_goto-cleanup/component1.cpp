#include "benchmark.h"

int init_component1(bool do_err, int *c)
{
  if (do_err)
    return -1;
  *c = 1;
  return 0;
}

void cleanup_component1(int c)
{
  benchmark::DoNotOptimize(c);
}
