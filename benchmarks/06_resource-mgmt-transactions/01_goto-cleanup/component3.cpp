#include "benchmark.h"

int init_component3(bool do_err, int *c)
{
  if (do_err)
    return -1;
  *c = 3;
  return 0;
}

void cleanup_component3(int c)
{
  benchmark::DoNotOptimize(c);
}
