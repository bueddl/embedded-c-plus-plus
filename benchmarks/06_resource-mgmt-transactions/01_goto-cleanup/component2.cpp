#include "benchmark.h"

int init_component2(bool do_err, int *c)
{
  if (do_err)
    return -1;
  *c = 2;
  return 0;
}

void cleanup_component2(int c)
{
  benchmark::DoNotOptimize(c);
}
