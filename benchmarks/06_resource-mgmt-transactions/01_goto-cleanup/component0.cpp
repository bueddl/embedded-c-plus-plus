#include "benchmark.h"

int init_component0(bool do_err, int *c)
{
  if (do_err)
    return -1;
  *c = 0;
  return 0;
}

void cleanup_component0(int c)
{
  benchmark::DoNotOptimize(c);
}
