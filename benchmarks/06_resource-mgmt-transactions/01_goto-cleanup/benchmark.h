#pragma once

#include <benchmark/benchmark.h>

int init_component0(bool do_err, int *c);
void cleanup_component0(int c);

int init_component1(bool do_err, int *c);
void cleanup_component1(int c);

int init_component2(bool do_err, int *c);
void cleanup_component2(int c);

int init_component3(bool do_err, int *c);
void cleanup_component3(int c);
