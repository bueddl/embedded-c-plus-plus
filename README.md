## Building

### GNU

```bash
cmake \
  -DCMAKE_C_COMPILER=/c/Program\ Files\ \(x86\)/GNU\ Tools\ Arm\ Embedded/9\ 2019-q4-major/bin/arm-none-eabi-gcc.exe \
  -DCMAKE_CXX_COMPILER=/c/Program\ Files\ \(x86\)/GNU\ Tools\ Arm\ Embedded/9\ 2019-q4-major/bin/arm-none-eabi-g++.exe \
  -DCMAKE_C_COMPILER_WORKS=1 \
  -DCMAKE_CXX_COMPILER_WORKS=1 \
  -DCMAKE_SYSTEM_NAME=Generic \
  ..
cmake --build .
```

### Clang

```bash
cmake \
  -DCMAKE_C_COMPILER=/c/msys64/mingw64/bin/clang.exe \
  -DCMAKE_C_COMPILER_ID=Clang \
  -DCMAKE_CXX_COMPILER=/c/msys64/mingw64/bin/clang++.exe \
  -DCMAKE_CXX_COMPILER_ID=Clang \
  -DCMAKE_C_COMPILER_WORKS=1 \
  -DCMAKE_CXX_COMPILER_WORKS=1 \
  -DCMAKE_SYSTEM_NAME=Generic \
  -DCMAKE_SYSTEM_PROCESSOR=cortex-m4 \
  ..
cmake --build .
```

### IAR

```bash
cmake \
  -DCMAKE_C_COMPILER=/c/Program\ Files\ \(x86\)/IAR\ Systems/Embedded\ Workbench\ 8.3/arm/bin/iccarm.exe \
  -DCMAKE_CXX_COMPILER=/c/Program\ Files\ \(x86\)/IAR\ Systems/Embedded\ Workbench\ 8.3/arm/bin/iccarm.exe \
  -DCMAKE_ASM_COMPILER=/c/Program\ Files\ \(x86\)/IAR\ Systems/Embedded\ Workbench\ 8.3/arm/bin/iasmarm.exe \
  -DCMAKE_C_COMPILER_WORKS=1 \
  -DCMAKE_CXX_COMPILER_WORKS=1 \
  -DCMAKE_SYSTEM_NAME=Generic \
  ..
cmake --build .
```

### ARMCC

```bash
cmake \
  -DCMAKE_C_COMPILER=/c/Keil_v5/ARM/ARMCC/Bin/ArmCC.exe \
  -DCMAKE_C_COMPILER_ID=ARMCC \
  -DCMAKE_CXX_COMPILER=/c/Keil_v5/ARM/ARMCC/Bin/ArmCC.exe \
  -DCMAKE_CXX_COMPILER_ID=ARMCC \
  -DCMAKE_ASM_COMPILER=/c/Keil_v5/ARM/ARMCC/Bin/ArmAsm.exe \
  -DCMAKE_ASM_COMPILER_ID=ARMCC \
  -DCMAKE_C_COMPILER_WORKS=1 \
  -DCMAKE_CXX_COMPILER_WORKS=1 \
  -DCMAKE_SYSTEM_NAME=Generic \
  ..
cmake --build .
```

### ARMClang

```bash
cmake \
  -DCMAKE_C_COMPILER=/c/Keil_v5/ARM/ARMCLANG/Bin/ArmClang.exe \
  -DCMAKE_CXX_COMPILER=/c/Keil_v5/ARM/ARMCLANG/Bin/ArmClang.exe \
  -DCMAKE_ASM_COMPILER=/c/Keil_v5/ARM/ARMCLANG/Bin/ArmAsm.exe \
  -DCMAKE_C_COMPILER_WORKS=1 \
  -DCMAKE_CXX_COMPILER_WORKS=1 \
  -DCMAKE_SYSTEM_NAME=Generic \
  -DCMAKE_SYSTEM_PROCESSOR=cortex-m4 \
  ..
cmake --build .
```

## Flashing

### STM32L0xx

```bash
SERIAL=066AFF494849887767173322 \
openocd \
	-c "tcl_port disabled" \
	-s /usr/share/openocd/scripts \
	-f ../flash/openocd/STM32L0xx/openocd.cfg \
	-c "program template.elf" \
	-c "init; reset; init;" \
	-c "exit"
```

### STM32L4xx

```bash
SERIAL=066AFF545051717867162827 \
openocd \
	-c "tcl_port disabled" \
	-s /usr/share/openocd/scripts \
	-f ../flash/openocd/STM32L4xx/openocd.cfg \
	-c "program template.elf" \
	-c "init; reset; init;" \
	-c "exit"
```

### STM32F0xx

```bash
SERIAL=066BFF323535474B43134813 \
openocd \
	-c "tcl_port disabled" \
	-s /usr/share/openocd/scripts \
	-f ../flash/openocd/STM32F0xx/openocd.cfg \
	-c "program template.elf" \
	-c "init; reset; init;" \
	-c "exit"
```

### STM32F2xx

```bash
SERIAL=0670FF495051717867114533 \
openocd \
	-c "tcl_port disabled" \
	-s /usr/share/openocd/scripts \
	-f ../flash/openocd/STM32F2xx/openocd.cfg \
	-c "program template.elf" \
	-c "init; reset; init;" \
	-c "exit"
```

### STM32F4xx

```bash
SERIAL=0676FF535156827867092859 \
openocd \
	-c "tcl_port disabled" \
	-s /usr/share/openocd/scripts \
	-f ../flash/openocd/STM32F4xx/openocd.cfg \
	-c "program template.elf" \
	-c "init; reset; init;" \
	-c "exit"
```

### STM32F7xx

```bash
SERIAL=066EFF515455777867052307 \
openocd \
	-c "tcl_port disabled" \
	-s /usr/share/openocd/scripts \
	-f ../flash/openocd/STM32F7xx/openocd.cfg \
	-c "program template.elf" \
	-c "init; reset; init;" \
	-c "exit"
```

### STM32L4P5

```bash
SERIAL=0669FF343039564157202134 \
openocd \
	-c "tcl_port disabled" \
	-s /usr/share/openocd/scripts \
	-f ../flash/openocd/STM32L4xx/openocd.cfg \
	-c "program template.elf" \
	-c "init; reset; init;" \
	-c "exit"
```

### STM32F303

```bash
SERIAL=066CFF575251717867161446 \
openocd \
	-c "tcl_port disabled" \
	-s /usr/share/openocd/scripts \
	-f ../flash/openocd/STM32F3xx/openocd.cfg \
	-c "program template.elf" \
	-c "init; reset; init;" \
	-c "exit"
```

### STM32F746

```bash
SERIAL=0668FF343039564157243725 \
openocd \
	-c "tcl_port disabled" \
	-s /usr/share/openocd/scripts \
	-f ../flash/openocd/STM32F7xx/openocd.cfg \
	-c "program template.elf" \
	-c "init; reset; init;" \
	-c "exit"
```