import yaml
import re
from pathlib import Path
from enum import Enum
import io
from itertools import groupby
from operator import itemgetter
from functools import reduce
import subprocess


class Benchmark:
  def __init__(self, iterations):
    self.iterations = iterations
    self.stack_usage = None

class BenchmarkReport:
  def __init__(self, metadata, benchmarks):
    self.metadata = metadata
    self.benchmarks = benchmarks

class State(Enum):
  INVALID = 0
  INIT = 1
  IN_METADATA = 2
  BEGIN_OF_BENCHMARK = 3
  END_OF_BENCHMARK = 4
  IN_BENCHMARK = 5
  END = 6

class BenchmarkParser:
  def __init__(self):
    self.__state = State.INVALID
    self.filename = 'stdin'

  def parse(self, text_io, filename = 'stdin'):
    self.__state = State.INIT
    self.__filename = filename

    metadata = {}
    benchmarks = {}

    for line_no, line in enumerate(text_io.read().split('\n')):
      if len(line.strip()) == 0:
        continue

      if self.__state == State.INIT:
        if '***** BENCHMARK INFO ******' in line:
          self.__state = State.IN_METADATA
        else:
          self.__parser_error(line_no, line)
      elif self.__state == State.IN_METADATA:
        if '*** BEGIN OF BENCHMARKS ***' in line:
          self.__state = State.BEGIN_OF_BENCHMARK
        else:
          key, value = map(lambda s: s.strip(), line.split(':'))
          metadata[key] = value
      elif self.__state == State.BEGIN_OF_BENCHMARK:
        m = re.match('=== \\((\\d+)/(\\d+)\\) \\[(.*?)\\] ===', line)
        if m:
          self.__state = State.IN_BENCHMARK
          current_benchmark_info = m.groups()
          current_benchmark_data = {}
        elif '**** END OF BENCHMARKS ****' in line:
          self.__state = State.END
      elif self.__state == State.IN_BENCHMARK:
        m = re.match('^\\[ *(\\d+)/ *(\\d+)\\] iterations: *(\\d+), count: *(\\d+) *$', line)
        if m:
          data = list(map(int, m.groups()))
          current_benchmark_data[data[2]] = data[3]
          if data[0] == data[1]:
            benchmarks[current_benchmark_info[2]] = Benchmark(current_benchmark_data)
            self.__state = State.END_OF_BENCHMARK
        else:
          self.__parser_error(line_no, line)
      elif self.__state == State.END_OF_BENCHMARK:
        m = re.match('^### +Stack usage: (\\d+) +###$', line)
        if m:
          benchmarks[current_benchmark_info[2]].stack_usage = int(m.group(1))
          self.__state = State.BEGIN_OF_BENCHMARK
        else:
          self.__parser_error(line_no, line)
      elif self.__state == State.END:
        self.__parser_error(line_no, line)
      else:
        self.__parser_error(line_no, line)

    if self.__state != State.END:
      self.__parser_error(line_no, line)

    return BenchmarkReport(metadata, benchmarks)

  def __parser_error(self, line_no, line_content):
    print('Parser error (state={}) in {}:{}: {}'.format(
    self.__state,
    self.__filename,
    line_no, 
    line_content))


def generate_time_data_tables(config):
  
  def collect_raw_data(config):
    def load_benchmark_report(category, benchmark, group, solution, mode, toolchain, target):
      filename = '{}/{}/{}-{}-{}'.format(
        category['map'],
        solution['map'],
        target['map'],
        toolchain['map'],
        mode['map'])

      p = Path('../tmp/', filename, 'out/benchmark/log/run_0.stdout.log')
      if not p.exists():
        #TODO theoretically possible to infer from test-runner config.yml... 
        #     needed? or is just a simple validity check enough?
        #print('no exist', p)
        return

      with open(p, 'r') as f:
        parser = BenchmarkParser()
        report = parser.parse(f, p)

      bmap = solution['map_benchmark'] if 'map_benchmark' in solution \
        else group['map'] if 'map' in group \
        else benchmark['map']

      if bmap not in report.benchmarks:
        if toolchain['id'] == 'armcc' and (
          bmap == 'BindCallWrapper' or
          bmap == 'InstallCallbackBindCallWrapper' or
          bmap == 'ExecuteCallbackBindCallWrapper'
          ):
          #print('not supported')
          return
        print('ERROR', benchmark['id'], bmap, report)
        return
      return report.benchmarks[bmap]

    return {
      category['id']:{
        benchmark['id']:{
          group['id']:{
            solution['id']:{
              mode['id']:{
                toolchain['id']:{
                  target['id']: report
                  for target in config['targets']
                  for report in [
                    load_benchmark_report(category, benchmark, group, solution, mode, toolchain, target)
                  ]
                  if report is not None
                }
                for toolchain in config['toolchains']
              }
              for mode in config['modes']
            }
            for solution in group['solutions']
          }
          for group in (
            benchmark['groups'] 
            if 'groups' in benchmark \
            else [{'id': benchmark['id'], 'solutions': benchmark['solutions']}])
        }
        for benchmark in category['benchmarks']
      }
      for category in config['categories']
    }

  def normalize_benchmark_data(raw_data):
    def find_suitable_iterations(current_benchmark):
      last_iter = 0
      last_cycle = 0
      for iter, cycle in current_benchmark.iterations.items():
        if last_iter > 8: #  iterations 1..8 sind scheisse :)
          if cycle < 1.5*last_cycle: # ideal: 2.0*last_cycle
            return last_iter
        last_iter = iter
        last_cycle = cycle
      return last_iter

    def get_normalization_bases(raw_data):
      return {
        category_id:{
          target_id: reduce(lambda a, b: b[1] if a == -1 else min(a, b[1]), group, -1)
          for target_id, group in 
            groupby(
              sorted([(target_id, cycle_count/iterations) 
                  for _, current_benchmark in current_category.items()
                  for _, current_group in current_benchmark.items()
                  for _, current_solution in current_group.items()
                  for _, current_mode in current_solution.items()
                  for _, current_toolchain in current_mode.items()
                  for target_id, current_benchmark in current_toolchain.items()
                  for iterations, cycle_count 
                  in current_benchmark.iterations.items()
                    if iterations == find_suitable_iterations(current_benchmark)
                  ], 
                  key=itemgetter(0)), 
              key=itemgetter(0))
        }
        for (category_id, current_category) in raw_data.items()
      }

    normalize_bases = get_normalization_bases(raw_data)

    return {category_id:{
        benchmark_id:{
          group_id:{
            solution_id:{
              mode_id:{
                toolchain_id:{
                  target_id:(
                    cycle_count/iterations, 
                    (cycle_count/iterations)/normalize_bases[category_id][target_id]
                  ) for (target_id,current_target) in current_toolchain.items()
                    for iterations, cycle_count 
                      in current_target.iterations.items()
                      if iterations == find_suitable_iterations(current_target)
                } for (toolchain_id,current_toolchain) in current_mode.items()
              } for (mode_id,current_mode) in current_solution.items()
            } for (solution_id,current_solution) in current_group.items()
          } for (group_id,current_group) in current_benchmark.items()
        } for (benchmark_id,current_benchmark) in current_category.items()
      } for (category_id,current_category) in raw_data.items()}

  def mean(lst):
      return sum(lst)/len(lst) \
             if len(lst) != 0 \
             else None

  def get_averaged_normalized_benchmark_data(raw_data):
    with_normalized_data = normalize_benchmark_data(raw_data)

    return {category_id:{
        benchmark_id:{
          group_id:{
            solution_id:{
              mode_id:{
                toolchain_id:
                  mean([cpi_norm for (target_id,(cpi,cpi_norm)) in current_mode.items()]) 
                  for (toolchain_id,current_mode) in current_toolchain.items()
              } for (mode_id,current_toolchain) in current_solution.items()
            } for (solution_id,current_solution) in current_group.items()
          } for (group_id,current_group) in current_benchmark.items()
        } for (benchmark_id,current_benchmark) in current_category.items()
      } for (category_id,current_category) in with_normalized_data.items()}

  def get_t_tc_omc_data(average_normalized):
    return {
      category_id:{
        benchmark_id:{
          group_id:{
            solution_id:{
              mode_id:(
                min([mean_of_targets 
                  for (_,mean_of_targets) in current_toolchain.items()
                  if mean_of_targets is not None]),
                max([mean_of_targets 
                  for (_,mean_of_targets) in current_toolchain.items()
                  if mean_of_targets is not None]),
                mean([mean_of_targets 
                  for (_,mean_of_targets) in current_toolchain.items()
                  if mean_of_targets is not None])
              ) for (mode_id,current_toolchain) in current_solution.items()
            } for (solution_id,current_solution) in current_group.items()
          } for (group_id,current_group) in current_benchmark.items()
        } for (benchmark_id,current_benchmark) in current_category.items()
      } for (category_id,current_category) in average_normalized.items()}

  def save_as_csv(filename, table_data):
    with open(filename, 'w') as f:
      print('\n'.join([','.join(map(str, row)) for row in table_data]), file=f)

  def generate_t_tc_omc_tables(average_normalized):
    for category_id,current_category in get_t_tc_omc_data(average_normalized).items():
      for benchmark_id,current_benchmark in current_category.items():
        for group_id,current_group in current_benchmark.items():
          table_data = [
            [
              'solution_id',
              *[y for x in [[
                    '{}_min'.format(mode['id']),
                    '{}_max'.format(mode['id']),
                    '{}_mean'.format(mode['id'])
                  ] 
                  for mode in config['modes']]
                for y in x]
            ],
            *[
            (
                solution_id,
                *[y 
                  for x 
                    in [current_solution[mode['id']] for mode in config['modes']]
                  for y in x]
            )
            for (solution_id,current_solution) in current_group.items()]
          ]
          dirname = Path('../data/csv/time/{}/{}'.format(category_id, benchmark_id))
          filename = dirname.joinpath('{}_t-tc-cc-omc.csv'.format(group_id))
          dirname.mkdir(exist_ok=True, parents=True)
          save_as_csv(filename, table_data)

  def get_t_tc_cd_data(average_normalized):
    return {
      category_id:{
        benchmark_id:{
          group_id:{
            mode_id:{
              solution_id:current_solution
              for _,solution_id,current_solution in current_mode
            }
            for (mode_id,current_mode) in
              groupby(
                sorted(
                  [
                    (
                      mode_id, 
                      solution_id, 
                      {
                        toolchain_id:cpi_norm 
                        for (toolchain_id,cpi_norm) 
                        in current_mode.items()
                      }
                    )
                    for (solution_id,current_solution) in current_group.items()
                    for (mode_id,current_mode) in current_solution.items()
                  ],
                  key=itemgetter(0)),
                key=itemgetter(0))

          } for (group_id,current_group) in current_benchmark.items()
        } for (benchmark_id,current_benchmark) in current_category.items()
      } for (category_id,current_category) in average_normalized.items()}

  def generate_t_tc_cd_tables(average_normalized):
    for category_id,current_category in get_t_tc_cd_data(average_normalized).items():
      for benchmark_id,current_benchmark in current_category.items():
        for group_id,current_group in current_benchmark.items():
          for mode_id,current_mode in current_group.items():
            table_data = [
            [
              'solution_id',
              *[toolchain['id'] for toolchain in config['toolchains']]
            ]
            ,*[
              (
                solution_id, 
                *[current_solution[toolchain['id']] 
                  if current_solution[toolchain['id']]
                  else 0
                  for toolchain in config['toolchains']]
              )
              for (solution_id,current_solution) in current_mode.items()
            ]]
            dirname = Path('../data/csv/time/{}/{}/{}'.format(category_id, benchmark_id,group_id))
            filename = dirname.joinpath('{}_t-tc-cd.csv'.format(mode_id))
            dirname.mkdir(exist_ok=True, parents=True)
            save_as_csv(filename, table_data)

  def generate_benchmark_tables(raw_data):
    average_normalized = get_averaged_normalized_benchmark_data(raw_data)

    # id: t-tc-cc-omc
    #     (time_target-collapsed_compiler-collapsed_optimization-mode-combined)
    # unit: category,benchmark,group
    # columns: solution,mode,min(cpi_norm),max(cpi_norm),mean(cpi_norm)
    # collapsed: target,toolchain
    #
    # cpi_norm = cycle per iteration, normalized
    #
    #     Lsg A  <--|--->
    #             <*|***>
    #     Lsg B     <-|->
    #             <*|********>
    #     Lsg C        <|------>
    #                <**|**>
    #     
    #     --- = opt size
    #     *** = opt speed
    #
    generate_t_tc_omc_tables(average_normalized)

    # id: t-tc-cd
    #     (time_target-collapsed_compiler-detail)
    # unit: category,benchmark,group,mode
    # columns: solution,toolchain,cpi_norm
    # collapsed: target
    #
    # cpi_norm = cycle per iteration, normalized
    #
    #     Lsg A -----
    #           *********
    #           ###
    #     Lsg B -----
    #           ******
    #           ###
    #     Lsg C ----------
    #           *******
    #           ####
    #     
    #     --- = Compiler A
    #     *** = Compiler B
    #     ### = Compiler C
    #
    generate_t_tc_cd_tables(average_normalized)

  def process(config):
    print("[Time] Collecting raw data...", end='', flush=True)
    raw_data = collect_raw_data(config)
    print("done")
    
    print("[Time] Generating benchmark tables...", end='', flush=True)
    generate_benchmark_tables(raw_data)
    print("done")

  process(config)


def generate_space_data_tables(config):
  def collect_raw_data(config):
    def load_nm_data(filename):
      result, exit_code = subprocess.Popen(['/mingw64/bin/arm-none-eabi-nm.exe', 
        '--no-demangle', 
        '--line-numbers', 
        '--print-size',
        '--defined-only',
        '--size-sort',
        filename],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE).communicate()

      # Format:
      # Address [Size] Type Name [Filename:Line]
      matches = [
        re.match(b'^([0-9a-fA-F]+)\\s+([0-9a-fA-F]+)?\\s+(.)\\s+(.*?)(?:\\s+(.*):(\\d+))?$', line)
        for line in result.splitlines()
        ]

      def to_path(s):
        return Path(
          s.replace('c:\\', '/c/').replace('C:\\', '/c/').replace('\\', '/')
          if s.lower().startswith('c:\\')
          else (s.replace('c:/', '/c/').replace('C:/', '/c/')
            if s.lower().startswith('c:/')
            else s)
          )

      symbol_sizes = [
        {
          'address': int(match.group(1), 16),
          'size': int(match.group(2), 16),
          'type': match.group(3),
          'name': match.group(4),
          'source_location': {
            'filename': to_path(str(match.group(5), 'utf-8')),
            'line': int(match.group(6))
          } if match.group(5) else None
        } 
        for match in matches if match
      ]
      return symbol_sizes

    def load_section_info(filename):
      result, exit_code = subprocess.Popen(['/mingw64/bin/arm-none-eabi-readelf.exe', 
        '-S',
        filename],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE).communicate()

      # Format:
      # \[Nr\] Name Type Addr Off Size ...
      matches = [
        re.match(b'^\\s+\\[\\s*(\\d+)\\]\\s+(.*?)\\s+(\\S+?)\\s+([0-9a-fA-F]+)\\s+([0-9a-fA-F]+)\\s([0-9a-fA-F]+)\\s([0-9a-fA-F]+)\\s.*$', line)
        for line in result.splitlines()
        ]

      section_info = [
        {
          'no': int(match.group(1)),
          'name': match.group(2),
          'type': match.group(3),
          'address': int(match.group(4), 16),
          'offset': int(match.group(5), 16),
          'size': int(match.group(6), 16)
        } 
        for match in matches if match
      ]
      return section_info

    def load_stack_usage(category, benchmark_map, solution_map, mode, toolchain, target):
      #print(category,"\n",benchmark,"\n", group, "\n",solution, "\n",mode,"\n", toolchain,"\n", target)
      filename = '{}/{}/{}-{}-{}'.format(
        category['map'],
        solution_map,
        target['map'],
        toolchain['map'],
        mode['map'])

      p = Path('../tmp/', filename, 'out/benchmark/log/run_0.stdout.log')
      if not p.exists():
        return

      with open(p, 'r') as f:
        parser = BenchmarkParser()
        report = parser.parse(f, p)

      #bmap = solution['map_benchmark'] if 'map_benchmark' in solution \
      #  else group['map'] if 'map' in group \
      #  else benchmark['map']
      bmap = benchmark_map

      if bmap not in report.benchmarks:
        if toolchain['id'] == 'armcc' and (
          bmap == 'BindCallWrapper' or
          bmap == 'InstallCallbackBindCallWrapper' or
          bmap == 'ExecuteCallbackBindCallWrapper'
          ):
          #print('not supported')
          return
        print('ERROR', bmap, report)
        return
      return report.benchmarks[bmap].stack_usage


    def load_stack_usages(category, benchmark_maps, solution_id, solution_map, mode, toolchain, target):
      return {
        benchmark_id:
          load_stack_usage(category, _map_info['map_benchmark'], solution_map, mode, toolchain, target)
        for benchmark_id, _map_info in benchmark_maps.items()
      }
    
    def load_size_data(category, benchmark_maps, solution_id, solution_map, mode, toolchain, target):
      filename = '{}/{}/{}-{}-{}'.format(
        category['map'],
        solution_map,
        target['map'],
        toolchain['map'],
        mode['map'])

      p = Path('../tmp/', filename, 'out/build/artifact/template.elf')
      if not p.exists():
        return
      
      return {
        'nm': load_nm_data(p),
        'readelf': load_section_info(p),
        'stack': load_stack_usages(category, benchmark_maps, solution_id, solution_map, mode, toolchain, target)
      }  

    return {
      category['id']:{
        solution['id']:{
          mode['id']:{
            toolchain['id']:{
              target['id']: [
                data_set
                for map_solution, _benchmark_maps in solution['maps'].items()
                for data_set in [
                  load_size_data(category, _benchmark_maps, solution['id'], map_solution, mode, toolchain, target)
                ]
                if data_set is not None
              ]
              for target in config['targets']
            }
            for toolchain in config['toolchains']
          }
          for mode in config['modes']
        }
        for solution in
          [
            {
              'id': id, 
              'maps': #set(map(itemgetter(1), maps))
                {
                  _solution_map:{
                    _benchmark_id:
                    {
                      'map_benchmark': _map_benchmark, #todo: groups
                    }
                    for (_, _, _benchmark_id, _map_benchmark) 
                    in _bench_maps
                  }
                  for (_solution_map, _bench_maps) 
                  in groupby(sorted(maps, key=itemgetter(1)), key=itemgetter(1)) 
                }
            }
            for (id, maps) in 
            groupby(
              sorted([
                (
                    _solution['id'], 
                    _solution['map'], 
                    benchmark['id'], 
                    _solution['map_benchmark']
                        if 'map_benchmark' in _solution
                        else (group['map'] if 'map' in group else benchmark['map']
                        )                        #todo: groups
                )
                for benchmark in category['benchmarks']
                for group in (
                  benchmark['groups'] 
                  if 'groups' in benchmark
                  else [{'id': benchmark['id'], 'solutions': benchmark['solutions']}]
                )
                for _solution in group['solutions']
              ], 
              key=itemgetter(0)),
            key=itemgetter(0))
          ]
      }
      for category in config['categories']
    }

  def mean(lst):
    filtered = list(filter(None, lst))
    return sum(filtered)/len(filtered) \
         if len(filtered) != 0 \
         else None

  def filter_symbols(raw_data):
    whitelisted_symbols = open('whitelist.txt', 'rb').read().splitlines()
    blacklisted_symbols = open('blacklist.txt', 'rb').read().splitlines()

    def nm_filter_accept(symbol_info):
      whitelisted = any([whitelisted_symbol.strip() in symbol_info['name']
        for whitelisted_symbol in whitelisted_symbols])

      blacklisted = any([blacklisted_symbol.strip() in symbol_info['name']
        for blacklisted_symbol in blacklisted_symbols])

      in_rom = 0x0000000 <= symbol_info['address'] <= 0x1FFFFFFF
      in_sram = 0x20000000 <= symbol_info['address'] <= 0x3FFFFFFF

      def is_bad_source_location(source_location):
        if source_location is None:
          return False
        filename = str(symbol_info['source_location']['filename'])
        index = filename.find('/sources/')
        relative_filename = filename[index+1:]
        return relative_filename.startswith('sources/src') or \
          relative_filename.startswith('sources/third-party')

      return (
        #not blacklisted and
        whitelisted and
        not is_bad_source_location(symbol_info['source_location']))

    return {
      category_id:{
        solution_id:{
          mode_id:{
            toolchain_id:{
              target_id:[
                {
                  'nm': [
                    symbol_info 
                    for symbol_info  in size_dataset['nm']
                    if nm_filter_accept(symbol_info)
                  ],
                  'readelf': size_dataset['readelf'],
                  'stack': size_dataset['stack']
                }
                for size_dataset in size_data
              ]
              for (target_id, size_data) in toolchain.items()
            }
            for (toolchain_id, toolchain) in mode.items()
          }
          for (mode_id, mode) in solution.items()
        }
        for (solution_id, solution) in category.items()
      }
      for (category_id, category) in raw_data.items()
    }

  def calc_code_size(filtered_symbols):
    #filtered_symbols = filter_symbols(raw_data)
    #for toolchain_id, toolchain in filtered_symbols['error_handling']['throw_exception_catch_all']['size'].items():
    #    for target_id, target in toolchain.items():
    #        print(toolchain_id, target_id)
    #        print('\n'.join(map(str, target[0]['nm'])))
    #        print('\n'.join(map(str, target[0]['readelf'])))
    #    
    #exit()

    def get_text_size(data, toolchain):
      def get_text_sections(readelf, toolchain):
        return [
          section 
          for section in readelf 
          if section['address'] != 0
          and section['name'].startswith(b'.text')
        ]

      return sum([
          symbol_info['size'] 
          for symbol_info in data['nm']
          if any([
            (section['address'] 
             <= symbol_info['address'] 
             <= (section['address'] + section['size']))
            for section in get_text_sections(data['readelf'], toolchain)])
            or (symbol_info['type'] == b't' or symbol_info['type'] == b'T')
        ])

    def get_rodata_size(data, toolchain):
      def get_rodata_sections(readelf, toolchain):
        return [
          section 
          for section in readelf 
          if section['address'] != 0
          and section['name'].startswith(b'.rodata')
        ]

      rodata_symbols = [
        symbol_info for symbol_info
        in data['nm']
        if any([
            (section['address'] 
             <= symbol_info['address'] 
             <= (section['address'] + section['size']))
            for section in get_rodata_sections(data['readelf'], toolchain)])
          or (symbol_info['type'] == b'r' or symbol_info['type'] == b'R')
          or (symbol_info['type'] == b'P')
          or (symbol_info['type'] == b'n')
      ]

      ex_sections = [
        section 
        for section in data['readelf']
        if section['type'] == b'ARM_EXIDX' or
           any([section['name'].startswith(s) for s in [b'.ARM.extab', b'.ARM.exidx']])
        ]

      return sum(map(itemgetter('size'), rodata_symbols)) \
        + sum(map(itemgetter('size'), ex_sections))

    def get_data_size(data, toolchain):
      def get_data_sections(readelf, toolchain):
        return [
          section 
          for section in readelf 
          if section['address'] != 0
          and section['name'].startswith(b'.data') or section['name'].startswith(b'.bss')
        ]

      return sum([
          symbol_info['size'] 
          for symbol_info in data['nm']
          if any([
            (section['address'] 
             <= symbol_info['address'] 
             <= (section['address'] + section['size']))
            for section in get_data_sections(data['readelf'], toolchain)])
            or (symbol_info['type'] == b'b' or symbol_info['type'] == b'B')
            or (symbol_info['type'] == b'd' or symbol_info['type'] == b'D')
            or (symbol_info['type'] == b's' or symbol_info['type'] == b'S')
        ])
        
    def combine_list_of_dict(dicts):
      result = {}
      if len(dicts) == 0:
        return result;
      for k in dicts[0]:
        result[k] = tuple(d.get(k, None) for d in dicts)
      return result        

    return {
      category_id:{
        solution_id:{
          mode_id:{
            toolchain_id:
            {
              'text': mean([
                get_text_size(size_dataset, toolchain_id)
                for (target_id, size_data) in toolchain.items()
                for size_dataset in size_data
              ]),
              'rodata': mean([
                get_rodata_size(size_dataset, toolchain_id)
                for (target_id, size_data) in toolchain.items()
                for size_dataset in size_data
              ]),
              'data': mean([
                get_data_size(size_dataset, toolchain_id)
                for (target_id, size_data) in toolchain.items()
                for size_dataset in size_data
              ]),
              # Note: stack usage is by benchmark... 
              #       that means (haha) we need one mean per benchmark
              # Note: when we reduce the benchmarks, the only sane operation 
              #       would be max(), not mean()! Note that we still can do
              #       min(), max() and mean() to reduce targets.
              'stack': {
                benchmark_id:mean(_values_of_targets) 
                for (benchmark_id,_values_of_targets) 
                in combine_list_of_dict([
                  {
                    _benchmark_id:mean(_values_of_groups) 
                    for (_benchmark_id,_values_of_groups)
                    in combine_list_of_dict(list(map(itemgetter('stack'), size_data))).items()
                  }
                  for (target_id, size_data) in toolchain.items()
                ]).items()
              },
            }
            for (toolchain_id, toolchain) in mode.items()
          }
          for (mode_id, mode) in solution.items()
        }
        for (solution_id, solution) in category.items()
      }
      for (category_id, category) in filtered_symbols.items()
    }
  
  def save_as_csv(filename, table_data):
    with open(filename, 'w') as f:
      print('\n'.join([','.join(map(str, row)) for row in table_data]), file=f)

  def get_s_tc_cc_omc_data(data):
    return {
      category_id:{
        solution_id:{
          mode_id: (
            # text (ROM)
            min([toolchain['text'] for _,toolchain in mode.items() if toolchain['text']]), 
            max([toolchain['text'] for _,toolchain in mode.items() if toolchain['text']]),  
            mean([toolchain['text'] for _,toolchain in mode.items() if toolchain['text']]),
            # rodata (ROM)
            min([toolchain['rodata'] for _,toolchain in mode.items() if toolchain['rodata']]), 
            max([toolchain['rodata'] for _,toolchain in mode.items() if toolchain['rodata']]),  
            mean([toolchain['rodata'] for _,toolchain in mode.items() if toolchain['rodata']]),
            # data (RAM)
            min([toolchain['data'] for _,toolchain in mode.items() if toolchain['data']]),
            max([toolchain['data'] for _,toolchain in mode.items() if toolchain['data']]),
            mean([toolchain['data'] for _,toolchain in mode.items() if toolchain['data']]),
            # stack (RAM)
            min([max(filter(None, toolchain['stack'].values())) for _,toolchain in mode.items() if toolchain['stack']]),
            max([max(filter(None, toolchain['stack'].values())) for _,toolchain in mode.items() if toolchain['stack']]),
            mean([max(filter(None, toolchain['stack'].values())) for _,toolchain in mode.items() if toolchain['stack']]),
          )
          for (mode_id, mode) in solution.items()
        }
        for (solution_id, solution) in category.items()
      } 
      for (category_id, category) in data.items()}

  def generate_s_tc_cc_omc_tables(data):
    for category_id, category in get_s_tc_cc_omc_data(data).items():
      table_data = [
        [
          'solution_id',
          *[column_header
            for headers_by_mode
             in [['{}_text_min'.format(mode['id']),
                '{}_text_max'.format(mode['id']),   
                '{}_text_mean'.format(mode['id']),
                '{}_rodata_min'.format(mode['id']), 
                '{}_rodata_max'.format(mode['id']), 
                '{}_rodata_mean'.format(mode['id']),
                '{}_data_min'.format(mode['id']),   
                '{}_data_max'.format(mode['id']),   
                '{}_data_mean'.format(mode['id']),
                '{}_stack_min'.format(mode['id']),  
                '{}_stack_max'.format(mode['id']),  
                '{}_stack_mean'.format(mode['id'])]
                for mode in config['modes']]
             for column_header in headers_by_mode   
           ]
        ],
       *[(
            solution_id, 
            *[value 
              for sublist 
                in [solution[mode['id']] for mode in config['modes']]
              for value in sublist]
        )
        for (solution_id, solution) in category.items()]
      ]
      dirname = Path('../data/csv/space'.format())
      filename = dirname.joinpath('{}_s-tc-cc-omc.csv'.format(category_id))
      dirname.mkdir(exist_ok=True, parents=True)
      save_as_csv(filename, table_data)

  def get_s_tc_cd_data(data):
    return {
      category_id:{
        mode_id:{
          solution_id:{
            toolchain_id:
              (
                toolchain['text'],
                toolchain['rodata'],
                toolchain['data'],
                max(filter(None, toolchain['stack'].values()), default=0),
              )
            for (toolchain_id, toolchain) in mode_data.items()
          }
          for _, solution_id, mode_data in solution_mode_data
        }        
        for (mode_id, solution_mode_data) in
        groupby(sorted(
          [
            (
              mode_id, 
              solution_id,
              mode_data
            )
            for (solution_id, solution) in category.items()
            for (mode_id, mode_data) in solution.items()
          ],
          key=itemgetter(0)),
        key=itemgetter(0))
      } 
      for (category_id, category) in data.items()
    }

  def generate_s_tc_cd_tables(data):
    for category_id, category in get_s_tc_cd_data(data).items():
      for mode_id, mode in category.items():
        table_data = [
          ['solution_id', 'toolchain_id', 'text_size', 'rodata_size', 'data_size', 'stack_usage'],
          *[(solution_id, toolchain_id, text_size, rodata_size, data_size, stack_usage)
            for (solution_id, solution) in mode.items()
            for (toolchain_id, (text_size, rodata_size, data_size, stack_usage)) in solution.items()]
        ]
        dirname = Path('../data/csv/space/{}'.format(category_id))
        filename = dirname.joinpath('{}_s-tc-cd.csv'.format(mode_id))
        dirname.mkdir(exist_ok=True, parents=True)
        save_as_csv(filename, table_data)

  def get_s_tc_cc_mdo_data(data):
    return get_s_tc_cc_omc_data(data)
    
  def generate_s_tc_cc_mdo_tables(data):
    for category_id, category in get_s_tc_cc_mdo_data(data).items():
      for mode in config['modes']:
        table_data = [
          [
            'solution_id',
            'text_min',
            'text_max',   
            'text_mean',
            'rodata_min', 
            'rodata_max', 
            'rodata_mean',
            'data_min',   
            'data_max',   
            'data_mean',
            'stack_min',  
            'stack_max',  
            'stack_mean'
          ],
         *[(
              solution_id, 
              *solution[mode['id']]
          )
          for (solution_id, solution) in category.items()]
        ]
        dirname = Path('../data/csv/space/{}'.format(category_id))
        filename = dirname.joinpath('{}_s-tc-cc-mdo.csv'.format(mode['id']))
        dirname.mkdir(exist_ok=True, parents=True)
        save_as_csv(filename, table_data)

  def get_s_tc_cd_mdd_data(data):
    return {
      category_id:{
        solution_id:{
          mode_id:{
            toolchain_id:(              
              toolchain['text'] or 0,
              toolchain['rodata'] or 0,
              toolchain['data'] or 0,
              max(filter(None, toolchain['stack'].values()), default=0),
            )
            for (toolchain_id, toolchain) in mode.items()
          }
          for (mode_id, mode) in solution.items()
        }
        for (solution_id, solution) in category.items()
      } 
      for (category_id, category) in data.items()}
    
  def generate_s_tc_cd_mdd_tables(data):
    for category_id, category in get_s_tc_cd_mdd_data(data).items():
      for solution_id, solution in category.items():
        for mode_id, mode in solution.items():
          table_data = [
            [
              'toolchain_id',
              'text',
              'rodata',   
              'data',
              'stack', 
            ],
           *[(
                toolchain_id, 
                *toolchain
            )
            for (toolchain_id, toolchain) in mode.items()]
          ]
          dirname = Path('../data/csv/space/{}/{}'.format(category_id, solution_id))
          filename = dirname.joinpath('{}_s-tc-cd-mdd.csv'.format(mode_id))
          dirname.mkdir(exist_ok=True, parents=True)
          save_as_csv(filename, table_data)

  def get_s_tc_cc_sd_su_data(data):
    def get_minmaxmean(stack_usages):
      stack_usages = list(filter(None, map(itemgetter(2), stack_usages)))
      return (min(stack_usages), max(stack_usages), mean(stack_usages))
      
  
    return {
      category_id:{
        solution_id:{
          benchmark_id:
          { # yields a dict mode_id:(min,max,mean)
            mode_id:list(stack_usage_of_mode)[0][2]
            for (mode_id, stack_usage_of_mode)
            in groupby( # yields a dict mode_id:[(_, _, (min,max,mean))...]
              sorted(mode_stack_usages, key=itemgetter(1)), key=itemgetter(1))
          }
          for (benchmark_id,mode_stack_usages)
          in groupby( # yields a dict of benchmark_id:[(_, mode_id, (min,max,mean))...]
              sorted([
                (benchmark_id,mode_id,get_minmaxmean(stack_usages))
                for (mode_id, mode) in solution.items()
                for (benchmark_id, stack_usages)
                  in groupby( # yields a dict benchmark_id:[(toolchain_id, _, stack_usage)...]
                    sorted([(toolchain_id, _benchmark_id, _stack_usage)
                        for (toolchain_id, toolchain) in mode.items()
                        for (_benchmark_id,_stack_usage) in toolchain['stack'].items()],
                      key=itemgetter(1)),
                    key=itemgetter(1))
              ], 
              key=itemgetter(0)),
            key=itemgetter(0))
            
        }
        for (solution_id, solution) in category.items()
      } 
      for (category_id, category) in data.items()}

  def generate_s_tc_cc_sd_su_tables(data):
    for category_id,category in get_s_tc_cc_sd_su_data(data).items():
      for solution_id,solution in category.items():
        table_data = [
          ['benchmark_id',
           'speed_min', 'speed_max', 'speed_mean',
           'size_min', 'size_max', 'size_mean'],
         *[
            [benchmark_id,
            *benchmark['speed'], *benchmark['size']] # well this is not so nice, but well...
            for (benchmark_id, benchmark) 
            in solution.items()
          ]
        ]
        dirname = Path('../data/csv/space/{}'.format(category_id))
        filename = dirname.joinpath('{}_s-tc-cc-sd-su-omc.csv'.format(solution_id))
        dirname.mkdir(exist_ok=True, parents=True)
        save_as_csv(filename, table_data)
          

  def dump(dirname, filename, data):
    Path(dirname).mkdir(exist_ok=True, parents=True)
    with open(Path(dirname).joinpath(filename), 'w') as f:
      print(data, file=f)
   
  def dump_full_set(foldername, data):
    dump('../data/dump/{}/'.format(foldername), 'data.txt', data)
    for category_id,category in data.items():
      for solution_id,solution in category.items():
        for mode_id,mode in solution.items():
          for toolchain_id,toolchain in mode.items():
            for target_id,target in toolchain.items():
              dump('../data/dump/{}/{}/{}/{}/{}'.format(
                    foldername,
                    category_id,
                    solution_id,
                    mode_id,
                    toolchain_id), 
                   '{}.txt'.format(target_id), 
                   target)
   
  print("[Space] Collecting raw data...", end='', flush=True)
  raw_data = collect_raw_data(config)
  print('done')
  
  print("[Space] Dumping raw data...", end='', flush=True)    
  dump_full_set('raw', raw_data)
  print('done')
  
  print("[Space] Filtering symbols...", end='', flush=True)
  filtered_symbols = filter_symbols(raw_data)
  print('done')
  
  print("[Space] Dumping filtered data...", end='', flush=True)    
  dump_full_set('filtered', filtered_symbols)
  print('done')
  
  #print({
  #  category_id:{
  #    solution_id:{
  #      mode_id:{
  #        toolchain_id:{
  #          target_id: [
  #            dataset['stack'] for dataset in datasets
  #          ] for (target_id,datasets) in toolchain.items()
  #        } for (toolchain_id,toolchain) in mode.items()
  #      } for (mode_id,mode) in solution.items()
  #    } for (solution_id,solution) in category.items()
  #  } for (category_id,category) in raw_data.items()
  #})
  #exit(0)
  print("[Space] Map-Reduce size info...", end='', flush=True)
  code_sizes = calc_code_size(filtered_symbols)
  print('done')
  
  #print(code_sizes)

  # id: s-tc-cc-omc
  #     (space_target-collapsed_compiler-collapsed_optimization-mode-combined)
  # unit: category
  # columns: solution,mode,rom_size
  # collapsed: target,group,benchmark,toolchain
  #
  #  Lsg A  <--|--->
  #          <*|***>
  #  Lsg B     <-|->
  #          <*|********>
  #  Lsg C        <|------>
  #             <**|**>
  #
  #  --- = opt size
  #  *** = opt speed
  print("[Space] Generating tables...", end='', flush=True)
  generate_s_tc_cc_omc_tables(code_sizes)

  # id: s-tc-cd
  #     (space_target-collapsed_compiler-detail)
  # unit: category,mode
  # columns: solution,toolchain,text_size,
  # collapsed: target,group,benchmark
  #
  #     Lsg A ---===
  #           ******~~~
  #           ##++
  #     Lsg B -----==
  #           ******~~
  #           ###+
  #     Lsg C ----------===
  #           ******~~~~
  #           ####++++
  #     
  #     --- = Compiler A Code, 
  #     === = Compiler A ROData
  #     *** = Compiler B
  #           ...
  #     ### = Compiler C
  generate_s_tc_cd_tables(code_sizes)
  
  # id: s-tc-cc-mdo
  #     (space_target-collapsed_compiler-collapsed_memory-distribution-overview)
  # unit: category,mode
  # columns: solution,min/max/mean of text, rodata, data, stack
  # collapsed: target,group,toolchain,benchmark
  generate_s_tc_cc_mdo_tables(code_sizes)
  
  # id: s-tc-cd-mdd
  generate_s_tc_cd_mdd_tables(code_sizes)
  
  # id: s-tc-cc-sd-su-omc
  generate_s_tc_cc_sd_su_tables(code_sizes)
  print('done')


def main():
  with open('config.yml', 'r') as f:
    config = yaml.safe_load(f)

  generate_time_data_tables(config)
  generate_space_data_tables(config)


# -------------------------------------

if __name__ == '__main__':
  main()
